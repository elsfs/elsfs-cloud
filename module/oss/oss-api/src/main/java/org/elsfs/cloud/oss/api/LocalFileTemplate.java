/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.oss.api;

import cn.hutool.core.io.FileUtil;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.elsfs.cloud.oss.api.properties.FileProperties;

/**
 * 本地文件读取模式
 *
 * @author zeng
 */
@RequiredArgsConstructor
public class LocalFileTemplate implements FileTemplate {

  private final FileProperties properties;

  @Override
  public void createBucket(String bucketName) {
    FileUtil.mkdir(properties.getLocal().getBasePath() + FileUtil.FILE_SEPARATOR + bucketName);
  }

  @Override
  public List<Bucket> getAllBuckets() {
    return Arrays.stream(FileUtil.ls(properties.getLocal().getBasePath()))
        .filter(FileUtil::isDirectory)
        .map(dir -> new Bucket(dir.getName()))
        .collect(Collectors.toList());
  }

  @Override
  public void removeBucket(String bucketName) {
    FileUtil.del(properties.getLocal().getBasePath() + FileUtil.FILE_SEPARATOR + bucketName);
  }

  @Override
  public void putObject(String bucketName, String objectName, InputStream stream) throws Exception {
    putObject(bucketName, objectName, stream, null);
  }

  @Override
  public void putObject(
      String bucketName, String objectName, InputStream stream, String contextType) {
    // 当 Bucket 不存在时创建
    String dir = properties.getLocal().getBasePath() + FileUtil.FILE_SEPARATOR + bucketName;
    if (!FileUtil.isDirectory(
        properties.getLocal().getBasePath() + FileUtil.FILE_SEPARATOR + bucketName)) {
      createBucket(bucketName);
    }

    // 写入文件
    File file = FileUtil.file(dir + FileUtil.FILE_SEPARATOR + objectName);
    FileUtil.writeFromStream(stream, file);
  }

  @Override
  @SneakyThrows
  public S3Object getObject(String bucketName, String objectName) {
    String dir = properties.getLocal().getBasePath() + FileUtil.FILE_SEPARATOR + bucketName;
    S3Object s3Object = new S3Object();
    s3Object.setObjectContent(FileUtil.getInputStream(dir + FileUtil.FILE_SEPARATOR + objectName));
    return s3Object;
  }

  @Override
  public void removeObject(String bucketName, String objectName) throws Exception {
    String dir = properties.getLocal().getBasePath() + FileUtil.FILE_SEPARATOR + bucketName;
    FileUtil.del(dir + FileUtil.FILE_SEPARATOR + objectName);
  }

  @Override
  public List<S3ObjectSummary> getAllObjectsByPrefix(
      String bucketName, String prefix, boolean recursive) {
    String dir = properties.getLocal().getBasePath() + FileUtil.FILE_SEPARATOR + bucketName;

    return Arrays.stream(FileUtil.ls(dir))
        .filter(file -> file.getName().startsWith(prefix))
        .map(
            file -> {
              S3ObjectSummary summary = new S3ObjectSummary();
              summary.setKey(file.getName());
              return new S3ObjectSummary();
            })
        .collect(Collectors.toList());
  }
}
