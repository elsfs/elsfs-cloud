/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.oss.api.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * 文件 配置信息
 *
 * @author zeng
 */
@Data
@ConfigurationProperties(prefix = "file")
public class FileProperties {
  /** 默认的存储桶名称 */
  private String bucketName = "local";

  /** 本地文件配置信息 */
  @NestedConfigurationProperty private LocalFileProperties local = new LocalFileProperties();

  /** oss 文件配置信息 */
  @NestedConfigurationProperty private OssProperties oss = new OssProperties();

  /**
   * 本地文件 配置信息
   *
   * @author zeng
   */
  @Data
  public static class LocalFileProperties {

    /** 是否开启 */
    private boolean enable;

    /** 默认路径 */
    private String basePath;
  }

  /**
   * aws 配置信息
   *
   * <p>配置文件添加： oss: enable: true endpoint: http://127.0.0.1:9000 # pathStyleAccess 采用nginx反向代理或者AWS
   * S3 配置成true，支持第三方云存储配置成false pathStyleAccess: false access-key: minioadmin secret-key:
   * minioadmin bucket-name: admin region: custom-domain:
   */
  @Data
  public static class OssProperties {
    /** 是否开启 */
    private boolean enable;

    /** 启用端点服务 */
    private boolean enableEndpoint = true;

    /** 对象存储服务的URL */
    private String endpoint;

    /** 自定义域名 */
    private String customDomain;

    /**
     * true path-style nginx 反向代理和S3默认支持 pathStyle {http://endpoint/bucketname} false supports
     * virtual-hosted-style 阿里云等需要配置为 virtual-hosted-style 模式{http://bucketname.endpoint}
     */
    private Boolean pathStyleAccess = true;

    /** 应用ID */
    private String appId;

    /** 区域 */
    private String region;

    /** Access key就像用户ID，可以唯一标识你的账户 */
    private String accessKey;

    /** Secret key是你账户的密码 */
    private String secretKey;

    /** 最大线程数，默认： 100 */
    private Integer maxConnections = 100;
  }
}
