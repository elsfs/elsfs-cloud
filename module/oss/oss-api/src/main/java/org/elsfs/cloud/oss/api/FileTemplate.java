/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.oss.api;

import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.io.InputStream;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;

/**
 * 文件操作模板
 *
 * @author zeng
 */
public interface FileTemplate extends InitializingBean {

  /**
   * 创建bucket
   *
   * @param bucketName bucket名称
   */
  void createBucket(String bucketName);

  /**
   * 获取存储系统中的所有bucket列表。
   *
   * <p>此方法不接受任何参数，调用后将返回包含所有bucket的列表。
   *
   * @return 返回一个包含所有bucket的列表。列表中的每个元素都是一个Bucket对象，代表一个存储桶。
   */
  List<Bucket> getAllBuckets();

  /**
   * 删除 bucket
   *
   * @param bucketName bucket名称
   */
  void removeBucket(String bucketName);

  /**
   * 上传文件
   *
   * @param bucketName bucket名称
   * @param objectName 文件名称
   * @param stream 文件流
   * @param contextType 文件类型
   * @throws Exception e
   */
  void putObject(String bucketName, String objectName, InputStream stream, String contextType)
      throws Exception;

  /**
   * 上传文件
   *
   * @param bucketName bucket名称
   * @param objectName 文件名称
   * @param stream 文件流
   * @throws Exception e
   */
  void putObject(String bucketName, String objectName, InputStream stream) throws Exception;

  /**
   * 获取文件
   *
   * @param bucketName bucket名称
   * @param objectName 文件名称
   * @return 二进制流 API Documentation
   */
  S3Object getObject(String bucketName, String objectName);

  /**
   * 从指定的存储桶中删除对象。
   *
   * @param bucketName 存储桶的名称，指定要从中删除对象的存储桶。
   * @param objectName 对象的名称，指定要删除的具体对象。
   * @throws Exception 如果删除过程中遇到任何问题，将抛出异常。
   */
  void removeObject(String bucketName, String objectName) throws Exception;

  @Override
  default void afterPropertiesSet() throws Exception {}

  /**
   * 根据文件前置查询文件
   *
   * @param bucketName bucket名称
   * @param prefix 前缀
   * @param recursive 是否递归查询
   * @return S3ObjectSummary 列表
   * @see <a href="http://docs.aws.amazon.com/goto/WebAPI/s3-2006-03-01/ListObjects">AWS API
   *     Documentation</a>
   */
  List<S3ObjectSummary> getAllObjectsByPrefix(String bucketName, String prefix, boolean recursive);
}
