/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.tenant.biz.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;
import org.elsfs.cloud.tenant.api.entity.TenantEntity;

/**
 * 租户信息
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysTenant extends BaseEntity implements TenantEntity {
  /** 租户ID */
  @TableId private String tenantId;

  /** 租户名称 */
  @TableField(condition = SqlCondition.LIKE)
  private String tenantName;

  /** 租户域名 */
  @TableField(condition = SqlCondition.LIKE)
  private String tenantDomain;

  /** 租户编码 */
  @TableField(condition = SqlCondition.LIKE)
  private String tenantCode;

  /** 租户联系人 */
  @TableField(condition = SqlCondition.LIKE)
  private String tenantContact;

  /** 租户联系人电话 */
  @TableField(condition = SqlCondition.LIKE)
  private String tenantContactPhone;

  /** 租户联系人邮箱 */
  @TableField(condition = SqlCondition.LIKE)
  private String tenantContactEmail;
  /** 租户联系地址 */
  private String tenantContactAddress;

  /** 状态 0-无效 1-有效 */
  @TableField(whereStrategy = FieldStrategy.NOT_EMPTY)
  private String validFlag;

  /** 备注 */
  @TableField(whereStrategy = FieldStrategy.NOT_EMPTY)
  private String memo;

  /** 租户logo */
  private String tenantLogo;

  /** 租户类型 */
  @TableField(whereStrategy = FieldStrategy.NOT_EMPTY)
  private String tenantType;
}
