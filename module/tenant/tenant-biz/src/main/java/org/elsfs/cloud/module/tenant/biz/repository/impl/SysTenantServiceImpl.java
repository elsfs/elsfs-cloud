/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.tenant.biz.repository.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.elsfs.cloud.common.mybatis.repository.ElsfsCrudRepositoryImpl;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.module.tenant.biz.entity.SysTenant;
import org.elsfs.cloud.module.tenant.biz.repository.SysTenantRepository;
import org.elsfs.cloud.module.tenant.biz.repository.mapper.SysTenantMapper;
import org.springframework.stereotype.Service;

/**
 * 系统租户
 *
 * @author zeng
 */
@Service
@DS(DatasourceNameConstant.ADMIN)
public class SysTenantServiceImpl
    extends ElsfsCrudRepositoryImpl<SysTenantMapper, SysTenant, String>
    implements SysTenantRepository {}
