/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.tenant.biz.adapter.front;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.tenant.biz.entity.SysTenant;
import org.elsfs.cloud.module.tenant.biz.repository.SysTenantRepository;
import org.elsfs.cloud.tenant.api.handler.TenantReader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 租户信息
 *
 * @author zeng
 */
@RestController
@RequestMapping("/q/tenant")
@RequiredArgsConstructor
@Tag(description = "tenant", name = "租户信息")
public class QSysTenantController {
  private final SysTenantRepository sysTenantRepository;
  private final TenantReader tenantReader;

  @GetMapping("/tenant")
  @Operation(summary = "租户信息", description = "租户信息")
  public R<SysTenant> tenant(HttpServletRequest request) {
    String tenantId = tenantReader.reader(request);
    return R.success(sysTenantRepository.getById(tenantId));
  }
}
