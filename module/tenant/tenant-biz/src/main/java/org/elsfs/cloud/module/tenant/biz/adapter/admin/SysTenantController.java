/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.tenant.biz.adapter.admin;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.elsfs.cloud.common.core.co.EditValidFlagCo;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.util.lang.NamingCase;
import org.elsfs.cloud.common.util.lang.StringUtils;
import org.elsfs.cloud.module.tenant.biz.entity.SysTenant;
import org.elsfs.cloud.module.tenant.biz.qry.TenantValidationQry;
import org.elsfs.cloud.module.tenant.biz.repository.SysTenantRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 租户信息
 *
 * @author zeng
 */
@RestController
@RequestMapping("/tenant")
@RequiredArgsConstructor
@Tag(name = "租户信息")
public class SysTenantController
    extends LogicRepositoryCrudController<
        SysTenant, SysTenant, SysTenant, SysTenant, SysTenantRepository, String> {

  @PostMapping("validFlag")
  @Operation(summary = "修改状态", description = "修改状态")
  public R<Boolean> validFlag(@RequestBody @Valid EditValidFlagCo co) {
    boolean update =
        repository.update(
            Wrappers.lambdaUpdate(SysTenant.class)
                .in(SysTenant::getTenantId, co.getIds())
                .set(SysTenant::getValidFlag, co.getValidFlag()));
    return R.success(update);
  }

  @Override
  protected void check(SysTenant entity, boolean isUpdate) {
    LambdaQueryWrapper<SysTenant> queryWrapper = Wrappers.lambdaQuery(SysTenant.class);
    if (isUpdate) {
      queryWrapper.ne(SysTenant::getTenantId, entity.getTenantId());
    }
    queryWrapper.and(
        q ->
            q.eq(SysTenant::getTenantName, entity.getTenantName())
                .or()
                .eq(SysTenant::getTenantCode, entity.getTenantCode()));
    long count = repository.count(queryWrapper);
    if (count > 0) {
      throw new RuntimeException("租户名称或者编码重复");
    }
  }

  @PostMapping("exist")
  @Operation(summary = "判断字段值是否存在", description = "判断字段值是否存在")
  public R<Void> exist(@RequestBody @Valid TenantValidationQry qry) {
    QueryWrapper<SysTenant> wrapper =
        Wrappers.query(SysTenant.class)
            .eq(NamingCase.toUnderlineCase(qry.getValidationField()), qry.getValue());
    if (StringUtils.hasText(qry.getId())) {
      wrapper.ne("tenant_id", qry.getId());
    }
    long count = repository.count(wrapper);
    return count > 0 ? R.error(qry.getValue() + "已存在") : R.success();
  }
}
