/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.tenant.api.handler;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 多租户读取程序
 *
 * @author zeng
 */
@FunctionalInterface
public interface TenantReader {
  /** 默认租户标识常量。 在系统中代表默认租户的标识符，通常用于在多租户系统中作为缺省值。 */
  String DEFAULT_TENANT = "0";

  /**
   * 从HttpServletRequest中读取内容并返回为String。
   *
   * @param request HttpServletRequest对象，用于获取请求的内容。
   * @return 从请求中读取到的内容，返回为String形式。
   */
  String reader(HttpServletRequest request);

  default String getDefaultTenantValue() {
    return DEFAULT_TENANT;
  }
}
