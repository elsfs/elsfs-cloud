/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.tenant.api.handler;

/**
 * 保存当前请求用户的的租户信息， 使用threadLocal来实现，和当前请求线程绑定
 *
 * @author zeng
 */
public class TenantRequestContext {

  private static final ThreadLocal<String> tenantLocal = new ThreadLocal<>();

  /**
   * 为当前线程设置租户标识符。 这个方法用于将给定的租户ID绑定到当前线程的上下文中，以便在多租户系统中进行上下文切换。
   *
   * @param tenantId 租户标识符，不能为空。
   */
  public static void setTenantLocal(String tenantId) {
    tenantLocal.set(tenantId); // 将租户ID设置到当前线程的本地存储中
  }

  /**
   * 获取当前线程绑定的租户本地信息。
   *
   * @return 返回当前线程绑定的租户本地信息。这是一个线程安全的操作，可以用于多线程环境中获取当前线程所关联的租户信息。
   */
  public static String getTenantLocal() {
    final String tenantId = tenantLocal.get();
    return tenantId == null ? "" : tenantId; // 从ThreadLocal变量中获取当前线程的租户信息
  }

  /** 从当前线程绑定的租户上下文中移除租户信息。 该方法没有参数。 该方法没有返回值。 */
  public static void remove() {
    tenantLocal.remove(); // 移除当前线程绑定的租户信息
  }
}
