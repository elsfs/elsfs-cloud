/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.tenant.api.entity;

/**
 * 租户实体
 *
 * @author zeng
 */
public interface TenantEntity {
  /**
   * 租户ID
   *
   * @return tenantId
   */
  String getTenantId();

  /**
   * 租户名称
   *
   * @return tenantName
   */
  String getTenantName();

  /**
   * 租户编码
   *
   * @return tenantCode
   */
  String getTenantCode();

  /**
   * 租户类型
   *
   * @return tenantType
   */
  String getTenantType();

  /**
   * 租户状态
   *
   * @return tenantStatus
   */
  String getValidFlag();

  /**
   * 租户LOGO
   *
   * @return tenantLogo
   */
  String getTenantLogo();

  /**
   * 租户域名
   *
   * @return tenantDomain
   */
  String getTenantDomain();

  /**
   * 租户联系人
   *
   * @return tenantContact
   */
  String getTenantContact();

  /**
   * 租户联系人电话
   *
   * @return tenantContactPhone
   */
  String getTenantContactPhone();

  /**
   * 租户联系人邮箱
   *
   * @return tenantContactEmail
   */
  String getTenantContactEmail();

  /**
   * 备注
   *
   * @return 备注
   */
  String getMemo();
}
