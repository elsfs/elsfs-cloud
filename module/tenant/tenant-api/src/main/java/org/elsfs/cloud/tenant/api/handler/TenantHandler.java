/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.tenant.api.handler;

import java.util.List;

/**
 * 多租户解析器
 *
 * @param <T> 多租户类型
 */
public interface TenantHandler<T> {

  /**
   * 租户ID值
   *
   * @return 租户 ID 值
   */
  default String findTenantId() {
    return TenantRequestContext.getTenantLocal();
  }

  /**
   * 获取租户字段名
   *
   * <p>默认字段名叫: tenant_id
   *
   * @return 租户字段名
   */
  default String getTenantIdColumn() {
    return "tenant_id";
  }

  /**
   * 根据表名判断是否忽略拼接多租户条件
   *
   * <p>默认都要进行解析并拼接多租户条件
   *
   * @param tableName 表名
   * @return 是否忽略, true:表示忽略，false:需要解析并拼接多租户条件
   */
  default boolean ignoreTable(String tableName) {
    return false;
  }

  /**
   * 忽略插入和更新租户字段逻辑
   *
   * @param columns 插入字段
   * @param tenantIdColumn 租户 ID 字段
   * @return 忽略插入和更新租户字段逻辑
   */
  boolean ignoreInsertAndUpdate(List<T> columns, String tenantIdColumn);
}
