/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.tenant.api.handler;

import jakarta.servlet.http.HttpServletRequest;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Setter;
import org.elsfs.cloud.common.util.lang.StringUtils;

/**
 * 默认租户读取器
 *
 * @author zeng
 */
@Setter
public class DefaultTenantReader implements TenantReader {
  private static final Logger LOGGER = Logger.getLogger(DefaultTenantReader.class.getName());

  @Override
  public String reader(HttpServletRequest request) {
    String tenantId = request.getHeader("TenantId");
    LOGGER.log(Level.FINER, "tenantId: {}", tenantId);
    return StringUtils.isNotBlank(tenantId) ? tenantId : getDefaultTenantValue();
  }
}
