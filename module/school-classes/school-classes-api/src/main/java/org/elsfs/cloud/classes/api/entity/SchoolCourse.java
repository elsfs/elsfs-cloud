package org.elsfs.cloud.classes.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 课程
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolCourse extends BaseEntity {
  @TableId
  @Schema(description = "课程ID")
  private String courseId;
  @Schema(description = "课程名称")
  private String courseName;
  @Schema(description = "课程类型")
  private String courseType;
  @Schema(description = "课程学时")
  private String courseHour;
  @Schema(description = "课程备注")
  private String courseRemark;
}
