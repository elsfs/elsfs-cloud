package org.elsfs.cloud.classes.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

import java.time.LocalTime;

/**
 * 课程表
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolClassCourse extends BaseEntity {
  @TableId
  @Schema(description = "课程表ID")
  private String classCourseId;
  @Schema(description = "班级ID")
  private String classGradesId;
  @Schema(description = "课程名称")
  private String courseName;
  @Schema(description = "教师ID")
  private String teacherId;
  @Schema(description = "教师姓名")
  private String teacherName;
  @Schema(description = "周次")
  private String week;
  @Schema(description = "星期几")
  private String day;
  @Schema(description = "第几节")
  private String section;
  @Schema(description = "教室")
  private String room;
  @Schema(description = "开始时间")
  private LocalTime startTime;
  @Schema(description = "结束时间")
  private LocalTime endTime;
}
