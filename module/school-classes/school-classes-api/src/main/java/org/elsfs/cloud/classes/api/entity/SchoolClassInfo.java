package org.elsfs.cloud.classes.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 班级信息
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolClassInfo extends BaseEntity {
  @TableId
  @Schema(description = "班级id")
  private String classGradesId;
  @Schema(description = "班级名称")
  private String name;
  @Schema(description = "班名称")
  private String className;
  @Schema(description = "年级名称")
  private String gradesName;
  @Schema(description = "届次")
  private String session;
  @Schema(description = "班主任id")
  private String classTeacherId;
  @Schema(description = "班主任姓名")
  private String classTeacherName;
  @Schema(description = "班级状态  班级状态 0 未开始 1 进行中 -1 结束")
  private String classStatus;
}
