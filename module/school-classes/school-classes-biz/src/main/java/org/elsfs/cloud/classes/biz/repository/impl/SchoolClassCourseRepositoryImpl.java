package org.elsfs.cloud.classes.biz.repository.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.elsfs.cloud.classes.api.entity.SchoolClassCourse;
import org.elsfs.cloud.classes.biz.repository.SchoolClassCourseRepository;
import org.elsfs.cloud.classes.biz.repository.mapper.SchoolClassCourseMapper;
import org.elsfs.cloud.common.mybatis.repository.ElsfsCrudRepositoryImpl;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.springframework.stereotype.Service;

/**
 * 班级课程表
 *
 * @author zeng
 */
@Service
public class SchoolClassCourseRepositoryImpl
  extends ElsfsCrudRepositoryImpl<SchoolClassCourseMapper, SchoolClassCourse, String>
implements SchoolClassCourseRepository

{
}
