package org.elsfs.cloud.classes.biz.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.classes.api.entity.SchoolClassInfo;
import org.elsfs.cloud.classes.biz.repository.SchoolClassInfoRepository;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 班级信息
 *
 * @author zeng
 */
@RestController
@RequestMapping("/schoolClassInfo")
@RequiredArgsConstructor
@Tag(description = "schoolClassInfo", name = "班级信息管理")
public  class SchoolClassInfoController
  extends LogicRepositoryCrudController<
    SchoolClassInfo, SchoolClassInfo, SchoolClassInfo, SchoolClassInfo, SchoolClassInfoRepository, String> {

    }
