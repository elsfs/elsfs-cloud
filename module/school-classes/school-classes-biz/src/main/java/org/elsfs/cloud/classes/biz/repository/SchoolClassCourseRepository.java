package org.elsfs.cloud.classes.biz.repository;

import org.elsfs.cloud.classes.api.entity.SchoolClassCourse;
import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;

public interface SchoolClassCourseRepository
extends IElsfsRepository<SchoolClassCourse, String>
{
}
