package org.elsfs.cloud.classes.biz.repository;

import org.elsfs.cloud.classes.api.entity.SchoolClassInfo;
import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;

public interface SchoolClassInfoRepository  extends IElsfsRepository<SchoolClassInfo,String> {
}
