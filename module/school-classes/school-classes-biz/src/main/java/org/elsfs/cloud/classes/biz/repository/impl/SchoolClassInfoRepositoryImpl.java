package org.elsfs.cloud.classes.biz.repository.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.elsfs.cloud.classes.api.entity.SchoolClassInfo;
import org.elsfs.cloud.classes.biz.repository.SchoolClassInfoRepository;
import org.elsfs.cloud.classes.biz.repository.mapper.SchoolClassInfoMapper;
import org.elsfs.cloud.common.mybatis.repository.ElsfsCrudRepositoryImpl;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.springframework.stereotype.Service;


/**
 * 班级信息
 *
 * @author zeng
 */
@Service
public class SchoolClassInfoRepositoryImpl extends ElsfsCrudRepositoryImpl<SchoolClassInfoMapper, SchoolClassInfo, String>
  implements SchoolClassInfoRepository {}
