package org.elsfs.cloud.classes.biz.repository.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Mapper;
import org.elsfs.cloud.classes.api.entity.SchoolClassInfo;
import org.elsfs.cloud.common.mybatis.context.ELsfsBaseMapper;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;

/**
 * 班级信息
 *
 * @author zeng
 */
@Mapper
@DS(DatasourceNameConstant.SCHOOL)
public interface SchoolClassInfoMapper extends ELsfsBaseMapper<SchoolClassInfo> {
}
