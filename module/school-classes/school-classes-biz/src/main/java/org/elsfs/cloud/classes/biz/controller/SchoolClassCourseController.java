package org.elsfs.cloud.classes.biz.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.classes.api.entity.SchoolClassCourse;
import org.elsfs.cloud.classes.biz.repository.SchoolClassCourseRepository;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程表
 *
 * @author zeng
 */
@RestController
@RequestMapping("/schoolClassCourse")
@RequiredArgsConstructor
@Tag(description = "schoolClassCourse", name = "课程表管理")
public class SchoolClassCourseController
  extends LogicRepositoryCrudController<
  SchoolClassCourse, SchoolClassCourse, SchoolClassCourse, SchoolClassCourse, SchoolClassCourseRepository, String> {

}

