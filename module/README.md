# 模块规则
1. 父模块命名规则 `{业务名}`或者`{范围实用大范围}-{业务名}` 
2. 子模块命名规则 `{业务名}-{范围实用范围}`
3. 约定大于配置，约定见下文
4. 业务提供web服务的模块不能依赖提供web服务的模块 消息和通知除外


## 模块介绍
| 模块名            | 业务    |
|----------------|-------|
| cms            | 内容管理  |
| config         | 通用配置  |
| datasource     | 数据源管理 |  
| dict	          | 字典管理  |
| oss            | 文件存储  |
| pay            | 支付    |
| permission     | 权限    |
| system         | 核心系统  |
| tenant         | 租户管理  |
| school-classes | 学校班级    |


## 业务依赖层次
依赖约定
1. 一级为基础层，二级为基础业务层，三级为业务层
2. 平级可以相互依赖，但是不能循环依赖，标注依赖不能逆依赖
3. system模块不能被同级依赖
4. 下级可以依赖上级
5. 上级不能依赖下级
6. 同级之间只能依赖api子模块
7. 业务层必须包含api子模块 ,biz子模块和sys子模块可选，如果有其他特殊业务需要定义模块的，子模块可以自定义（通车是业务的首字母或者有明确的业务单纯）
```mermaid
graph TB
    tenant--> datasource
    datasource--> dict
    datasource--> config
    datasource--> oss
    datasource--> pay
    dict--> permission
    dict--> system
    dict--> cms
    dict--> school-classes
```
标注依赖
```mermaid
graph LR
    permission--> system
```
## 查询数据和存储数据顺序约定
优先级 顺序，没有接人对应的缓存，可以向下查询数据

1. 本地缓存
2. 分布式缓存
3. elasticsearch
4. 关系数据库

## 通用web接口约定
| 后者路径            | 业务          | 请求方式｜  |
|-----------------|-------------|--------|
| list            | 列表          | GET    |
| page            | 分页          | GET    |
| getById/{id}    | 详情          | GET    |
| add             | 添加          | POST   |
| edit            | 保存          | PUT    |
| tree            | 获取树型列表      | GET    |
| /del/{id}       | 根据id删除接口    | DELETE |
| /del            | 批量删除        | DELETE |
| exportXls       | 导出          | GET    |
| importExcel     | 导入          | POST   |
| editState       | 修改状态        | PUT    |
| /logic/page     | 分页查询软删除数据   | GET    |
| /logic/add/{id} | 恢复软删除数据     | PUT    |
| /logic/add      | 批量恢复软删除数据   | PUT    |
| /logic/del/{id} | 根据id删除软删除数据 | DELETE |
| /logic/del      | 批量删除软删除数据   | DELETE |









