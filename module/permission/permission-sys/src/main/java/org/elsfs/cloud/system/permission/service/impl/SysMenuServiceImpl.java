/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.permission.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.utils.TreeUtils;
import org.elsfs.cloud.permission.api.dto.vo.TreeMenuVue3VO;
import org.elsfs.cloud.permission.api.entity.SysMenu;
import org.elsfs.cloud.permission.api.service.SysMenuService;
import org.elsfs.cloud.system.permission.repository.SysMenuRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * 菜单服务实现类
 *
 * @author zeng
 */
@Service
@RequiredArgsConstructor
public class SysMenuServiceImpl implements SysMenuService {

  private final SysMenuRepository sysMenuRepository;

  @Override
  public boolean save(SysMenu entity) {
    return sysMenuRepository.save(entity);
  }

  @Override
  public List<SysMenu> listByIds(Collection<? extends Serializable> idList) {
    return sysMenuRepository.listByIds(idList);
  }

  @Override
  public List<SysMenu> list(SysMenu entity) {
    return sysMenuRepository.list(Wrappers.query(entity));
  }

  @Override
  public boolean updateById(SysMenu entity) {
    return sysMenuRepository.updateById(entity);
  }

  @Override
  public boolean removeByIds(Collection<?> list) {
    return sysMenuRepository.removeByIds(list);
  }

  @Override
  public boolean removeById(Serializable entity) {
    return sysMenuRepository.removeById(entity);
  }

  /**
   * 构建 tree
   *
   * @return tree
   */
  public List<TreeMenuVue3VO> getTreeVue3(List<SysMenu> menuList) {

    if (CollectionUtils.isEmpty(menuList)) {
      return List.of();
    }
    List<TreeMenuVue3VO> treeMenuVue3VOS = new ArrayList<>(menuList.size());
    for (SysMenu menu : menuList) {
      TreeMenuVue3VO node = new TreeMenuVue3VO();
      node.setId(menu.getMenuId());
      node.setName(menu.getMenuName());
      node.setParentId(menu.getParentId());
      node.setPath(menu.getRoutePath());
      node.setComponent(menu.getComponent());
      TreeMenuVue3VO.Meta meta = new TreeMenuVue3VO.Meta();
      meta.setTitle(menu.getMenuName());
      meta.setIcon(menu.getIcon());
      if (menu.getType().equals("0")) {
        node.setHideChildrenInMenu(!menu.getIsShow());
        if (menu.getRoutePath().equals("/dashboard")) {
          node.setRedirect("/dashboard/analysis");
        }
      } else {
        // 隐藏菜单
        node.setHideMenu(!menu.getIsShow());
        // 隐藏面包屑
        node.setHideBreadcrumb(!menu.getIsShow());
      }
      node.setMeta(meta);
      treeMenuVue3VOS.add(node);
    }
    return TreeUtils.buildTree(treeMenuVue3VOS, "0");
  }
}
