/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.permission.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.controller.ControllerConfig;
import org.elsfs.cloud.common.controller.TreeLogicRepositoryCrudController;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.permission.api.entity.SysMenu;
import org.elsfs.cloud.permission.api.service.SysMenuService;
import org.elsfs.cloud.system.permission.repository.SysMenuRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 菜单控制器
 *
 * @author zeng
 */
@RestController
@RequestMapping("/menu")
@RequiredArgsConstructor
@Tag(description = "menu", name = "菜单管理")
public class SysMenuController
    extends TreeLogicRepositoryCrudController<
        SysMenu, SysMenu, SysMenu, SysMenu, SysMenuRepository, String> {
  private final SysMenuService sysMenuService;

  @Override
  protected ControllerConfig getConfig() {
    return super.getConfig().list(true);
  }

  /**
   * 获取菜单树
   *
   * @return r
   */
  @GetMapping("/getMenuVue3")
  public R<?> getMenuVue3() {
    List<SysMenu> menuList =
        repository.list(
            Wrappers.lambdaQuery(SysMenu.class)
                .ne(SysMenu::getType, "2")
                .orderByDesc(SysMenu::getOrderNo));

    return R.success( sysMenuService.getTreeVue3(menuList));
  }
}
