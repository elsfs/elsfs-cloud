/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.permission.repository.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import java.io.Serializable;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.mybatis.repository.ElsfsCrudRepositoryImpl;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.permission.api.dto.event.SysMenuRemoveEvent;
import org.elsfs.cloud.permission.api.entity.SysMenu;
import org.elsfs.cloud.system.permission.repository.SysMenuRepository;
import org.elsfs.cloud.system.permission.repository.mapper.SysMenuMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * 菜单服务
 *
 * @author zeng
 */
@Service
@RequiredArgsConstructor
@DS(DatasourceNameConstant.ADMIN)
public class SysMenuRepositoryImpl extends ElsfsCrudRepositoryImpl<SysMenuMapper, SysMenu, String>
    implements SysMenuRepository {
  private final ApplicationEventPublisher applicationEventPublisher;

  @Override
  public boolean save(SysMenu entity) {
    if (Objects.equals(entity.getParentId(), null)) {
      entity.setParentId("0");
    }
    if (Objects.equals(entity.getType(), "0")) {
      entity.setComponent("BasicLayout");
    }
    return super.save(entity);
  }

  @Override
  public boolean updateById(SysMenu entity) {
    entity.setMenuId(entity.getId() == null ? entity.getMenuId() : entity.getId());
    if (Objects.equals(entity.getParentId(), null)) {
      entity.setParentId("0");
    }
    if (Objects.equals(entity.getType(), "0")) {
      entity.setComponent("LAYOUT");
    }
    return super.updateById(entity);
  }

  @Override
  public boolean removeById(Serializable id) {
    boolean isRemove = super.removeById(id);
    if (isRemove) {
      applicationEventPublisher.publishEvent(new SysMenuRemoveEvent(id.toString()));
    }
    return isRemove;
  }
}
