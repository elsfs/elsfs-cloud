/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.permission.api.entity;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseTreeEntity;

/**
 * 菜单
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysMenu extends BaseTreeEntity<SysMenu, String> {

  @TableId private String menuId;

  /** 菜单名称 */
  @TableField(condition = SqlCondition.LIKE)
  private String menuName;

  /** 路由地址 */
  private String routePath;

  /** 上级菜单 */
  private String parentId;

  private String icon;

  private String component;

  /** 是否显示 */
  private Boolean isShow;

  /** 排序值，越小越靠前 */
  private Integer orderNo;

  /** 是否缓存路由，0否，1是 */
  private Boolean keepAlive;

  /** 是否外链 */
  private Boolean isExt;

  /** 菜单类型，0目录，1菜单，2按钮 */
  private String type;

  /** 状态 */
  private Integer status;

  @Override
  public String getId() {
    return menuId;
  }
}
