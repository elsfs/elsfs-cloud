/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.permission.api.entity;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 权限
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermission extends BaseEntity {
  /** 权限 id */
  @TableId private String permissionId;

  /** 权限名称 */
  @TableField(condition = SqlCondition.LIKE)
  private String name;

  /** 权限编码 */
  private String code;

  /** 权限类型 */
  private String type;

  /** 请求路径 */
  private String uri;

  /** 权限组 */
  private String pgId;

  private Integer orderNo;

  private Integer status;

  /** 权限描述 */
  private String description;
}
