/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.permission.api.dto.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import org.elsfs.cloud.common.core.utils.TreeNode;

/**
 * 菜单树
 *
 * @author zeng
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TreeMenuVue3VO implements TreeNode<TreeMenuVue3VO, String> {
  /** 菜单id */
  private String id;

  /** 父级id */
  private String parentId;

  /** 菜单名称 */
  private String name;

  /** 路由地址 */
  private String path;

  /** 组件 */
  private String component;

  /** 隐藏菜单 */
  private Boolean hideMenu;

  /** 隐藏面包屑 */
  private Boolean hideBreadcrumb;

  /** 重定向地址 */
  private String redirect;

  /** 隐藏子菜单 */
  private Boolean hideChildrenInMenu;

  private Meta meta;

  /** 子菜单 */
  private List<TreeMenuVue3VO> children = new ArrayList<>();

  /** 元数据 */
  @Data
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class Meta {
    /** 图标 */
    private String icon;

    /** 标题 */
    private String title;

    /** 跳转路径 */
    private String redirect;

    /** 隐藏子菜单 */
    private Boolean hideChildrenInMenu;
  }
}
