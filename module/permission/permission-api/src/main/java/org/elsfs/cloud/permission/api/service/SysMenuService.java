/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.permission.api.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import org.elsfs.cloud.permission.api.dto.vo.TreeMenuVue3VO;
import org.elsfs.cloud.permission.api.entity.SysMenu;

/**
 * 菜单服务
 *
 * @author zeng
 */
public interface SysMenuService {
  boolean save(SysMenu entity);

  List<SysMenu> listByIds(Collection<? extends Serializable> idList);

  List<SysMenu> list(SysMenu entity);

  boolean updateById(SysMenu entity);

  boolean removeByIds(Collection<?> list);

  boolean removeById(Serializable entity);

  /**
   * 构建 tree
   *
   * @return tree
   */
  List<TreeMenuVue3VO> getTreeVue3(List<SysMenu> menuList);
}
