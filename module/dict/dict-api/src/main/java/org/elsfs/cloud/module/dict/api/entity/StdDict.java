/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.api.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 字典表
 *
 * @author zeng
 * @since 2024-07-27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("std_dict")
@Schema(description = "字典表")
public class StdDict extends BaseEntity {

  @Schema(description = "字典ID")
  @TableId("dict_id")
  private String dictId;

  @Schema(description = "表名 null表示在std_dict_item表，标准的基本上在该表")
  @TableField(whereStrategy = FieldStrategy.NOT_EMPTY)
  private String tableName;

  @Schema(description = "是否是树形结构")
  private Boolean isTree;

  @Schema(description = "是否有独立数据表 配合table_name使用，0表示无独立数据表，1表示有独立数据表")
  private Boolean isIndependence;

  @Schema(description = "是否允许添加")
  private Boolean allowAdding;

  @Schema(description = "状态")
  private String status;

  @TableField(condition = SqlCondition.LIKE,whereStrategy = FieldStrategy.NOT_EMPTY)
  @Schema(description = "标准号")
  private String stdNo;

  @TableField(condition = SqlCondition.LIKE)
  @Schema(description = "标准类别")
  private String stdCategory;

  @Schema(description = "全部代替标准")
  @TableField(condition = SqlCondition.LIKE)
  private String stdReplace;

  /** 中国标准分类号(chinese classification for standards) 字典 css */
  @TableField(condition = SqlCondition.LIKE)
  @Schema(description = "中国标准分类号 字典 css")
  private String ccs;

  @TableField(condition = SqlCondition.LIKE)
  @Schema(description = "国际标准分类号(ICS) 字典 ics")
  private String ics;

  @TableField(condition = SqlCondition.LIKE)
  @Schema(description = "字典描述")
  private String dictDesc;

  @Schema(description = "字典编码")
  @TableField(whereStrategy = FieldStrategy.NOT_EMPTY)
  private String dictCode;

  @Schema(description = "字典名称")
  @TableField(condition = SqlCondition.LIKE,whereStrategy = FieldStrategy.NOT_EMPTY)
  private String dictName;

  @Schema(description = "标准类型")
  private String dictType;

  @Schema(description = "参考网站")
  @TableField(condition = SqlCondition.LIKE)
  private String website;

  @Schema(description = "备注")
  @TableField(condition = SqlCondition.LIKE)
  private String remark;

  @Schema(description = "发布日期")
  private LocalDate releaseDate;

  @Schema(description = "实施日期")
  private LocalDate implementationDate;

  @Schema(description = "废止日期")
  private LocalDate revocatoryDate;
}
