/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.api.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.OrderBy;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseTreeEntity;

/**
 * 字典项
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("std_dict_item")
public class StdDictItem extends BaseTreeEntity<StdDictItem, String> {
  @TableId
  @Schema(description = "字典项ID")
  private String dictItemId;

  @Schema(description = "字典ID")
  private String dictId;

  @Schema(description = "字典项名称")
  @TableField(condition = SqlCondition.LIKE,whereStrategy = FieldStrategy.NOT_EMPTY)
  private String label;

  @Schema(description = "字典项值")
  private String value;

  @Schema(description = "字典ID")
  private String parentId;

  @Schema(description = "深度 数结构深度")
  private Integer depth;

  @Schema(description = "自定义字典类型或者分类")
  private String customType;

  @Schema(description = "样式")
  private String css;

  @Schema(description = "备注")
  private String remark;

  @Schema(description = "图标")
  private String icon;

  @Schema(description = "状态")
  private String status;
  @Schema(description = "排序")
  @OrderBy(asc = true, sort = 10)
  private Integer orderNo;

  @Override
  public String getId() {
    return dictItemId;
  }
}
