/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.biz.repository.impl;

import org.elsfs.cloud.common.mybatis.repository.ElsfsCrudRepositoryImpl;
import org.elsfs.cloud.module.dict.api.entity.StdDictItem;
import org.elsfs.cloud.module.dict.biz.repository.StdDictItemRepository;
import org.elsfs.cloud.module.dict.biz.repository.mapper.StdDictItemMapper;
import org.springframework.stereotype.Service;

/**
 * 字典项
 *
 * @author zeng
 */
@Service
public class StdDictItemRepositoryImpl
    extends ElsfsCrudRepositoryImpl<StdDictItemMapper, StdDictItem, String>
    implements StdDictItemRepository {}
