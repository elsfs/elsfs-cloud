/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.biz.adapter.front;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.module.dict.api.entity.StdDictItem;
import org.elsfs.cloud.module.dict.api.service.DictService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 字典项管理控制器
 *
 * @author zeng
 */
@RestController
@RequestMapping("/q/dictItem")
@RequiredArgsConstructor
@Tag(description = "dictItem", name = "字典管理")
public class QStdDictItemController {
  private final DictService dictService;

  @RequestMapping("/getListByDictCode/{dictCode}")
  @Operation(summary = "根据字典编码获取字典项", description = "根据字典编码获取字典项")
  public List<StdDictItem> getListByDictCode(@PathVariable("dictCode") String dictCode) {
    return dictService.getListByDictCode(dictCode);
  }
}
