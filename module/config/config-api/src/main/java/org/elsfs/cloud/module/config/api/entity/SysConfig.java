/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.config.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 系统配置
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysConfig extends BaseEntity implements ConfigEntity {
  /** 配置ID */
  @TableId private String configId;

  /** 配置KEY */
  private String configKey;

  /** 配置名称 */
  private String configName;

  /** 配置值 */
  private String configValue;

  /** 配置类型 */
  private String configType;

  /** 配置描述 */
  private String remark;
}
