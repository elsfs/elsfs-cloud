/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.config.api.entity;

/**
 * 配置
 *
 * @author zeng
 */
public interface ConfigEntity {

  /**
   * 配置id
   *
   * @return 配置id
   */
  String getConfigId();

  /**
   * 配置名称
   *
   * @return 配置名称
   */
  String getConfigName();

  /**
   * 配置值
   *
   * @return 配置值
   */
  String getConfigValue();

  /**
   * 配置类型
   *
   * @return 配置类型
   */
  String getConfigType();

  /**
   * 配置键
   *
   * @return 配置键
   */
  String getConfigKey();

  /**
   * 配置描述
   *
   * @return 配置描述
   */
  String getRemark();
}
