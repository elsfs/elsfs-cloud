/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.config.biz.adapter.front;

import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.config.api.entity.SysConfig;
import org.elsfs.cloud.module.config.biz.repository.SysConfigRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 配置管理
 *
 * @author zeng
 */
@RestController
@RequestMapping("/q/sysConfig")
@RequiredArgsConstructor
@Tag(description = "sysConfig", name = "配置管理")
public class QSysConfigController {
  private final SysConfigRepository sysConfigRepository;

  @GetMapping("/getConfig/{configKey}")
  public R<SysConfig> getConfig(@PathVariable("configKey") String configKey) {
    return R.success(sysConfigRepository.getConfig(configKey));
  }

  @GetMapping("/getConfig/{configKey}/{configType}")
  public R<SysConfig> getConfigs(
      @PathVariable("configKey") String configKey, @PathVariable String configType) {
    return R.success(sysConfigRepository.getConfig(configKey, configType));
  }

  @GetMapping("/list")
  public R<List<SysConfig>> list() {
    return R.success(sysConfigRepository.list());
  }
}
