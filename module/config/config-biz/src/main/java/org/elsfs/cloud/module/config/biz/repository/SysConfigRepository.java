/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.config.biz.repository;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;
import org.elsfs.cloud.module.config.api.entity.SysConfig;

public interface SysConfigRepository extends IElsfsRepository<SysConfig, String> {
  default SysConfig getConfig(String configKey) {
    return getOne(Wrappers.lambdaQuery(SysConfig.class).eq(SysConfig::getConfigKey, configKey));
  }

  default SysConfig getConfig(String configKey, String configType) {
    return getOne(
        Wrappers.lambdaQuery(SysConfig.class)
            .eq(SysConfig::getConfigKey, configKey)
            .eq(SysConfig::getConfigType, configType));
  }
}
