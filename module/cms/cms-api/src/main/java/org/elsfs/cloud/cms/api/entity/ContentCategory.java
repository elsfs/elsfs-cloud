/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.cms.api.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseTreeEntity;

/**
 * 内容分类
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("cms_content_category")
public class ContentCategory extends BaseTreeEntity<ContentCategory, String> {
  @TableId
  @Schema(description = "分类ID")
  private String categoryId;

  @Schema(description = "父id")
  private String parentId;

  @Schema(description = "分类名称")
  @TableField(whereStrategy = FieldStrategy.NOT_EMPTY,condition = SqlCondition.LIKE)
  private String categoryName;

  @Schema(description = "图标")
  private String icon;

  @Schema(description = "描述")
  private String description;

  @Schema(description = "类型")
  private String type;

  @Schema(description = "排序值")
  private Integer sortValue;

  @Override
  public String getId() {
    return categoryId;
  }
}
