/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.cms.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 文章附件
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "文章附件")
@TableName("cms_attachment")
public class CmsAttachment extends BaseEntity {
  @TableId
  @Schema(description = "附件id")
  private String attachmentId;

  @Schema(description = "文章id")
  private String articleId;

  @Schema(description = "附件名称")
  private String fileName;

  @Schema(description = "附件url")
  private Long attachmentUrl;

  @Schema(description = "附件大小单位kb")
  private Long fileSize;
}
