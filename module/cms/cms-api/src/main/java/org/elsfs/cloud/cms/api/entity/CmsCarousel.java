/*
 * Copyright (c) 2023-2025 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.cms.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 轮播图
 *
 * @author zeng
 */
@Schema(description = "轮播图")
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsCarousel extends BaseEntity {
  @TableId private String carouselId;

  @Schema(description = "标题")
  private String title;

  @Schema(description = "图片url")
  private String imageUrl;

  @Schema(description = "描述")
  private String description;

  @Schema(description = "css")
  private String css;

  @Schema(description = "跳转的url")
  private String jumpUrl;

  @Schema(description = "排序值")
  private Integer sortValue;
}
