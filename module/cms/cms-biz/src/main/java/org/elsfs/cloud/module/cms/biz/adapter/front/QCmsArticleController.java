/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.cms.biz.adapter.front;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.cms.api.entity.CmsArticle;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.mybatis.utils.DateQuery;
import org.elsfs.cloud.module.cms.biz.repository.CmsArticleRepository;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章管理
 *
 * @author zeng
 */
@RestController
@RequestMapping("/q/article")
@RequiredArgsConstructor
@Tag(description = "article", name = "文章管理")
public class QCmsArticleController {
  private final CmsArticleRepository cmsArticleRepository;

  @GetMapping("/page")
  @Operation(summary = "分页查询文章", description = "分页查询文章")
  public R<IPage<CmsArticle>> page(
      @ParameterObject CmsArticle entity, @ParameterObject DateQuery<CmsArticle> qry) {
    IPage<CmsArticle> page = cmsArticleRepository.page(qry.toPage(), Wrappers.query(entity));
    return R.success(page);
  }
}
