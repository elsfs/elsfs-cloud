/*
 * Copyright (c) 2023-2025 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.cms.biz.adapter.admin;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.cms.api.entity.CmsCarousel;
import org.elsfs.cloud.cms.api.entity.ContentCategory;
import org.elsfs.cloud.common.controller.ControllerConfig;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.elsfs.cloud.common.controller.TreeLogicRepositoryCrudController;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.cms.biz.repository.CmsCarouselRepository;
import org.elsfs.cloud.module.cms.biz.repository.ContentCategoryRepository;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 轮播图管理
 *
 * @author zeng
 */
@RestController
@RequestMapping("/carousel")
@RequiredArgsConstructor
@Tag(description = "carousel", name = "轮播图管理")
public class CmsCarouselController
  extends LogicRepositoryCrudController<
    CmsCarousel,
    CmsCarousel,
    CmsCarousel,
    CmsCarousel,
    CmsCarouselRepository,
    String> {
  @Override
  protected ControllerConfig getConfig() {
    return super.getConfig().list(true);
  }
}

