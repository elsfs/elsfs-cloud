/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.cms.biz.adapter.front;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.cms.api.entity.CmsAttachment;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.cms.biz.repository.CmsAttachmentRepository;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章附件管理
 *
 * @author zeng
 */
@RestController
@RequestMapping("/q/cmsAttachment")
@RequiredArgsConstructor
@Tag(description = "cmsAttachment", name = "文章附件管理")
public class QCmsAttachmentController {
  private final CmsAttachmentRepository cmsAttachmentRepository;

  @GetMapping("/list")
  @Operation(summary = "文章附件查询", description = "文章附件查询")
  public R<List<CmsAttachment>> list(@ParameterObject CmsAttachment entity) {
    var list = cmsAttachmentRepository.list(Wrappers.query(entity));
    return R.success(list);
  }
}
