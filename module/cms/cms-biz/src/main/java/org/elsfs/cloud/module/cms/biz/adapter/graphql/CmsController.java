/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.cms.biz.adapter.graphql;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.cms.api.entity.ContentCategory;
import org.elsfs.cloud.module.cms.biz.repository.ContentCategoryRepository;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class CmsController {
  private final ContentCategoryRepository contentCategoryRepository;

  @QueryMapping
  public List<ContentCategory> allLogs() {
    return contentCategoryRepository.list();
  }
}
