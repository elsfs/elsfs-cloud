import { defHttp } from '@/utils/http/axios';
import { Result } from '#/axios';
import { Key } from 'ant-design-vue/lib/table/interface';

enum Api {
  list = '/tenant/list',
  add = '/tenant/add',
  edit = '/tenant/edit',
  del = '/tenant/del',
  exist = '/tenant/exist',
  validFlag = '/tenant/validFlag',
}

export type DataType = {
  record?: ${entity};
  isUpdate: boolean;
};
// 首先定义一个类型，用于描述数据的结构
export type ${entity} = {
## ----------  BEGIN 字段循环遍历  ----------
#foreach($field in ${table.fields})
    ${field.propertyName}
#end
};
export type ${entity}ValidationQry = {
  validationField: string;
  value: string;
  id?: string;
};

export const list = (params: ${entity}): Promise<${entity}> =>
  defHttp.get({ url: Api.list, params });
export const add = (params: ${entity}): Promise<boolean> => defHttp.post({ url: Api.add, params });
export const edit = (params: ${entity}) => defHttp.put({ url: Api.edit, params });
let timer;

export const exist = (params: ${entity}ValidationQry): Promise<Result> => {
  return new Promise((resolve, rejected) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      defHttp
        .post({ url: Api.exist, params }, { isTransformResponse: false })
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          rejected(error);
        });
    });
  });
};

/**
 * @description: 删除
 * @param params 删除参数 参数 string类型时，删除的 id， 参数 object 类型时，删除的 tenantId 为 object.tenantId
 */
export const del = (params: ${entity} | string) =>
  defHttp.delete({ url: Api.del + `/\${typeof params == 'string' ? params : params.tenantId}` });

export const batchDel = (params: Key[]) => defHttp.delete({ url: Api.del, params });

//启用
export const editValidFlag = (params: { ids: Key[]; validFlag: string }) =>
  defHttp.post({ url: Api.validFlag, params });
