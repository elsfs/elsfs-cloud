/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通用字段的填充策略和显示策略
 *
 * @author zeng
 */
@Getter
@AllArgsConstructor
public enum CommonColumnFiledEnum {

  /** create_by 字段 */
  create_by("0", "0", "INSERT", 100),

  /** create_at 字段 */
  create_at("0", "0", "INSERT", 101),
  /** update_by 字段 */
  update_by("0", "0", "INSERT_UPDATE", 102),
  /** update_at 字段 */
  update_at("0", "0", "INSERT_UPDATE", 103),
  /** delete_flag 字段 */
  delete_flag("0", "0", "DEFAULT", 104),
  /** tenant_id 字段 */
  tenant_id("0", "0", "DEFAULT", 105);

  /** 表单是否默认显示 */
  private final String formItem;

  /** 表格是否默认显示 */
  private final String gridItem;

  /** 自动填充策略 */
  private final String autoFill;

  /** 排序值 */
  private final Integer sort;
}
