/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.repository;

import java.util.Set;
import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;
import org.elsfs.cloud.module.datasource.biz.entity.GenFieldType;

/**
 * 列属性类型服务
 *
 * @author zeng
 */
public interface GenFieldTypeRepository extends IElsfsRepository<GenFieldType, String> {

  /**
   * 根据tableId，获取包列表
   *
   * @param dsName 数据源名称
   * @param tableName 表名称
   * @return 返回包列表
   */
  Set<String> getPackageByTableId(String dsName, String tableName);
}
