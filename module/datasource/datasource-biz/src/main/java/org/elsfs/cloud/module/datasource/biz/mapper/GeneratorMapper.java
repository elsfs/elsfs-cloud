/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.elsfs.cloud.module.datasource.biz.entity.GenTableColumn;
import org.elsfs.cloud.module.datasource.biz.vo.GenTableVo;
import org.elsfs.cloud.module.datasource.biz.vo.TableColumnVO;
import org.elsfs.cloud.module.datasource.biz.vo.TableVO;

/**
 * 代码生成器
 *
 * @author zeng
 */
@InterceptorIgnore(tenantLine = "true")
public interface GeneratorMapper {

  /**
   * 查询全部的表
   *
   * @return List
   */
  @InterceptorIgnore(tenantLine = "true")
  List<GenTableVo> queryTable();

  /**
   * 分页查询表格
   *
   * @param page 分页信息
   * @param tableName 表名称
   * @return page
   */
  @InterceptorIgnore(tenantLine = "true")
  IPage<GenTableVo> queryTable(
      IPage<GenTableVo> page, @Param("tableName") String tableName, @Param("dsName") String dsName);

  /**
   * 查询表信息
   *
   * @param tableName 表名称
   * @param dsName 数据源名称
   * @return VO
   */
  @InterceptorIgnore(tenantLine = "true")
  GenTableVo queryTable(@Param("tableName") String tableName, @Param("dsName") String dsName);

  /**
   * 查询表全部列信息
   *
   * @param tableName 表名称
   * @param dsName 数据源名称
   * @return LIST
   */
  @InterceptorIgnore(tenantLine = "true")
  List<GenTableColumn> selectMapTableColumn(
      @Param("tableName") String tableName, @Param("dsName") String dsName);

  @InterceptorIgnore(tenantLine = "true")
  IPage<TableVO> queryTableVo(IPage<TableVO> page, @Param("tableName") String tableName);

  List<TableColumnVO> queryTableColumnVo(@Param("tableName") String tableName);
}
