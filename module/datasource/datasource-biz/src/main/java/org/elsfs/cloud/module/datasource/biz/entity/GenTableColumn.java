/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 记录表字段的配置信息
 *
 * @author zeng
 */
@Data
@TableName("gen_table_column")
@EqualsAndHashCode(callSuper = true)
public class GenTableColumn extends BaseEntity {
  /** 主键 */
  @TableId(type = IdType.ASSIGN_ID)
  @Schema(description = "主键")
  private String id;

  /** 数据源名 */
  @Schema(description = "数据源名")
  private String dsName;

  /** 表名称 */
  @Schema(description = "表名称")
  private String tableName;

  /** 字段名称 */
  @Schema(description = "字段名称")
  private String fieldName;

  /** 排序 */
  @Schema(description = "排序")
  private Integer sort;

  /** 字段类型 */
  @Schema(description = "字段类型")
  private String fieldType;

  /** 字段说明 */
  @Schema(description = "字段说明")
  private String fieldComment;

  /** 属性名 */
  @Schema(description = "属性名")
  private String attrName;

  /** 属性类型 */
  @Schema(description = "属性类型")
  private String attrType;

  /** 属性包名 */
  @Schema(description = "属性包名")
  private String packageName;

  /** 自动填充 */
  @Schema(description = "自动填充")
  private String autoFill;

  /** 主键 0：否 1：是 */
  @Schema(description = "主键 0：否 1：是")
  private String primaryPk;

  /** 基类字段 0：否 1：是 */
  @Schema(description = "基类字段 0：否 1：是")
  private String baseField;

  /** 表单项 0：否 1：是 */
  @Schema(description = "表单项 0：否 1：是")
  private String formItem;

  /** 表单必填 0：否 1：是 */
  @Schema(description = "表单必填 0：否 1：是")
  private String formRequired;

  /** 表单类型 */
  @Schema(description = "表单类型")
  private String formType;

  /** 表单效验 */
  @Schema(description = "表单效验")
  private String formValidator;

  /** 列表项 0：否 1：是 */
  @Schema(description = "列表项 0：否 1：是")
  private String gridItem;

  /** 列表排序 0：否 1：是 */
  @Schema(description = "列表排序 0：否 1：是")
  private String gridSort;

  /** 查询项 0：否 1：是 */
  @Schema(description = "查询项 0：否 1：是")
  private String queryItem;

  /** 查询方式 */
  @Schema(description = "查询方式")
  private String queryType;

  /** 查询表单类型 */
  @Schema(description = "查询表单类型")
  private String queryFormType;

  /** 字段字典类型 */
  @TableField(updateStrategy = FieldStrategy.IGNORED)
  @Schema(description = "字段字典类型")
  private String fieldDict;
}
