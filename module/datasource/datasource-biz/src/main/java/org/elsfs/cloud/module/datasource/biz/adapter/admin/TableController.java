/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.adapter.admin;

import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.mybatis.utils.DateQuery;
import org.elsfs.cloud.common.util.lang.StringUtils;
import org.elsfs.cloud.module.datasource.biz.manager.GenTableManager;
import org.elsfs.cloud.module.datasource.biz.mapper.GeneratorMapper;
import org.elsfs.cloud.module.datasource.biz.vo.TableColumnVO;
import org.elsfs.cloud.module.datasource.biz.vo.TableVO;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 表管理
 *
 * @author zeng
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/table")
@Tag(description = "table", name = "表管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class TableController {
  @GetMapping("/page")
  @Operation(summary = "分页查询", description = "分页查询")
  public R<IPage<TableVO>> page(
      @ParameterObject TableVO entity, @ParameterObject DateQuery<TableVO> qry) {
    if (StringUtils.isBlank(entity.getDsName())) {
      return R.success(new Page<>());
    }
    GeneratorMapper mapper = GenTableManager.getMapper(entity.getDsName());
    DynamicDataSourceContextHolder.push(entity.getDsName());
    IPage<TableVO> page = mapper.queryTableVo(qry.toPage(), entity.getTableName());
    page.getRecords().forEach(item -> item.setDsName(entity.getDsName()));
    return R.success(page);
  }

  @GetMapping("/tableColumnList/{dsName}/{tableName}")
  @Operation(summary = "查询表属性列表", description = "查询表属性列表")
  public R<List<TableColumnVO>> tableColumnList(
      @PathVariable("tableName") String tableName, @PathVariable("dsName") String dsName) {
    GeneratorMapper mapper = GenTableManager.getMapper(dsName);
    DynamicDataSourceContextHolder.push(dsName);
    return R.success(mapper.queryTableColumnVo(tableName));
  }
}
