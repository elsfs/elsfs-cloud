/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 列属性
 *
 * @author zeng
 */
@Data
@TableName("gen_table")
@EqualsAndHashCode(callSuper = true)
@Schema(description = "列属性")
public class GenTable extends BaseEntity {

  /** id */
  @TableId(type = IdType.ASSIGN_ID)
  @Schema(description = "id")
  private String id;

  /** 数据源名称 */
  @Schema(description = "数据源名称")
  private String dsName;

  /** 数据源类型 */
  @Schema(description = "数据源类型")
  private String dbType;

  /** 表名 */
  @Schema(description = "表名")
  private String tableName;

  /** 类名 */
  @Schema(description = "类名")
  private String className;

  /** 说明 */
  @Schema(description = "表注释")
  private String tableComment;

  /** 作者 */
  @Schema(description = "作者")
  private String author;

  /** 邮箱 */
  @Schema(description = "邮箱")
  private String email;

  /** 项目包名 */
  @Schema(description = "项目包名")
  private String packageName;

  /** 项目版本号 */
  @Schema(description = "项目版本号")
  private String version;

  /** 生成方式 0：zip压缩包 1：自定义目录 */
  @Schema(description = "生成方式  0：zip压缩包   1：自定义目录")
  private String generatorType;

  /** 后端生成路径 */
  @Schema(description = "后端生成路径")
  private String backendPath;

  /** 前端生成路径 */
  @Schema(description = "前端生成路径")
  private String frontendPath;

  /** 模块名 */
  @Schema(description = "模块名")
  private String moduleName;

  /** 功能名 */
  @Schema(description = "功能名")
  private String functionName;

  /** 表单布局 1：一列 2：两列 */
  @Schema(description = "表单布局  1：一列   2：两列")
  private Integer formLayout;

  /** 表主键 */
  @Schema(description = "表主键")
  private String primaryKey;

  /** 代码生成风格 */
  private Long style;
}
