/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.adapter.admin;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.datasource.biz.entity.GenGroup;
import org.elsfs.cloud.module.datasource.biz.manager.GenGroupManager;
import org.elsfs.cloud.module.datasource.biz.repository.GenGroupRepository;
import org.elsfs.cloud.module.datasource.biz.vo.GroupVo;
import org.elsfs.cloud.module.datasource.biz.vo.TemplateGroupVO;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 模板分组
 *
 * @author zeng
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/genGroup")
@Tag(description = "genGroup", name = "模板分组管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class GenGroupController
    extends LogicRepositoryCrudController<
        GenGroup, TemplateGroupVO, TemplateGroupVO, GroupVo, GenGroupRepository, String> {
  private final GenGroupManager genGroupManager;

  @Override
  public R<GroupVo> getById(String id) {
    return R.success(repository.getGroupVoById(id));
  }

  public R<Boolean> add(TemplateGroupVO genTemplateGroup) {
    genGroupManager.saveGenGroup(genTemplateGroup);
    return R.success();
  }

  /**
   * 修改模板分组
   *
   * @param groupVo 模板分组
   * @return R
   */
  public R<Boolean> edit(GroupVo groupVo) {
    genGroupManager.updateGroupAndTemplateById(groupVo);
    return R.success();
  }

  @Override
  public R<Boolean> removeById(String id) {
    genGroupManager.delGroupAndTemplate(List.of(id));
    return R.success();
  }

  @Override
  public R<Boolean> removeBatchByIds(List<String> ids) {
    genGroupManager.delGroupAndTemplate(ids);
    return R.success();
  }
}
