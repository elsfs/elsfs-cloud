/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.vo;

import lombok.Data;

/**
 * 表信息 --列信息
 *
 * @author zeng
 */
@Data
public class TableColumnVO {
  private String tableCatalog; //  -- 目录
  private String tableSchema; //  -- 模式 库名
  private String tableName; //  -- 表名
  private String columnName; //  -- 字段名
  private String ordinalPosition; //  -- 字段位置 排序
  private String columnDefault; //  -- 默认值
  private String isNullable; //  -- 是否为空
  private String dataType; //  -- 字段类型
  private String characterMaximumLength; // ; -- 最大长度
  private String characterOctetLength; //  -- 字节长度
  private String numericPrecision; //  -- 精度
  private String numericScale; //  -- 小数位数
  private String datetimePrecision; //  -- 日期精度
  private String characterSetName; //  -- 字符集
  private String collationName; //  -- 排序规则
  private String columnType; //  -- 字段类型
  private String columnKey; //  -- 字段键值 pri 主键 mul 多重索引
  private String extra; //  -- 额外信息 自增
  private String privileges; //  -- 权限
  private String columnComment; //  -- 字段描述
  private String generationExpression; //  -- 表达式
  private String srsId; //  -- 空间 id
}
