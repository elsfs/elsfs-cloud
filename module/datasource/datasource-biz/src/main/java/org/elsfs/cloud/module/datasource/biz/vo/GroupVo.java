/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.module.datasource.biz.entity.GenGroup;
import org.elsfs.cloud.module.datasource.biz.entity.GenTemplate;

/**
 * 分组传输对象
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GroupVo extends GenGroup {

  /** 模板ids */
  @Schema(description = "拥有的模板列表")
  private List<String> templateIds;

  /**
   * 获取模板id集合
   *
   * @return 模板id集合
   */
  public List<String> getTemplateIds() {
    if (templateIds == null) {
      return templateList.stream().map(GenTemplate::getId).toList();
    }
    return templateIds;
  }

  /** 模板列表 */
  @Schema(description = "拥有的模板列表")
  private List<GenTemplate> templateList;
}
