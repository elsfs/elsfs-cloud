/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.adapter.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.controller.ControllerConfig;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.mybatis.utils.DateQuery;
import org.elsfs.cloud.module.datasource.biz.entity.GenTable;
import org.elsfs.cloud.module.datasource.biz.entity.GenTableColumn;
import org.elsfs.cloud.module.datasource.biz.manager.GenTableManager;
import org.elsfs.cloud.module.datasource.biz.repository.GenTableRepository;
import org.elsfs.cloud.module.datasource.biz.vo.GenTableVo;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 表管理
 *
 * @author zeng
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/genTable")
@Tag(description = "genTable", name = "列属性管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class GenTableController
    extends LogicRepositoryCrudController<
        GenTable, GenTable, GenTable, GenTable, GenTableRepository, String> {

  private final GenTableManager genTableManager;

  @Override
  protected ControllerConfig getConfig() {
    return super.getConfig().page(true);
  }

  @Operation(summary = "分页查询", description = "分页查询")
  @GetMapping("/pageMap")
  public R<IPage<GenTableVo>> getTablePage(
      @ParameterObject GenTableVo entity, @ParameterObject DateQuery<GenTableVo> qry) {

    return R.success(genTableManager.page(qry.toPage(), entity));
  }

  /**
   * 获取表信息
   *
   * @param dsName 数据源
   * @param tableName 表名称
   */
  @GetMapping("/tableInfo/{dsName}/{tableName}")
  public R<GenTable> info(
      @PathVariable("dsName") String dsName, @PathVariable("tableName") String tableName) {
    return R.success(genTableManager.queryOrBuildTable(dsName, tableName));
  }

  /**
   * 获取所有表
   *
   * @param dsName 数据源名称
   * @return r
   */
  @Operation(summary = "获取所有表", description = "获取所有表")
  @GetMapping("/list/{dsName}")
  public R<List<GenTableVo>> listTable(@PathVariable("dsName") String dsName) {
    return R.success(genTableManager.queryDsAllTable(dsName));
  }

  @GetMapping("/column/{dsName}/{tableName}")
  public R<List<GenTableColumn>> column(
      @PathVariable("dsName") String dsName, @PathVariable("tableName") String tableName) {
    return R.success(genTableManager.queryColumn(dsName, tableName));
  }

  /**
   * 同步表信息
   *
   * @param dsName 数据源
   * @param tableName 表名称
   */
  @GetMapping("/sync/{dsName}/{tableName}")
  public R<GenTableVo> sync(
      @PathVariable("dsName") String dsName, @PathVariable("tableName") String tableName) {
    return R.success(genTableManager.sync(dsName, tableName));
  }

  /**
   * 修改表字段数据
   *
   * @param dsName 数据源
   * @param tableName 表名称
   * @param tableFieldList 字段列表
   */
  @PutMapping("/field/{dsName}/{tableName}")
  public R<Boolean> updateTableField(
      @PathVariable("dsName") String dsName,
      @PathVariable String tableName,
      @RequestBody List<GenTableColumn> tableFieldList) {
    return R.success(genTableManager.updateTableField(dsName, tableName, tableFieldList));
  }
}
