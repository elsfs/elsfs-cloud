/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.repository;

import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;
import org.elsfs.cloud.module.datasource.biz.entity.GenTable;
import org.elsfs.cloud.module.datasource.biz.vo.GenTableVo;

/**
 * 列属性
 *
 * @author zeng
 */
public interface GenTableRepository extends IElsfsRepository<GenTable, String> {

  /**
   * 获取表信息
   *
   * @param dsName 数据源名称
   * @param tableName tableName
   * @return GenTableVo
   */
  GenTableVo getGenTableVoByDsNameAndTableName(String dsName, String tableName);

  /**
   * 根据id获取vo
   *
   * @param id id
   * @return vo
   */
  GenTableVo getVoByIdVo(String id);
}
