/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 代码生成默认配置类
 *
 * @author zeng
 */
@Data
@Configuration(proxyBeanMethods = false)
@ConfigurationProperties(prefix = CodeGenProperties.PREFIX)
public class CodeGenProperties {
  public static final String PREFIX = "codegen";

  /** 生成代码的包名 */
  private String packageName = "org.elsfs.cloud";

  /** 生成代码的版本 */
  private String version = "1.0.0";

  /** 生成代码的后端路径 */
  private String backendPath = "zeng";

  /** 生成代码的前端路径 */
  private String frontendPath = "elsfs-cloud-ui";

  /** 生成代码的作者 */
  private String author = "zeng";

  /** 生成代码的邮箱 */
  private String email = "2679652842@qq.com";

  /** 表单布局（一列、两列） */
  private Integer formLayout = 2;

  /** 生成代码的模块名 */
  private String moduleName = "admin";

  /** 下载方式 （0 文件下载、1写入目录） */
  private String generatorType = "0";
}
