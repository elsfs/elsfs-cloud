/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.repository.impl;

import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.mybatis.repository.ElsfsCrudRepositoryImpl;
import org.elsfs.cloud.module.datasource.biz.entity.GenTable;
import org.elsfs.cloud.module.datasource.biz.mapper.GenTableMapper;
import org.elsfs.cloud.module.datasource.biz.repository.GenTableRepository;
import org.elsfs.cloud.module.datasource.biz.vo.GenTableVo;
import org.springframework.stereotype.Service;

/** 列属性 */
@Service
@RequiredArgsConstructor
public class GenTableRepositoryImpl
    extends ElsfsCrudRepositoryImpl<GenTableMapper, GenTable, String>
    implements GenTableRepository {

  @Override
  public GenTableVo getGenTableVoByDsNameAndTableName(String dsName, String tableName) {
    return baseMapper.queryGenTableVoByDsNameAndTableName(dsName, tableName);
  }

  @Override
  public GenTableVo getVoByIdVo(String id) {
    return baseMapper.selectVoById(id);
  }
}
