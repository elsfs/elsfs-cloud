/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.vo;

import lombok.Data;

/**
 * 表信息
 *
 * @author zeng
 */
@Data
public class TableVO {
  private String dsName; // -- 数据源名称
  private String primaryKey; // -- 主键
  private String tableCatalog; //  -- 表目录
  private String tableSchema; // -- 表模式 (数据库模式) elsfs-admin
  private String tableName; // -- 表名
  private String tableType; // -- 表类型
  private String engine; // -- 存储引擎
  private String version; // -- 版本
  private String rowFormat; // -- 行格式Dynamic
  private String tableRows; // -- 行数
  private String avgRowLength; // -- 平均行长度
  private String dataLength; // -- 数据长度
  private String maxDataLength; // -- 最大行长度
  private String indexLength; // -- 索引长度
  private String dataFree; // -- 未使用长度
  private String autoIncrement; // -- 自动递增
  private String createTime; // -- 创建时间
  private String updateTime; // -- 更新时间
  private String checkTime; // -- 检查时间
  private String tableCollation; // -- 字符集
  private String checksum; // -- 校验和
  private String createOptions; //  -- 创建选项
  private String tableComment; //  表注释
}
