/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.manager;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import java.util.LinkedList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.module.datasource.biz.entity.GenTemplateGroup;
import org.elsfs.cloud.module.datasource.biz.repository.GenGroupRepository;
import org.elsfs.cloud.module.datasource.biz.repository.GenTemplateGroupRepository;
import org.elsfs.cloud.module.datasource.biz.vo.GroupVo;
import org.elsfs.cloud.module.datasource.biz.vo.TemplateGroupVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 模板分组 管理
 *
 * @author zeng
 */
@Service
@RequiredArgsConstructor
public class GenGroupManager {

  private final GenGroupRepository genGroupRepository;
  private final GenTemplateGroupRepository genTemplateGroupRepository;

  /**
   * 新增模板分组
   *
   * @param genTemplateGroup 模板分组
   */
  public void saveGenGroup(TemplateGroupVO genTemplateGroup) {

    genGroupRepository.save(genTemplateGroup);
    // 2.保存关系
    List<GenTemplateGroup> goals = new LinkedList<>();
    for (String templateId : genTemplateGroup.getTemplateIds()) {
      GenTemplateGroup templateGroup = new GenTemplateGroup();
      templateGroup.setTemplateId(templateId).setGroupId(genTemplateGroup.getId());
      goals.add(templateGroup);
    }
    genTemplateGroupRepository.saveBatch(goals);
  }

  /**
   * 按照ids删除
   *
   * @param ids groupIds
   */
  @Transactional(rollbackFor = Exception.class)
  public void delGroupAndTemplate(List<String> ids) {
    // 删除分组
    genGroupRepository.removeByIds(ids);
    // 删除关系
    genTemplateGroupRepository.remove(
        Wrappers.<GenTemplateGroup>lambdaQuery().in(GenTemplateGroup::getGroupId, ids));
  }

  /**
   * 根据id更新
   *
   * @param groupVo groupVo
   */
  public void updateGroupAndTemplateById(GroupVo groupVo) {
    // 1.更新自身

    genGroupRepository.updateById(groupVo);
    // 2.更新模板
    // 2.1根据id删除之前的模板
    genTemplateGroupRepository.remove(
        Wrappers.<GenTemplateGroup>lambdaQuery().eq(GenTemplateGroup::getGroupId, groupVo.getId()));
    // 2.2根据ids创建新的模板分组赋值
    List<GenTemplateGroup> goals = new LinkedList<>();
    for (String templateId : groupVo.getTemplateIds()) {
      goals.add(new GenTemplateGroup().setGroupId(groupVo.getId()).setTemplateId(templateId));
    }
    genTemplateGroupRepository.saveBatch(goals);
  }
}
