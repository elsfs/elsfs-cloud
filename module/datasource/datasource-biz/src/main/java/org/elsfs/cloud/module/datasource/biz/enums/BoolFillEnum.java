/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/** boolean 类型枚举 */
@Getter
@RequiredArgsConstructor
public enum BoolFillEnum {

  /** true */
  TRUE("1"),
  /** false */
  FALSE("0");

  private final String value;
}
