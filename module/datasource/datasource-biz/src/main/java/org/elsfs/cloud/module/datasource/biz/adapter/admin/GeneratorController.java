/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.adapter.admin;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.datasource.biz.manager.GeneratorDocManager;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 代码生成器
 *
 * @author zeng
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/generator")
public class GeneratorController {

  private final GeneratorDocManager generatorDocManager;

  /**
   * ZIP 下载生成代码
   *
   * @param tableIds 数据表ID
   * @param response 流输出对象
   */
  @SneakyThrows
  @GetMapping("/download")
  public void download(String tableIds, HttpServletResponse response) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    ZipOutputStream zip = new ZipOutputStream(outputStream);

    // 生成代码
    for (String tableId : tableIds.split(",")) {
      generatorDocManager.downloadCode(tableId, zip);
    }
    zip.close();

    // zip压缩包数据
    byte[] data = outputStream.toByteArray();

    response.reset();
    response.setHeader(
        HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s.zip", tableIds));
    response.addHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(data.length));
    response.setContentType("application/octet-stream; charset=UTF-8");
    // 将response.getOutputStream() 写到 data
    ServletOutputStream responseOutputStream = response.getOutputStream();
    responseOutputStream.write(data);
  }

  /** 目标目录生成代码 */
  @ResponseBody
  @GetMapping("/code")
  public R<String> code(String tableIds) throws Exception {
    // 生成代码
    for (String tableId : tableIds.split(",")) {
      generatorDocManager.generatorCode(tableId);
    }

    return R.success();
  }

  /**
   * 预览代码
   *
   * @param tableId 表ID
   * @return 预览数据
   */
  @SneakyThrows
  @GetMapping("/preview")
  public List<Map<String, String>> preview(String tableId) {
    return generatorDocManager.preview(tableId);
  }
}
