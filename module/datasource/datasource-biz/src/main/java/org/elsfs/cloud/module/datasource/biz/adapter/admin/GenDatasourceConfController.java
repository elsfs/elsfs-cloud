/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.adapter.admin;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.elsfs.cloud.common.controller.ControllerConfig;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.datasource.biz.manager.GeneratorDocManager;
import org.elsfs.cloud.module.dict.api.entity.GenDatasourceConf;
import org.elsfs.cloud.module.dict.api.repository.GenDatasourceConfRepository;
import org.elsfs.cloud.screw.engine.EngineFileType;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 数据源管理
 *
 * @author zeng
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/genDatasourceConf")
@Tag(description = "genDatasourceConf", name = "数据源管理")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class GenDatasourceConfController
    extends LogicRepositoryCrudController<
        GenDatasourceConf,
        GenDatasourceConf,
        GenDatasourceConf,
        GenDatasourceConf,
        GenDatasourceConfRepository,
        String> {

  private final GeneratorDocManager generatorDocManager;

  @Override
  protected ControllerConfig getConfig() {
    return super.getConfig().list(true);
  }

  @Override
  public R<Boolean> add(GenDatasourceConf co) {
    return R.success(repository.saveDsByEnc(co));
  }

  @Override
  public R<Boolean> edit(GenDatasourceConf co) {
    return R.success(repository.updateDsByEnc(co));
  }

  /**
   * 查询数据源对应的文档
   *
   * @param dsName 数据源名称
   */
  @GetMapping("/doc")
  @SneakyThrows
  public void generatorDoc(@RequestParam("dsName") String dsName, HttpServletResponse response) {
    ByteArrayOutputStream byteArrayOutputStream = generatorDocManager.generatorDoc(dsName);
    response.reset();
    response.addHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(byteArrayOutputStream.size()));
    response.setContentType("application/octet-stream");
    ServletOutputStream outputStream = response.getOutputStream();
    outputStream.write(
        byteArrayOutputStream.toString(StandardCharsets.UTF_8).getBytes(StandardCharsets.UTF_8));
  }

  @GetMapping("/doc.html")
  public ResponseEntity<String> generatorDocHtml(
      @RequestParam("dsName") String dsName, ModelAndView view) {
    ByteArrayOutputStream doc = generatorDocManager.generatorDoc(dsName, EngineFileType.HTML);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.TEXT_HTML)
        .body(doc.toString(StandardCharsets.UTF_8));
  }
}
