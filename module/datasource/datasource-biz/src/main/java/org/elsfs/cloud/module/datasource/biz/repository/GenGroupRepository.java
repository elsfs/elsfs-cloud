/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.datasource.biz.repository;

import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;
import org.elsfs.cloud.module.datasource.biz.entity.GenGroup;
import org.elsfs.cloud.module.datasource.biz.vo.GroupVo;

/**
 * 模板分组
 *
 * @author zeng
 */
public interface GenGroupRepository extends IElsfsRepository<GenGroup, String> {

  /**
   * 查询group数据
   *
   * @param id id
   * @return GroupVo
   */
  GroupVo getGroupVoById(String id);
}
