/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.constant;

/**
 * 默认常量
 *
 * @author zeng
 */
public interface ScrewConstants {
  /** 名称 */
  String NAME = "screw";

  /** 百分号 */
  String PERCENT_SIGN = "%";

  /** 暂未支持 */
  String NOT_SUPPORTED = "Not supported yet!";

  /** 默认字符集 */
  String DEFAULT_ENCODING = "UTF-8";

  /** 默认国际化 */
  String DEFAULT_LOCALE = "zh_CN";

  /** Mac */
  String MAC = "Mac";

  /** Windows */
  String WINDOWS = "Windows";

  /** 小数点0 */
  String ZERO_DECIMAL_DIGITS = "0";

  /** 默认描述 */
  String DESCRIPTION = "数据库设计文档";

  /** mysql useInformationSchema */
  String USE_INFORMATION_SCHEMA = "useInformationSchema";

  /** oracle 连接参数备注 */
  String ORACLE_REMARKS = "remarks";

  /** 日志开始 */
  String LOGGER_BEGINS = "Database design document generation begins 🚀";

  /** 日志结束 */
  String LOGGER_COMPLETE =
      "Database design document generation is complete , time cost:%s second 🎇";

  /** 零 */
  String ZERO = "0";

  /** N */
  String N = "N";

  /** Y */
  String Y = "Y";

  /** phoenix 命名空间 */
  String PHOENIX_NAMESPACE_MAPPING = "phoenix.schema.isNamespaceMappingEnabled";

  /** phoenix 系统命名空间 */
  String PHOENIX_SYS_NAMESPACE_MAPPING = "phoenix.schema.mapSystemTablesToNamespace";
}
