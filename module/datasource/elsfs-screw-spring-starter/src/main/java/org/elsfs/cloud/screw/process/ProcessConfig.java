/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.process;

import java.io.Serializable;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * 数据处理
 *
 * @author zeng
 */
@Data
@Builder
public class ProcessConfig implements Serializable {
  /** 忽略表名 */
  private List<String> ignoreTableName;

  /** 忽略表前缀 */
  private List<String> ignoreTablePrefix;

  /** 忽略表后缀 */
  private List<String> ignoreTableSuffix;

  /** 指定生成表名 */
  private List<String> designatedTableName;

  /** 指定生成表前缀 */
  private List<String> designatedTablePrefix;

  /** 指定生成表后缀 */
  private List<String> designatedTableSuffix;
}
