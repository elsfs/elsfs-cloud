/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.query.phoenix.model;

import java.io.Serial;
import lombok.Data;
import org.elsfs.cloud.screw.mapping.MappingField;
import org.elsfs.cloud.screw.metadata.PrimaryKey;

/**
 * 表主键
 *
 * @author zeng
 */
@Data
public class PhoenixPrimaryKeyModel implements PrimaryKey {

  @Serial private static final long serialVersionUID = -4908250184995248600L;

  /** 主键名称 */
  @MappingField(value = "PK_NAME")
  private String pkName;

  /** tableSchem */
  @MappingField(value = "TABLE_SCHEM")
  private String tableSchem;

  /** keySeq */
  @MappingField(value = "KEY_SEQ")
  private String keySeq;

  /** tableCat */
  @MappingField(value = "TABLE_CAT")
  private String tableCat;

  /** 列名 */
  @MappingField(value = "COLUMN_NAME")
  private String columnName;

  /** 表名 */
  @MappingField(value = "TABLE_NAME")
  private String tableName;
}
