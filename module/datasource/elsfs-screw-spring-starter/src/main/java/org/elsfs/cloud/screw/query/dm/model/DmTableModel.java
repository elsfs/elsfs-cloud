/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.query.dm.model;

import lombok.Data;
import org.elsfs.cloud.screw.mapping.MappingField;
import org.elsfs.cloud.screw.metadata.Table;

/**
 * oracle table
 *
 * @author zeng
 */
@Data
public class DmTableModel implements Table {
  /** TABLE_CAT */
  @MappingField(value = "TABLE_CAT")
  private String tableCat;

  /** 表名 */
  @MappingField(value = "TABLE_NAME")
  private String tableName;

  /** 表模式 */
  @MappingField(value = "TABLE_SCHEM")
  private String tableSchem;

  /** 表类型 */
  @MappingField(value = "TABLE_TYPE")
  private String tableType;

  /** 备注 */
  @MappingField(value = "REMARKS")
  private String remarks;
}
