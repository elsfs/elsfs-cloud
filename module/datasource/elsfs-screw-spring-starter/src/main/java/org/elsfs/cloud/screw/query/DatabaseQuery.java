/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.query;

import java.io.Serializable;
import java.util.List;
import org.elsfs.cloud.common.util.exception.QueryException;
import org.elsfs.cloud.screw.metadata.Column;
import org.elsfs.cloud.screw.metadata.Database;
import org.elsfs.cloud.screw.metadata.PrimaryKey;
import org.elsfs.cloud.screw.metadata.Table;

/**
 * 通用查询接口 查询数据库信息
 *
 * @author zeng
 */
public interface DatabaseQuery extends Serializable {
  /**
   * 获取数据库
   *
   * @return {@link Database} 数据库信息
   * @throws QueryException QueryException
   */
  Database getDataBase() throws QueryException;

  /**
   * 获取表信息
   *
   * @return {@link List} 所有表信息
   * @throws QueryException QueryException
   */
  List<? extends Table> getTables() throws QueryException;

  /**
   * 获取列信息
   *
   * @param table {@link String} 表名
   * @return {@link List} 表字段信息
   * @throws QueryException QueryException
   */
  List<? extends Column> getTableColumns(String table) throws QueryException;

  /**
   * 获取所有列信息
   *
   * @return {@link List} 表字段信息
   * @throws QueryException QueryException
   */
  List<? extends Column> getTableColumns() throws QueryException;

  /**
   * 根据表名获取主键
   *
   * @param table {@link String}
   * @return {@link List}
   * @throws QueryException QueryException
   */
  List<? extends PrimaryKey> getPrimaryKeys(String table) throws QueryException;

  /**
   * 获取主键
   *
   * @return {@link List}
   * @throws QueryException QueryException
   */
  List<? extends PrimaryKey> getPrimaryKeys() throws QueryException;
}
