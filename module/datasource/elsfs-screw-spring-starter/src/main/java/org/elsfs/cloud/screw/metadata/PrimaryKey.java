/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.metadata;

import java.io.Serializable;

/**
 * 主键
 *
 * @author zeng
 */
public interface PrimaryKey extends Serializable {
  /**
   * 表名
   *
   * @return {@link String}
   */
  String getTableName();

  /**
   * 主键名称
   *
   * @return {@link String}
   */
  String getPkName();

  /**
   * 列名
   *
   * @return {@link String}
   */
  String getColumnName();

  /**
   * 关键序列
   *
   * @return {@link String}
   */
  String getKeySeq();
}
