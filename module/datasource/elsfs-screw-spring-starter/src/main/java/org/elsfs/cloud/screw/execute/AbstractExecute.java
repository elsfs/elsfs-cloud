/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.execute;

import static org.elsfs.cloud.screw.constant.ScrewConstants.DESCRIPTION;

import org.elsfs.cloud.common.util.lang.Assert;
import org.elsfs.cloud.common.util.lang.StringUtils;
import org.elsfs.cloud.screw.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 抽象执行
 *
 * @author zeng
 */
public abstract class AbstractExecute implements Execute {
  /** LOGGER */
  final Logger logger = LoggerFactory.getLogger(this.getClass());

  protected Configuration config;

  public AbstractExecute(Configuration config) {
    Assert.notNull(config, "Configuration can not be empty!");
    this.config = config;
  }

  /**
   * 获取文档名称
   *
   * @param database {@link String}
   * @return {@link String} 名称
   */
  String getDocName(String database) {
    // 自定义文件名称不为空
    if (StringUtils.isNotBlank(config.getEngineConfig().getFileName())) {
      return config.getEngineConfig().getFileName();
    }
    // 描述
    String description = config.getDescription();
    if (StringUtils.isBlank(description)) {
      description = DESCRIPTION;
    }
    // 版本号
    String version = config.getVersion();
    if (StringUtils.isBlank(version)) {
      return database + "_" + description;
    }
    return database + "_" + description + "_" + version;
  }
}
