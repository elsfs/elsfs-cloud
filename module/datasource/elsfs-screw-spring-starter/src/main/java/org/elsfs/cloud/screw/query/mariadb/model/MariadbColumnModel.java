/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.query.mariadb.model;

import lombok.Data;
import org.elsfs.cloud.screw.mapping.MappingField;
import org.elsfs.cloud.screw.metadata.Column;

/**
 * 表字段信息
 *
 * @author zeng
 */
@Data
public class MariadbColumnModel implements Column {

  /** scopeTable */
  @MappingField(value = "SCOPE_TABLE")
  private Object scopeTable;

  /** tableCat */
  @MappingField(value = "TABLE_CAT")
  private String tableCat;

  /** bufferLength */
  @MappingField(value = "BUFFER_LENGTH")
  private String bufferLength;

  /** isNullable */
  @MappingField(value = "IS_NULLABLE")
  private String isNullable;

  /** tableName */
  @MappingField(value = "TABLE_NAME")
  private String tableName;

  /** columnDef */
  @MappingField(value = "COLUMN_DEF")
  private String columnDef;

  /** scopeCatalog */
  @MappingField(value = "SCOPE_CATALOG")
  private Object scopeCatalog;

  /** tableSchem */
  @MappingField(value = "TABLE_SCHEM")
  private Object tableSchem;

  /** columnName */
  @MappingField(value = "COLUMN_NAME")
  private String columnName;

  /** nullable */
  @MappingField(value = "NULLABLE")
  private String nullable;

  /** remarks */
  @MappingField(value = "REMARKS")
  private String remarks;

  /** decimalDigits */
  @MappingField(value = "DECIMAL_DIGITS")
  private String decimalDigits;

  /** numPrecRadix */
  @MappingField(value = "NUM_PREC_RADIX")
  private String numPrecRadix;

  /** sqlDatetimeSub */
  @MappingField(value = "SQL_DATETIME_SUB")
  private String sqlDatetimeSub;

  /** isGeneratedColumn */
  @MappingField(value = "IS_GENERATEDCOLUMN")
  private String isGeneratedColumn;

  /** isAutoIncrement */
  @MappingField(value = "IS_AUTOINCREMENT")
  private String isAutoIncrement;

  /** sqlDataType */
  @MappingField(value = "SQL_DATA_TYPE")
  private String sqlDataType;

  /** charOctetLength */
  @MappingField(value = "CHAR_OCTET_LENGTH")
  private String charOctetLength;

  /** ordinalPosition */
  @MappingField(value = "ORDINAL_POSITION")
  private String ordinalPosition;

  /** scopeSchema */
  @MappingField(value = "SCOPE_SCHEMA")
  private Object scopeSchema;

  /** sourceDataType */
  @MappingField(value = "SOURCE_DATA_TYPE")
  private Object sourceDataType;

  /** dataType */
  @MappingField(value = "DATA_TYPE")
  private String dataType;

  /** typeName */
  @MappingField(value = "TYPE_NAME")
  private String typeName;

  /**
   * 列表示给定列的指定列大小。 对于数值数据，这是最大精度。 对于字符数据，这是字符长度。 对于日期时间数据类型，这是 String 表示形式的字符长度（假定允许的最大小数秒组件的精度）。
   * 对于二进制数据，这是字节长度。 对于 ROWID 数据类型，这是字节长度。对于列大小不适用的数据类型，则返回 Null。
   */
  @MappingField(value = "COLUMN_SIZE")
  private String columnSize;

  /** 是否主键 */
  private String primaryKey;

  /** 列类型（带长度） */
  @MappingField(value = "COLUMN_TYPE")
  private String columnType;

  /** 列长度 */
  @MappingField(value = "COLUMN_LENGTH")
  private String columnLength;
}
