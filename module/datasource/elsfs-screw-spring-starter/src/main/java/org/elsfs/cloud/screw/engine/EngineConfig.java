/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.engine;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 * 文件生成配置
 *
 * @author zeng
 */
@Data
@Builder
public class EngineConfig implements Serializable {
  /** 是否打开输出目录 */
  private boolean openOutputDir;

  /** 文件产生位置 */
  private String fileOutputDir;

  /** 生成文件类型 */
  private EngineFileType fileType;

  /** 生成实现 */
  private EngineTemplateType produceType;

  /** 自定义模板，模板需要和文件类型和使用模板的语法进行编写和处理，否则将会生成错误 */
  private String customTemplate;

  /** 文件名称 */
  private String fileName;
}
