/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.spring;

import java.io.ByteArrayOutputStream;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.screw.Configuration;
import org.elsfs.cloud.screw.engine.EngineConfig;
import org.elsfs.cloud.screw.execute.DocumentationExecute;
import org.elsfs.cloud.screw.process.ProcessConfig;

/**
 * Screw
 *
 * @author zeng
 */
@RequiredArgsConstructor
public class Screw {
  private final ScrewProperties screwProperties;

  /**
   * 生成文档 （文件形式）
   *
   * @param dataSource 目标数据源信息
   * @return ByteArrayOutputStream
   */
  public ByteArrayOutputStream documentGeneration(String dsName, DataSource dataSource) {
    return this.documentGeneration(dsName, dataSource, screwProperties);
  }

  /**
   * 生成文档 （文件形式）
   *
   * @param dataSource 目标数据源信息
   * @param screwProperties 配置信息
   * @return ByteArrayOutputStream
   */
  public ByteArrayOutputStream documentGeneration(
      String dsName, DataSource dataSource, ScrewProperties screwProperties) {
    // 总配置
    Configuration config =
        Configuration.builder()
            // 组织
            .organization(screwProperties.getOrganization())
            // url
            .organizationUrl(screwProperties.getOrganizationUrl())
            // 标题
            .title(screwProperties.getTitle())
            // 版本
            .version(screwProperties.getVersion())
            // 描述
            .description(screwProperties.getDescription())
            // 数据源
            .dataSource(dataSource)
            // 引擎模板配置
            .engineConfig(getEngineConfig())
            // 数据处理配置
            .produceConfig(getProcessConfig())
            .build();
    // 生成文档
    return new DocumentationExecute(config).execute(dsName);
  }

  /**
   * 引擎模板配置
   *
   * @return {@link EngineConfig}
   */
  private EngineConfig getEngineConfig() {
    return EngineConfig.builder()
        // 文件类型
        .fileType(screwProperties.getFileType())
        // 生成模板实现
        .produceType(screwProperties.getProduceType())
        // 自定义模板位置
        .customTemplate(screwProperties.getTemplate())
        // 文件名称
        .fileName(screwProperties.getFileName())
        .build();
  }

  /**
   * 数据处理配置
   *
   * @return {@link ProcessConfig}
   */
  private ProcessConfig getProcessConfig() {
    return ProcessConfig.builder()
        // 忽略表名
        .ignoreTableName(screwProperties.getIgnoreTableName())
        // 忽略表前缀
        .ignoreTablePrefix(screwProperties.getIgnoreTablePrefix())
        // 忽略表后缀
        .ignoreTableSuffix(screwProperties.getIgnoreTableSuffix())
        // 指定生成表名
        .designatedTableName(screwProperties.getDesignatedTableName())
        // 指定生成表前缀
        .designatedTablePrefix(screwProperties.getDesignatedTablePrefix())
        // 指定生成表后缀
        .designatedTableSuffix(screwProperties.getDesignatedTableSuffix())
        .build();
  }
}
