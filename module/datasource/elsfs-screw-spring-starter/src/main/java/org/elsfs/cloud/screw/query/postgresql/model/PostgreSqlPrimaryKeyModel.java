/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.query.postgresql.model;

import lombok.Data;
import org.elsfs.cloud.screw.mapping.MappingField;
import org.elsfs.cloud.screw.metadata.PrimaryKey;

/**
 * 表主键
 *
 * @author zeng
 */
@Data
public class PostgreSqlPrimaryKeyModel implements PrimaryKey {

  private static final long serialVersionUID = -4908250184995248600L;

  /** 主键名称 */
  @MappingField(value = "pk_name")
  private String pkName;

  /** tableSchem */
  @MappingField(value = "table_schem")
  private String tableSchem;

  /** keySeq */
  @MappingField(value = "key_seq")
  private String keySeq;

  /** tableCat */
  @MappingField(value = "table_cat")
  private String tableCat;

  /** 列名 */
  @MappingField(value = "column_name")
  private String columnName;

  /** 表名 */
  @MappingField(value = "table_name")
  private String tableName;
}
