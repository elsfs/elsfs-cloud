/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.engine;

import java.io.ByteArrayOutputStream;
import org.elsfs.cloud.common.util.exception.ProduceException;
import org.elsfs.cloud.screw.metadata.model.DataModel;

/**
 * 文件产生接口
 *
 * @author zeng
 */
public interface TemplateEngine {
  /**
   * 生成文档
   *
   * @param info {@link DataModel}
   * @param docName {@link String}
   * @throws ProduceException ProduceException
   */
  ByteArrayOutputStream produce(DataModel info, String docName) throws ProduceException;
}
