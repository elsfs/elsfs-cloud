/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.spring;

import java.util.List;
import lombok.Data;
import org.elsfs.cloud.screw.engine.EngineFileType;
import org.elsfs.cloud.screw.engine.EngineTemplateType;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * screw 配置文件
 *
 * @author zeng
 */
@Data
@ConfigurationProperties(prefix = "screw")
public class ScrewProperties {

  // ====================基本配置====================//
  /** 组织 */
  private String organization;

  /** url */
  private String organizationUrl;

  /** 标题 */
  private String title;

  /** 版本 */
  private String version;

  /** 描述 */
  private String description;

  // ====================连接配置====================//
  /** 用户名 */
  private volatile String username;

  /** 密码 */
  private volatile String password;

  /** 驱动类名称 */
  private String driverClassName;

  /** JDBC URL */
  private String jdbcUrl;

  // ====================数据处理配置====================//
  /** 忽略表名 */
  private List<String> ignoreTableName;

  /** 忽略表前缀 */
  private List<String> ignoreTablePrefix;

  /** 忽略表后缀 */
  private List<String> ignoreTableSuffix;

  /** 指定生成表名 */
  private List<String> designatedTableName;

  /** 指定生成表前缀 */
  private List<String> designatedTablePrefix;

  /** 指定生成表后缀 */
  private List<String> designatedTableSuffix;

  // ====================生成引擎配置====================//
  /** 生成文件类型 */
  private EngineFileType fileType = EngineFileType.HTML;

  /** 生成实现 */
  private EngineTemplateType produceType = EngineTemplateType.freemarker;

  /** 自定义模板，模板需要和文件类型和使用模板的语法进行编写和处理，否则将会生成错误 */
  private String template;

  /** 文件名称 */
  private String fileName;
}
