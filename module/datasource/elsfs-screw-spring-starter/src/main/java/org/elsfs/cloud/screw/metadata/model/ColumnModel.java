/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.metadata.model;

import java.io.Serializable;
import lombok.Data;

/**
 * 表列领域对象，目前包含如下内容
 *
 * <p>名称
 *
 * <p>数据类型
 *
 * <p>长度
 *
 * <p>小数位
 *
 * <p>允许空值
 *
 * <p>主键
 *
 * <p>默认值
 *
 * <p>说明
 *
 * @author zeng
 */
@Data
public class ColumnModel implements Serializable {

  /** 表中的列的索引（从 1 开始） */
  private String ordinalPosition;

  /** 名称 */
  private String columnName;

  /** SQL 数据类型带长度 */
  private String columnType;

  /** SQL 数据类型 名称 */
  private String typeName;

  /** 列长度 */
  private String columnLength;

  /** 列大小 */
  private String columnSize;

  /** 小数位 */
  private String decimalDigits;

  /** 可为空 */
  private String nullable;

  /** 是否主键 */
  private String primaryKey;

  /** 默认值 */
  private String columnDef;

  /** 说明 */
  private String remarks;

  /** 嵌套数据信息（用于文档数据库） */
  private TableModel nestedTable;

  /** 是否弃用 */
  private Boolean deprecated;
}
