/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.engine;

import lombok.Getter;

/**
 * 模板类型
 *
 * @author zeng
 */
@Getter
public enum EngineTemplateType {
  /** velocity 模板 */
  velocity("/template/velocity/", VelocityTemplateEngine.class, ".vm"),
  /** freeMarker 模板 */
  freemarker("/template/freemarker/", FreemarkerTemplateEngine.class, ".ftl");

  /** 模板目录 */
  private final String templateDir;

  /** 模板驱动实现类类型 */
  private final Class<? extends TemplateEngine> implClass;

  /** 后缀 */
  private final String suffix;

  /**
   * 构造
   *
   * @param freemarker {@link String}
   * @param template {@link Class}
   */
  EngineTemplateType(String freemarker, Class<? extends TemplateEngine> template, String suffix) {
    this.templateDir = freemarker;
    this.implClass = template;
    this.suffix = suffix;
  }
}
