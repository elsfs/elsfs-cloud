/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.query.mysql.model;

import lombok.Data;
import org.elsfs.cloud.screw.mapping.MappingField;
import org.elsfs.cloud.screw.metadata.Table;

/**
 * 表信息
 *
 * @author zeng
 */
@Data
public class MySqlTableModel implements Table {

  /** tableCat */
  @MappingField(value = "TABLE_CAT")
  private String tableCat;

  /** 表名 */
  @MappingField(value = "TABLE_NAME")
  private String tableName;

  /** selfReferencingColName */
  @MappingField(value = "SELF_REFERENCING_COL_NAME")
  private String selfReferencingColName;

  /** tableSchem */
  @MappingField(value = "TABLE_CAT")
  private String tableSchem;

  /** typeSchem */
  @MappingField(value = "TYPE_SCHEM")
  private String typeSchem;

  /** typeCat */
  @MappingField(value = "TABLE_CAT")
  private Object typeCat;

  /** 表类型 */
  @MappingField(value = "TABLE_TYPE")
  private String tableType;

  /** 备注 */
  @MappingField(value = "REMARKS")
  private String remarks;

  /** refGeneration */
  @MappingField(value = "REF_GENERATION")
  private String refGeneration;

  /** 类型名称 */
  @MappingField(value = "TYPE_NAME")
  private String typeName;
}
