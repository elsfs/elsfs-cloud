/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.screw.query.cachedb.model;

import lombok.Data;
import org.elsfs.cloud.screw.mapping.MappingField;
import org.elsfs.cloud.screw.metadata.Column;

/**
 * 表字段信息
 *
 * @author zeng
 */
@Data
public class CacheDbColumnModel implements Column {

  /** scopeTable */
  @MappingField(value = "SCOPE_TABLE")
  private String scopeTable;

  /** tableCat */
  @MappingField(value = "TABLE_CAT")
  private String tableCat;

  /** BUFFER_LENGTH */
  @MappingField(value = "BUFFER_LENGTH")
  private String bufferLength;

  /** IS_NULLABLE */
  @MappingField(value = "IS_NULLABLE")
  private String isNullable;

  /** TABLE_NAME */
  @MappingField(value = "TABLE_NAME")
  private String tableName;

  /** columnDef */
  @MappingField(value = "COLUMN_DEF")
  private String columnDef;

  /** scopeCatalog */
  @MappingField(value = "SCOPE_CATALOG")
  private String scopeCatalog;

  /** TABLE_SCHEM */
  @MappingField(value = "TABLE_SCHEM")
  private String tableSchem;

  /** COLUMN_NAME */
  @MappingField(value = "COLUMN_NAME")
  private String columnName;

  /** nullable */
  @MappingField(value = "NULLABLE")
  private String nullable;

  /** REMARKS */
  @MappingField(value = "REMARKS")
  private String remarks;

  /** DECIMAL_DIGITS */
  @MappingField(value = "DECIMAL_DIGITS")
  private String decimalDigits;

  /** numPrecRadix */
  @MappingField(value = "NUM_PREC_RADIX")
  private String numPrecRadix;

  /** sqlDatetimeSub */
  @MappingField(value = "SQL_DATETIME_SUB")
  private String sqlDatetimeSub;

  /** IS_GENERATEDCOLUMN */
  @MappingField(value = "IS_GENERATEDCOLUMN")
  private String isGeneratedcolumn;

  /** IS_AUTOINCREMENT */
  @MappingField(value = "IS_AUTOINCREMENT")
  private String isAutoincrement;

  /** SQL_DATA_TYPE */
  @MappingField(value = "SQL_DATA_TYPE")
  private String sqlDataType;

  /** CHAR_OCTET_LENGTH */
  @MappingField(value = "CHAR_OCTET_LENGTH")
  private String charOctetLength;

  /** ORDINAL_POSITION */
  @MappingField(value = "ORDINAL_POSITION")
  private String ordinalPosition;

  /** scopeSchema */
  @MappingField(value = "SCOPE_SCHEMA")
  private String scopeSchema;

  /** sourceDataType */
  @MappingField(value = "SOURCE_DATA_TYPE")
  private String sourceDataType;

  /** DATA_TYPE */
  @MappingField(value = "DATA_TYPE")
  private String dataType;

  /** TYPE_NAME */
  @MappingField(value = "TYPE_NAME")
  private String typeName;

  /** COLUMN_SIZE */
  @MappingField(value = "COLUMN_SIZE")
  private String columnSize;

  /** 是否主键 */
  private String primaryKey;

  /** 列类型（带长度） */
  @MappingField(value = "COLUMN_TYPE")
  private String columnType;

  /** 列长度 */
  @MappingField(value = "COLUMN_LENGTH")
  private String columnLength;
}
