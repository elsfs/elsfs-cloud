/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 数据源表
 *
 * @author zeng
 */
@Data
@TableName("gen_datasource_conf")
@EqualsAndHashCode(callSuper = true)
public class GenDatasourceConf extends BaseEntity {

  /** 主键 */
  @TableId(type = IdType.ASSIGN_ID)
  private Long id;

  /** 名称 */
  private String name;

  /** 数据库类型 */
  private String dsType;

  /** 配置类型 （0 主机形式 | 1 url形式） */
  private String confType;

  /** 主机地址 */
  private String host;

  /** 端口 */
  private Integer port;

  /** jdbc-url */
  private String url;

  /** 实例 */
  private String instance;

  /** 数据库名称 */
  @TableField(condition = SqlCondition.LIKE)
  private String dsName;

  /** 用户名 */
  private String username;

  /** 状态 */
  private String state;

  /** 密码 */
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private String password;
}
