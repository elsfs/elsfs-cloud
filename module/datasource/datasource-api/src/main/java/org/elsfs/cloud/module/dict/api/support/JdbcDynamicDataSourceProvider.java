/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.api.support;

import com.baomidou.dynamic.datasource.creator.DataSourceProperty;
import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.dynamic.datasource.provider.AbstractJdbcDataSourceProvider;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;

/**
 * 从数据源中获取 配置信息
 *
 * @author zeng
 */
@Slf4j
public class JdbcDynamicDataSourceProvider extends AbstractJdbcDataSourceProvider {
  private final DataSourceProperties properties;
  private final DefaultDataSourceCreator defaultDataSourceCreator;
  private final StringEncryptor stringEncryptor;

  /**
   * 构造函数：JdbcDynamicDataSourceProvider的构造方法。 用于初始化数据源提供者，基于传入的默认数据源创建器和数据源属性来配置动态数据源。
   *
   * @param defaultDataSourceCreator 默认数据源创建器，用于创建默认的数据源。
   * @param properties 数据源属性，包含驱动类名、数据库URL、用户名和密码等配置信息。
   */
  public JdbcDynamicDataSourceProvider(
      DefaultDataSourceCreator defaultDataSourceCreator,
      DataSourceProperties properties,
      StringEncryptor stringEncryptor) {
    // 调用父类构造函数，传入数据源的基本配置信息
    super(
        defaultDataSourceCreator,
        properties.getDriverClassName(),
        properties.getUrl(),
        properties.getUsername(),
        properties.getPassword());
    this.defaultDataSourceCreator = defaultDataSourceCreator; // 保存默认数据源创建器实例
    this.properties = properties; // 保存数据源属性实例
    this.stringEncryptor = stringEncryptor;
  }



  /**
   * 执行语句获得数据源参数
   *
   * @param statement 语句
   * @return 数据源参数
   * @throws SQLException sql异常
   */
  @Override
  protected Map<String, DataSourceProperty> executeStmt(Statement statement) throws SQLException {
    ResultSet rs = statement.executeQuery(properties.getQueryDsSql());
    Map<String, DataSourceProperty> map = new HashMap<>(8);
    while (rs.next()) {
      String username = rs.getString(DataSourceConstants.DS_USER_NAME);
      String password = rs.getString(DataSourceConstants.DS_USER_PWD);
      String url = rs.getString(DataSourceConstants.DS_JDBC_URL);
      DataSourceProperty property = new DataSourceProperty();
      property.setUsername(username);
      property.setLazy(true);
      property.setPassword(stringEncryptor.decrypt(password));
      property.setUrl(url);
      String name = rs.getString(DataSourceConstants.DS_NAME);
      map.put(name, property);
    }
    // 添加默认主数据源
    DataSourceProperty property = new DataSourceProperty();
    property.setUsername(properties.getUsername());
    property.setPassword(properties.getPassword());
    property.setUrl(properties.getUrl());
    property.setLazy(false);
    map.put(DataSourceConstants.DS_MASTER, property);
    return map;
  }
}
