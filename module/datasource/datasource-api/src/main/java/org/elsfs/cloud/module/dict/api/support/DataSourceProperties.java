/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.api.support;

import lombok.Data;

/**
 * 多数据源 数据库读取配置
 *
 * @author zeng
 */
@Data
public class DataSourceProperties {

  /** 用户名 */
  private String username;

  /** 密码 */
  private String password;

  /** jdbcurl */
  private String url;

  /** 驱动类型 */
  private String driverClassName;

  /** 查询数据源的SQL */
  private String queryDsSql = "select * from datasource_conf";

  /** 开启自定义 jdbc数据源 */
  private boolean dynamicJdbcEnabled = true;
}
