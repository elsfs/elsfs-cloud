/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.api.enums;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

/** jdbc-url */
@Getter
@AllArgsConstructor
public enum DsJdbcUrlEnum {

  /** mysql 数据库 */
  MYSQL(
      "mysql",
      "jdbc:mysql://%s:%s/%s?characterEncoding=utf8"
          + "&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true"
          + "&useLegacyDatetimeCode=false&allowMultiQueries=true&allowPublicKeyRetrieval=true",
      "select 1",
      "mysql8 链接"),

  /** pg 数据库 */
  PG("pg", "jdbc:postgresql://%s:%s/%s", "select 1", "postgresql 链接"),

  /** SQL SERVER */
  MSSQL(
      "mssql",
      "jdbc:sqlserver://%s:%s;database=%s;characterEncoding=UTF-8",
      "select 1",
      "sqlserver 链接"),

  /** oracle */
  ORACLE("oracle", "jdbc:oracle:thin:@%s:%s:%s", "select 1 from dual", "oracle 链接"),

  /** db2 */
  DB2("db2", "jdbc:db2://%s:%s/%s", "select 1 from sysibm.sysdummy1", "DB2 TYPE4 连接"),

  /** 达梦 */
  DM("dm", "jdbc:dm://%s:%s/%s", "select 1 from dual", "达梦连接"),

  /** pg 数据库 */
  HIGHGO("highgo", "jdbc:highgo://%s:%s/%s", "select 1", "highgo 链接");

  private final String dbName;

  private final String url;

  private final String validationQuery;

  private final String description;

  /**
   * 获取数据库类型
   *
   * @param dsType 数据库类型
   * @return DsJdbcUrlEnum
   */
  public static DsJdbcUrlEnum get(String dsType) {
    return Arrays.stream(DsJdbcUrlEnum.values())
        .filter(dsJdbcUrlEnum -> dsType.equals(dsJdbcUrlEnum.getDbName()))
        .findFirst()
        .get();
  }
}
