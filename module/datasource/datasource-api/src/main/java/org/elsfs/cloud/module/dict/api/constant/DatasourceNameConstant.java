/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.module.dict.api.constant;

/**
 * 数据源名称常量
 *
 * @author zeng
 */
public interface DatasourceNameConstant {

  /** 用户 */
  String USER = "user";
  /** 订单 */
  String SCHOOL = "school";
  /** 代码生成 */
  String CODE_GEN = "codeGen";

  /** 订单 */
  String ORDER = "order";

  /** 购物车 */
  String SHOPPING_CART = "shoppingCart";

  /** oauth2 */
  String OAUTH2 = "oauth2";

  /** nacos */
  String NACOS = "nacos";

  /** 管理 */
  String ADMIN = "admin";

  /** 字典 */
  String STD = "std";

  String CMS = "cms";

  /** 产品，商品 */
  String PRODUCT = "product";

  /** 会员 */
  String MEMBERSHIP = "membership";

  /** 供应商 */
  String PURVEYOR = "purveyor";

  /** 支付 */
  String PAYMENT = "payment";

  /** 物流 */
  String LOGISTICS = "logistics";
}
