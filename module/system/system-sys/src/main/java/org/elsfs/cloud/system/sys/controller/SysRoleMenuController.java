package org.elsfs.cloud.system.sys.controller;

import com.baomidou.dynamic.datasource.annotation.DS;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.dto.co.RolMenuSaveCO;
import org.elsfs.cloud.system.sys.manager.SysRoleManager;
import org.elsfs.cloud.system.sys.repository.mapper.SysRoleMenuMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(description = "roleMenu", name = "角色菜单管理")
@RequestMapping("/roleMenu")
public class SysRoleMenuController {
  private final SysRoleMenuMapper sysRoleMenuMapper;
  private final SysRoleManager sysRoleManager;

  /**
   * 根据角色id获取菜单id
   *
   * @param roleId 角色 id
   * @return List
   */
  @GetMapping("/getMenuIdByRoleId/{roleId}")
  public R<List<String>> getMenuIdByRoleId(@PathVariable("roleId") String roleId) {
    List<String> menuIds = sysRoleMenuMapper.selectByRole(roleId);
    return R.success(menuIds);
  }

  /**
   * 保存角色菜单
   * @param co 保存角色菜单
   * @return r
   */
  @PostMapping("/saveRoleMenu")
  public R<?> saveRoleMenu(@RequestBody RolMenuSaveCO co) {
    return sysRoleManager.saveRoleMenu(co);
  }
}
