/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.manager;

import com.baomidou.dynamic.datasource.annotation.DS;
import jakarta.transaction.Transactional;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.util.lang.CollectionUtils;
import org.elsfs.cloud.common.util.lang.StringUtils;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.dto.co.RolMenuSaveCO;
import org.elsfs.cloud.system.api.dto.co.RoleCreateCO;
import org.elsfs.cloud.system.api.entity.SysRole;
import org.elsfs.cloud.system.api.entity.SysRoleMenu;
import org.elsfs.cloud.system.sys.repository.SysRoleRepository;
import org.elsfs.cloud.system.sys.repository.mapper.SysRoleMenuMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 角色管理
 *
 * @author zeng
 */
@Service
@RequiredArgsConstructor
public class SysRoleManager {
  private final SysRoleRepository sysRoleRepository;
  private final SysRoleMenuMapper sysRoleMenuMapper;

  /**
   * 创建角色 1. 新增角色 2. 新增角色和菜单关系
   *
   * @param co data对象
   * @return r
   */
  public R<Void> create(RoleCreateCO co) {
    sysRoleRepository.save(co);
    if (CollectionUtils.isNotEmpty(co.getMenuIds())) {
      List<SysRoleMenu> list =
          co.getMenuIds().stream()
              .filter(StringUtils::isNotBlank)
              .map(s -> new SysRoleMenu(co.getRoleId(), s))
              .toList();
      sysRoleMenuMapper.insetRoleMenu(list);
    }
    return R.success();
  }

  /**
   * 更新角色 1.更新角色信息 2.删除角色和菜单关系 3.添加角色和菜单关系
   *
   * @param co 更新数据
   * @return r
   */
  public R<Void> update(@RequestBody RoleCreateCO co) {
    sysRoleRepository.updateById(co);
    sysRoleMenuMapper.deleteRoleMenuByRoleIds(List.of(co.getRoleId()));
    if (CollectionUtils.isNotEmpty(co.getMenuIds())) {
      List<SysRoleMenu> list =
          co.getMenuIds().stream().map(menuId -> new SysRoleMenu(co.getRoleId(), menuId)).toList();
      sysRoleMenuMapper.insetRoleMenu(list);
    }
    return R.success();
  }

  /**
   * 删除角色 1. 删除角色 2. 删除角色和菜单关系
   *
   * @param roleId 角色id
   * @return r
   */
  public R<Void> delete(String roleId) {
    sysRoleRepository.removeById(roleId);
    sysRoleMenuMapper.deleteRoleMenuByRoleIds(List.of(roleId));
    return R.success();
  }

  public R<Void> updateRoleStatus(SysRole co) {
    sysRoleRepository.updateById(co);
    return R.success();
  }

  /**
   * 更新角色菜单关系
   *
   * @param co co
   * @return r
   */
  @Transactional
  @DS(DatasourceNameConstant.ADMIN)
  public R<Void> saveRoleMenu(RolMenuSaveCO co) { // 15008594929
    List<SysRoleMenu> menuList =
        co.getMenuIds().stream().map(menuId -> new SysRoleMenu( co.getRoleId(),menuId)).toList();
    sysRoleMenuMapper.deleteRoleMenuByRoleIds(List.of(co.getRoleId()));
    sysRoleMenuMapper.insetRoleMenu(menuList);
    return R.success();
  }
}
