/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;
import org.elsfs.cloud.system.api.dto.qry.SysUserQry;
import org.elsfs.cloud.system.api.entity.SysUser;

/**
 * 用户服务
 *
 * @author zeng
 */
public interface SysUserRepository extends IElsfsRepository<SysUser, String> {

  /**
   * 翻页查询
   *
   * @param page 翻页对象
   * @param sysUserQry 实体对象
   */
  <P extends IPage<?>> P pageUser(P page, SysUserQry sysUserQry);

  @Override
  default boolean getAllowEditState() {
    return true;
  }

  default SysUser getUserByUsername(String username) {
    return getOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUsername, username));
  }

  SysUser getUserByPhone(String phone);

  SysUser getUserByEmail(String email);
}
