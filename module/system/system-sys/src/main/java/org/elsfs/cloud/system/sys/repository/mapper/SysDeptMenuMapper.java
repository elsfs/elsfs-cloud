/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.repository.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.entity.SysDeptMenu;

/**
 * 部门菜单关联表 Mapper 接口
 *
 * @author elsfs
 */
@Mapper
@DS(DatasourceNameConstant.ADMIN)
public interface SysDeptMenuMapper {

  /**
   * 根据部门id查询菜单id
   *
   * @param deptId 部门id
   * @return 菜单ids
   */
  List<String> selectMenuIdsByDeptId(@Param("deptId") String deptId);

  List<String> selectMenuIdsByDeptIds(@Param("deptIds") List<String> deptIds);

  /**
   * 根据部门id删除
   *
   * @param deptId 部门 id
   */
  void deleteByDeptId(@Param("deptId") String deptId);

  Integer insertBatch(@Param("list") List<SysDeptMenu> sysDeptMenus);
}
