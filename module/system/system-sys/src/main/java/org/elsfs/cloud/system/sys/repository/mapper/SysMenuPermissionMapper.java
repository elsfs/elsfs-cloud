/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.repository.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import java.util.List;
import java.util.Set;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.entity.SysMenuPermission;

/**
 * 菜单权限 Mapper 接口
 *
 * @author zeng
 */
@Mapper
@DS(DatasourceNameConstant.ADMIN)
public interface SysMenuPermissionMapper {
  List<String> selectPermissionIdsByMenuIds(@Param("menuIds") Set<String> menuIds);

  List<String> selectMenuIdsByPermissionId(@Param("permissionId") String permissionId);

  void deleteByPermissionId(@Param("permissionId") String permissionId);

  int saveBatch(@Param("list") List<SysMenuPermission> list);
}
