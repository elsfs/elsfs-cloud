/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.controller.ControllerConfig;
import org.elsfs.cloud.common.controller.LogicRepositoryCrudController;
import org.elsfs.cloud.common.controller.TreeLogicRepositoryCrudController;
import org.elsfs.cloud.system.api.entity.SysDept;
import org.elsfs.cloud.system.api.entity.SysPost;
import org.elsfs.cloud.system.api.entity.SysRole;
import org.elsfs.cloud.system.sys.repository.SysDeptRepository;
import org.elsfs.cloud.system.sys.repository.SysPostRepository;
import org.elsfs.cloud.system.sys.repository.SysRoleRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统注入控制器
 *
 * @author zeng
 */
@Component
public class SystemInjectController {
  /**
   * 部门控制器
   *
   * @author zeng
   */
  @RestController
  @RequestMapping("/dept")
  @RequiredArgsConstructor
  @Tag(description = "dept", name = "部门管理")
  public static class SysDeptController
      extends TreeLogicRepositoryCrudController<
          SysDept, SysDept, SysDept, SysDept, SysDeptRepository, String> {
    @Override
    protected ControllerConfig getConfig() {
      return super.getConfig().list(true);
    }
  }

  /**
   * 角色控制器
   *
   * @author zeng
   */
  @RestController
  @RequestMapping("/role")
  @RequiredArgsConstructor
  @Tag(description = "role", name = "角色管理")
  public static class SysRoleController
      extends LogicRepositoryCrudController<
          SysRole, SysRole, SysRole, SysRole, SysRoleRepository, String> {
    @Override
    protected ControllerConfig getConfig() {
      return super.getConfig().list(true);
    }
  }

  /**
   * 部门控制器
   *
   * @author zeng
   */
  @RestController
  @RequestMapping("/post")
  @RequiredArgsConstructor
  @Tag(description = "post", name = "部门管理")
  public static class SysPostController
      extends LogicRepositoryCrudController<
          SysPost, SysPost, SysPost, SysPost, SysPostRepository, String> {
    @Override
    protected ControllerConfig getConfig() {
      return super.getConfig().list(true);
    }
  }
}
