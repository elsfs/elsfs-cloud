/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.system.api.dto.co.AddUserRoleCO;
import org.elsfs.cloud.system.api.entity.SysUser;
import org.elsfs.cloud.system.api.entity.SysUserRole;
import org.elsfs.cloud.system.sys.manager.SysUserManager;
import org.elsfs.cloud.system.sys.repository.SysUserRepository;
import org.elsfs.cloud.system.sys.service.SysUserRoleService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户角色管理
 *
 * @author zeng
 */
@RestController
@RequestMapping("/user/role")
@RequiredArgsConstructor
@Tag(description = "userRole", name = "用户角色管理")
public class SysUserRoleController {
  private final SysUserRoleService sysUserRoleService;
  private final SysUserManager sysUserManager;

  /**
   * 获取角色用户
   *
   * @param roleId 角色id
   * @return r
   */
  @GetMapping("/getRoleIds/{roleId}")
  public R<List<SysUser>> getRoleIds(@PathVariable("roleId") String roleId) {
    return R.success(sysUserManager.getUserByRoleId(roleId));
  }

  /**
   * 获取用户角色id
   *
   * @param userId 用户id
   * @return r
   */
  @GetMapping("/getUserRoleIds/{userId}")
  public R<?> getUserRoleIds(@PathVariable("userId") String userId) {
    return R.success(sysUserRoleService.getRoleIdsByUserId(userId));
  }

  @DeleteMapping("deleteUserRole")
  public R<?> deleteUserRole(@RequestBody SysUserRole sysUserRole) {
    return R.success(sysUserRoleService.delete(sysUserRole));
  }

  /**
   * 添加用户到角色
   */
  @PostMapping("addUserRole")
  public R<?> addUserRole(@RequestBody @Validated AddUserRoleCO co) {
    List<SysUserRole> list =
      co.getUserIds().stream().map(userId -> new SysUserRole(userId, co.getRoleId())).toList();
    sysUserRoleService.deleteBatch(co);
    sysUserRoleService.saveBatch(list);
    return R.success();
  }
}
