/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.dto.co.AddUserRoleCO;
import org.elsfs.cloud.system.api.entity.SysUserRole;
import org.elsfs.cloud.system.sys.repository.mapper.SysUserRoleMapper;
import org.elsfs.cloud.system.sys.service.SysUserRoleService;
import org.springframework.stereotype.Service;

/**
 * 用户角色关联表 服务实现类
 *
 * @author zeng
 */
@RequiredArgsConstructor
@Service
@DS(DatasourceNameConstant.ADMIN)
public class SysUserRoleServiceImpl implements SysUserRoleService {

  private final SysUserRoleMapper sysUserRoleMapper;

  @Override
  public List<String> getRoleIdsByUserId(String userId) {
    return sysUserRoleMapper.selectRoleIdsByUserId(userId);
  }

  @Override
  public boolean delete(SysUserRole sysUserRole) {
    return sysUserRoleMapper.delete(sysUserRole);
  }

  @Override
  public void saveBatch(List<SysUserRole> list) {
    sysUserRoleMapper.insertBatch(list);
  }

  @Override
  public boolean deleteBatch(AddUserRoleCO co) {
    return sysUserRoleMapper.deleteBatch(co);
  }
}
