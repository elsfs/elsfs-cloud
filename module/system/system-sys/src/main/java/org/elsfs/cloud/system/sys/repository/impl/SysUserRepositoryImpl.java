/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.repository.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.elsfs.cloud.common.mybatis.repository.ElsfsCrudRepositoryImpl;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.dto.qry.SysUserQry;
import org.elsfs.cloud.system.api.entity.SysUser;
import org.elsfs.cloud.system.sys.repository.SysUserRepository;
import org.elsfs.cloud.system.sys.repository.mapper.SysUserMapper;
import org.springframework.stereotype.Service;

/**
 * 用户服务
 *
 * @author zeng
 */
@Service
@DS(DatasourceNameConstant.ADMIN)
public class SysUserRepositoryImpl extends ElsfsCrudRepositoryImpl<SysUserMapper, SysUser, String>
    implements SysUserRepository {

  /**
   * 分页查询
   *
   * @param page 翻页对象
   * @param sysUserQry 实体对象
   */
  @Override
  public <P extends IPage<?>> P pageUser(P page, SysUserQry sysUserQry) {
    return baseMapper.selectUserPage(page, sysUserQry);
  }

  @Override
  public SysUser getUserByPhone(String phone) {
    return getOne(Wrappers.lambdaQuery(SysUser.class).eq(SysUser::getPhone, phone));
  }

  @Override
  public SysUser getUserByEmail(String email) {
    return getOne(lambdaQuery().eq(SysUser::getEmail, email));
  }
}
