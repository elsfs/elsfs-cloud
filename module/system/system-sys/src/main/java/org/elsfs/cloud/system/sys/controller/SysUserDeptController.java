/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.system.api.dto.co.AddUserDeptCO;
import org.elsfs.cloud.system.api.entity.SysUserDept;
import org.elsfs.cloud.system.sys.service.SysUserDeptService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户部门管理
 *
 * @author zeng
 */
@RestController
@RequestMapping("/user/dept")
@RequiredArgsConstructor
@Tag(description = "userDept", name = "用户部门管理")
public class SysUserDeptController {
  private final SysUserDeptService sysUserDeptService;

  @GetMapping("/getByUserId/{userId}")
  public R<?> getByUserId(@PathVariable("userId") String userId) {
    return R.success(sysUserDeptService.getByUserId(userId));
  }

  @DeleteMapping("deleteUserDept")
  public R<?> deleteUserRole(@RequestBody SysUserDept sysUserDept) {
    return R.success(sysUserDeptService.delete(sysUserDept));
  }

  @PostMapping("/bindUserDept")
  public R<?> bindUserDept(@RequestBody @Validated AddUserDeptCO co) {
    List<SysUserDept> list =
        co.getUserIds().stream().map(userId -> new SysUserDept(userId, co.getDeptId())).toList();
    sysUserDeptService.deleteBatch(co);
    sysUserDeptService.saveBatch(list);
    return R.success();
  }
}
