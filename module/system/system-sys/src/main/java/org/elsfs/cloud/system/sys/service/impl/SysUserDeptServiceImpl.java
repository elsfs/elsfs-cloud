/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.util.lang.CollectionUtils;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.dto.co.AddUserDeptCO;
import org.elsfs.cloud.system.api.entity.SysUserDept;
import org.elsfs.cloud.system.sys.repository.mapper.SysUserDeptMapper;
import org.elsfs.cloud.system.sys.service.SysUserDeptService;
import org.springframework.stereotype.Service;

/**
 * 用户部门关联表服务实现类
 *
 * @author zeng
 */
@Service
@RequiredArgsConstructor
@DS(DatasourceNameConstant.ADMIN)
public class SysUserDeptServiceImpl implements SysUserDeptService {
  private final SysUserDeptMapper sysUserDeptMapper;

  @Override
  public List<String> getByUserId(String userId) {
    return sysUserDeptMapper.selectByUserId(userId);
  }

  @Override
  public Boolean deleteByUserId(String userId) {
    return sysUserDeptMapper.deleteByUserId(userId);
  }

  @Override
  public boolean saveBatch(List<SysUserDept> sysUserDepts) {
    if (CollectionUtils.isEmpty(sysUserDepts)) {
      return false;
    }
    return sysUserDeptMapper.insertBatch(sysUserDepts);
  }

  @Override
  public boolean delete(SysUserDept sysUserDept) {
    return sysUserDeptMapper.delete(sysUserDept);
  }

  @Override
  public Boolean deleteBatch(AddUserDeptCO co) {
    return sysUserDeptMapper.deleteBatch(co);
  }
}
