/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.repository.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.entity.SysRoleMenu;

/**
 * 角色菜单关系mapper
 *
 * @author zeng
 */
@Mapper
@DS(DatasourceNameConstant.ADMIN)
public interface SysRoleMenuMapper {
  /**
   * 根据{@code roleIds}角色id集合删除角色菜单关系
   *
   * @param roleIds 角色 id 集合
   * @return 成功数量
   */
  int deleteRoleMenuByRoleIds(@Param("roleIds") List<String> roleIds);

  /**
   * 根据{@code menuIds}菜单id集合删除角色菜单关系
   *
   * @param menuIds 菜单id集合
   * @return 成功数量
   */
  int deleteRoleMenuByMenuIds(@Param("menuIds") List<String> menuIds);

  /**
   * 添加角色菜单关系
   *
   * @param roleMenus 角色菜单关系集合
   * @return 成功数量
   */
  int insetRoleMenu(@Param("roleMenus") List<SysRoleMenu> roleMenus);

  /**
   * 根据角id获取关联的菜单id集合
   *
   * @param roleId 角色id
   * @return 菜单id集合
   */
  List<String> selectByRole(@Param("roleId") String roleId);

  List<String> selectByMneuIdByRoleIds(@Param("roleIds") List<String> roleIds);
}
