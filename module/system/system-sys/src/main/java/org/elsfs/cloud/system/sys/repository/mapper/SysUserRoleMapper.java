/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.sys.repository.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.system.api.dto.co.AddUserRoleCO;
import org.elsfs.cloud.system.api.entity.SysUserRole;

/**
 * 角色用户关系mapper
 *
 * @author zeng
 */
@Mapper
@DS(DatasourceNameConstant.ADMIN)
public interface SysUserRoleMapper {

  boolean insert(@Param("roleUser") SysUserRole sysRoleUser);

  Boolean deleteByUserId(@Param("userId") String userId);

  List<String> selectByRoleId(@Param("roleId") String roleId);

  void insertBatch(@Param("p") List<SysUserRole> list);

  boolean delete(@Param("p") SysUserRole sysUserRole);

  List<String> selectRoleIdsByUserId(@Param("userId") String userId);

  boolean deleteBatch(@Param("co") AddUserRoleCO co);
}
