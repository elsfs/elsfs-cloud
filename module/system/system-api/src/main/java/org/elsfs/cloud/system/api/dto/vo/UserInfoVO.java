/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.dto.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import java.util.List;
import lombok.Data;

/**
 * 用户信息
 *
 * @author zeng
 */
@Data
@Schema(name = "用户查询")
public class UserInfoVO {
  /** 用户 id */
  private String userId;

  /** 用户名称 */
  private String username;

  /** 用户邮箱 */
  private String email;

  /** 用户手机号 */
  private String phone;

  /** 租户 id */
  private String tenantId;

  /** 用户昵称 */
  private String nickname;

  /** 用户角色id */
  private String roleId;

  /** 用户部门 id */
  private List<String> deptIds;

  private Integer sex;
  private LocalDate birthday;
  private String avatar;

  private String status;
}
