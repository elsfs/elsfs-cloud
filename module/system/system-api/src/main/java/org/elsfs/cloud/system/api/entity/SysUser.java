/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.entity;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.time.LocalDate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 用户
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUser extends BaseEntity {
  /** 用户id */
  @TableId
  @Schema(description = "用户 id")
  private String userId;

  /** 用户名 */
  @TableField(condition = SqlCondition.LIKE)
  @NotNull(message = "用户名不能为空") private String username;

  /** 用户密码 */
  @JsonIgnore private String password;

  /** 用户头像 */
  private String avatar;

  /** 用户邮箱 */
  @TableField(condition = SqlCondition.LIKE)
  @NotBlank(message = "用户邮箱不能为空")
  @Email(message = "邮箱格式不正确")
  private String email;

  /** 用户手机号 */
  @TableField(condition = SqlCondition.LIKE)
  @NotBlank(message = "用户手机号不能为空")
  @Pattern(regexp = "^1[3-9]\\d{9}$", message = "手机号格式不正确")
  private String phone;

  /** 用户租户 */
  private String tenantId;

  /** 用户昵称 */
  @TableField(condition = SqlCondition.LIKE)
  @NotBlank(message = "用户昵称不能为空")
  private String nickname;

  /** 用户性别 */
  private String sex;

  private LocalDate birthday;
  private Integer status;
}
