/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.converter;

import org.elsfs.cloud.system.api.dto.vo.UserInfoVO;
import org.elsfs.cloud.system.api.entity.SysUser;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * SysUserConverter
 *
 * @author zengchao
 */
@Mapper(componentModel = "jakarta", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SysUserConverter {

  UserInfoVO toUserInfoVO(SysUser sysUser);
}
