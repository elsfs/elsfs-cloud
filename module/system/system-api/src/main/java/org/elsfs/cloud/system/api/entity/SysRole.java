/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.entity;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 角色数据库实体
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysRole extends BaseEntity {
  /** 角色ID */
  @TableId private String roleId;

  /** 角色名称 */
  @TableField(condition = SqlCondition.LIKE)
  private String roleName;

  /** 角色编码 */
  @TableField(condition = SqlCondition.LIKE)
  @Schema(description = "角色编码")
  private String roleCode;

  /** 角色描述 */
  @TableField(condition = SqlCondition.LIKE)
  private String roleDesc;

  /** 排序 */
  private Integer orderNo;

  /** 状态 */
  private Integer status;
}
