/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.dto.co;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.system.api.entity.SysUser;

/**
 * 用户信息
 *
 * @author zeng
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(title = "用户信息")
public class UserInfoCO extends SysUser {

  /** 用户角色id */
  @NotNull(message = "用户角色id不能为空") private List<String> roleIds;

  /** 用户部门id */
  @NotBlank(message = "用户部门id不能为空")
  private String deptId;
}
