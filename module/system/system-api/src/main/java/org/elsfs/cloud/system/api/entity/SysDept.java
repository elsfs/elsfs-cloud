/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.entity;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseTreeEntity;

/**
 * 部门
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysDept extends BaseTreeEntity<SysDept, String> {
  @TableId private String deptId;

  /** 上级部门 */
  @NotBlank private String parentId;

  /** 部门电话 */
  private String mobile;

  /** 部门地址 */
  private String address;

  /** 部门名称 */
  @NotBlank
  @TableField(condition = SqlCondition.LIKE)
  private String deptName;

  /** 部门编码 */
  private String orgCode;

  /** 部门类型 */
  private Integer orgCategory;

  /** 排序 */
  @NotNull private Integer orderNo;

  /** 状态 */
  private Integer status;

  /** 备注 */
  @TableField(condition = SqlCondition.LIKE)
  private String deptDesc;

  @Override
  public String getId() {
    return deptId;
  }
}
