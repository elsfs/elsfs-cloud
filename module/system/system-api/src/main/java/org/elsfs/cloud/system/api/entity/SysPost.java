/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 部门
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysPost extends BaseEntity {
  /** postId */
  @TableId(type = IdType.ASSIGN_ID)
  @Schema(description = "id")
  private String postId;

  /** 职务编码 */
  @Schema(description = "职务编码")
  private String postCode;

  /** 职务名称 */
  @Schema(description = "职务名称")
  @TableField(condition = SqlCondition.LIKE)
  private String postName;

  /** 职级 */
  private String postRank;

  /** 状态 */
  @Schema(description = "状态")
  private String status;

  /** 排序 */
  @Schema(description = "排序")
  private Integer orderNo;

  /** 备注 */
  @Schema(description = "备注")
  private String remark;
}
