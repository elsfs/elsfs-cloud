/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.system.api.dto.co;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.Set;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * 新增用户部门关联
 *
 * @author zengzhongjie
 */
@Data
@RequiredArgsConstructor
public class AddUserRoleCO {
  @NotNull(message = "userIds不能为空") @Size(min = 1, message = "至少选择一个用户")
  private Set<String> userIds;

  @NotBlank(message = "roleId不能为空")
  private String roleId;
}
