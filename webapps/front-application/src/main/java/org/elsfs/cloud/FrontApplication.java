/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud;

import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.core.utils.PrintStartInfo;
import org.elsfs.cloud.module.dict.api.annotation.EnableDynamicDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@Slf4j
@ComponentScan(
    excludeFilters = {
      @ComponentScan.Filter(
          type = FilterType.REGEX,
          pattern = {"org.elsfs.cloud.module.*.biz.adapter.admin.*"})
    })
@EnableDynamicDataSource
public class FrontApplication {
  /**
   * main
   *
   * @param args args
   */
  public static void main(String[] args) { // DynamicDataSourceContextHolder
    long start = System.currentTimeMillis();
    ConfigurableApplicationContext context = SpringApplication.run(FrontApplication.class, args);
    PrintStartInfo.outputLog(context.getEnvironment(), start);
  }
}
