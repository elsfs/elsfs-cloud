## 0 下载 nacos
https://nacos.io/docs/latest/quickstart/quick-start/

# 1. 解压缩Nacos 发行包
```shell
unzip nacos-server-$version.zip
  # 或者 tar -xvf nacos-server-$version.tar.gz

cd nacos/bin
```
# 2.启动服务器
:::TOP
注：Nacos的运行建议至少在2C 4G 60G的机器配置下运行。
:::
Linux/Unix/Mac
```shell
# 启动命令(standalone代表着单机模式运行，非集群模式):
sh startup.sh -m standalone
  -Xms512m -Xmx512m -Xmn256m -Dnacos.standalone=true -Dnacos.member.list= -Xlog:gc*:file=/app/nacos-server-2.4.3/nacos/logs/nacos_gc.log:time,tags:filecount=10,filesize=100m -Dloader.path=/app/nacos-server-2.4.3/nacos/plugins,/app/nacos-server-2.4.3/nacos/plugins/health,/app/nacos-server-2.4.3/nacos/plugins/cmdb,/app/nacos-server-2.4.3/nacos/plugins/selector -Dnacos.home=/app/nacos-server-2.4.3/nacos -jar /app/nacos-server-2.4.3/nacos/target/nacos-server.jar  --spring.config.additional-location=file:/app/nacos-server-2.4.3/nacos/conf/ --logging.config=/app/nacos-server-2.4.3/nacos/conf/nacos-logback.xml --server.max-http-header-size=524288
```
Windows
```shell
# 启动命令(standalone代表着单机模式运行，非集群模式):
startup.cmd -m standalone
```
# 3.验证Nacos服务是否启动成功
进入${nacos.home}/logs/ 目录下， 使用tail -f start.out 查看日志，如果看到如下日志，说明服务启动成功。
```shell
Nacos started successfully in stand alone mode. use embedded storage
# 可以通过下列服务，快速检验Nacos的功能。
```
# 4.关闭服务器
```shell
# Linux/Unix/Mac
sh shutdown.sh
# Windows
shutdown.cmd
# 或者双击shutdown.cmd运行文件。
```
