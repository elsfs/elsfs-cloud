
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`
(
  `config_id`    varchar(32)              NOT NULL COMMENT '配置ID',
  `config_key`  varchar(32)              NOT NULL COMMENT ' 配置键',
  `config_name`  varchar(32)              NOT NULL COMMENT '配置名称',
  `config_value`  varchar(32)              NOT NULL COMMENT '配置值',
  `config_type`  varchar(32)              NOT NULL COMMENT '配置类型',
  `remark`     varchar(256) DEFAULT NULL COMMENT '备注',
  `valid_flag` varchar(3)   DEFAULT '1' not null COMMENT '状态 0无效 1有效',
  `tenant_id`  varchar(32)              NOT NULL COMMENT '租户ID',
  `create_by`   varchar(32)              NOT NULL COMMENT '创建人',
  `create_at`  datetime     DEFAULT NULL COMMENT '创建时间',
  `update_by`  varchar(32)              NOT NULL COMMENT '修改人',
  `update_at`  datetime     DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT ='配置管理';
