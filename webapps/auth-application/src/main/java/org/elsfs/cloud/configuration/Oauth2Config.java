/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.configuration;

import java.time.Instant;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;

/**
 * 配置客户端
 *
 * @author elsfs
 */
@Configuration
public class Oauth2Config {
  private final String redirectUri = "https://www.baidu.com";
  private static final List<String> SCOPE = List.of("message.read", "message.write", "openid");
  private static final String CLIENT_ID = "messaging-client";
  private static final String CLIENT_SECRET = "123456";

  /**
   * dev 或者正式环境使用 配置客户端
   *
   * @param passwordEncoder 密码加密器
   * @return RegisteredClientRepository
   */
  @Bean
  @Profile("test")
  RegisteredClientRepository registeredClientRepository(PasswordEncoder passwordEncoder) {

    RegisteredClient.Builder builder1 =
        RegisteredClient.withId(CLIENT_ID)
            .clientId(CLIENT_ID)
            .clientIdIssuedAt(Instant.MAX)
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .id(CLIENT_ID)
            .clientSecret(passwordEncoder.encode(CLIENT_SECRET))
            .clientName(CLIENT_ID)
            .redirectUri(redirectUri)
            .clientSettings(
                ClientSettings.builder()
                    // oauth 和设备模式是否需要授权
                    .requireAuthorizationConsent(true)
                    .build())
            .scopes(strings -> strings.addAll(SCOPE))
            .clientAuthenticationMethods(
                builder -> builder.add(ClientAuthenticationMethod.CLIENT_SECRET_BASIC));
    return new InMemoryRegisteredClientRepository(builder1.build());
  }

  /**
   * 测试使用 配置客户端
   *
   * @param passwordEncoder 密码加密器
   * @return RegisteredClientRepository
   */
  @Bean
  @Profile({"dev", "prod"})
  RegisteredClientRepository clientRepository(PasswordEncoder passwordEncoder) {

    RegisteredClient.Builder builder1 =
        RegisteredClient.withId("messaging")
            .clientId("messaging")
            .clientIdIssuedAt(Instant.MAX)
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .id("messaging")
            .clientSecret(passwordEncoder.encode("123456"))
            .clientName("messaging")
            .redirectUri(redirectUri)
            .clientSettings(
                ClientSettings.builder()
                    // oauth 和设备模式是否需要授权
                    .requireAuthorizationConsent(true)
                    .build())
            .scopes(strings -> strings.addAll(List.of("message", "message", "openid")))
            .clientAuthenticationMethods(
                builder -> builder.add(ClientAuthenticationMethod.CLIENT_SECRET_BASIC));
    return new InMemoryRegisteredClientRepository(builder1.build());
  }
}
