/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.configuration;

import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.api.security.service.SecurityUserService;
import org.elsfs.cloud.spring.common.core.userdetails.SecurityUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * 默认用户服务
 *
 * @author zeng
 */
@Component
@RequiredArgsConstructor
public class SecurityUserServiceImpl implements SecurityUserService<SecurityUser> {
  private final PasswordEncoder passwordEncoder;

  @Override
  public SecurityUser loadUserByUsername(String username) throws RuntimeException {

    return convert(username);
  }

  @Override
  public SecurityUser loadUserByPhone(String phone) throws RuntimeException {
    return convert(phone);
  }

  @Override
  public SecurityUser loadUserByUserId(String userId) throws RuntimeException {
    return convert(userId);
  }

  @Override
  public SecurityUser loadUserByEmail(String email) throws RuntimeException {
    return convert(email);
  }

  @Override
  public SecurityUser geLoginUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication instanceof UsernamePasswordAuthenticationToken) {
      return (SecurityUser) authentication.getPrincipal();
    }
    return null;
  }

  @Override
  public SecurityUser loadByUserId(String userId) {
    return null;
  }

  @Override
  public void createUser(UserDetails user) {}

  @Override
  public void updateUser(UserDetails user) {}

  @Override
  public void deleteUser(String username) {}

  @Override
  public void changePassword(String oldPassword, String newPassword) {}

  @Override
  public boolean userExists(String username) {
    return false;
  }

  SecurityUser convert(String sysUser) {

    SecurityUser securityUser = new SecurityUser();
    securityUser.setUserId("111");
    securityUser.setUsername("2323");
    securityUser.setNickname("saas");
    securityUser.setPassword(passwordEncoder.encode("123456"));
    securityUser.setEmail("wewe");
    securityUser.setPhone("wew");
    securityUser.setAvatar("ewe");
    securityUser.setTenantId("qqw");
    securityUser.setSex("wewe");
    securityUser.setValidFlag("0");
    return securityUser;
  }
}
