package org.elsfs.cloud.configuration;


import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import lombok.AllArgsConstructor;
import org.elsfs.cloud.api.security.key.InMemoryOAuth2JWKRepository;
import org.elsfs.cloud.api.security.key.OAuth2JWKRepository;
import org.elsfs.cloud.api.security.utils.GenerateKeyUtils;
import org.elsfs.cloud.common.core.utils.SpringContextHolder;
import org.elsfs.cloud.module.dict.api.constant.DatasourceNameConstant;
import org.elsfs.cloud.spring.common.core.jwt.JdbcRsaKeyPairRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;

import javax.sql.DataSource;

@Configuration
@AllArgsConstructor
public class AuthDataSourceConfiguration {
  private final DataSource dataSource;
  private final RegisteredClientRepository registeredClientRepository ;

  public JdbcTemplate getJdbcTemplate() {
    if (dataSource instanceof DynamicRoutingDataSource dynamicRoutingDataSource) {
      return new JdbcTemplate(dynamicRoutingDataSource.getDataSource(DatasourceNameConstant.OAUTH2));
    }
    return new JdbcTemplate(dataSource);
  }

  /**
   * Spring Security OAuth2 中用于处理授权同意逻辑的服务接口。它负责存储和检索用户的授权同意信息，确保用户在授权第三方应用访问其资源时的同意状态被正确记录。
   *
   * @return service
   */
  @Bean
  OAuth2AuthorizationConsentService oAuth2AuthorizationConsentService() {
    return new JdbcOAuth2AuthorizationConsentService(getJdbcTemplate(), registeredClientRepository);
  }

  @Bean
  OAuth2JWKRepository oAuth2JWKRepository() {
    JdbcRsaKeyPairRepository repository = new JdbcRsaKeyPairRepository(getJdbcTemplate());
    // repository.save(GenerateKeyUtils.getRsaKey());
    return repository;
  }

  /**
   * 存储授权信息 认证之后的授权信息
   */
  @Bean
  OAuth2AuthorizationService oAuth2AuthorizationService() {
    return new JdbcOAuth2AuthorizationService(getJdbcTemplate(), registeredClientRepository);
  }

}
