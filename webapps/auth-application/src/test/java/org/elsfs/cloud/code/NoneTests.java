/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.code;

import java.time.Instant;
import org.elsfs.cloud.ResponseBody;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.endpoint.PkceParameterNames;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

/** NoneTests */
public class NoneTests extends AbstractCodeTypeAuthorizationServerTests {

  private static final String CLIENT_ID = "messaging-client";
  private static final String CLIENT_SECRET = "secret";

  @Override
  protected ResponseEntity<ResponseBody> getToken(String code) {
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(OAuth2ParameterNames.CLIENT_ID, "messaging-client");
    requestMap.add(OAuth2ParameterNames.SCOPE, StringUtils.collectionToDelimitedString(SCOPE, " "));

    requestMap.add(OAuth2ParameterNames.CODE, code);
    requestMap.add(OAuth2ParameterNames.REDIRECT_URI, redirectUri);
    requestMap.add(PkceParameterNames.CODE_VERIFIER, CODE_VERIFIER_VALUE);
    requestMap.add(
        OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.AUTHORIZATION_CODE.getValue());

    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUrl() + "/oauth2/token")
            .header(HttpHeaders.AUTHORIZATION, BASIC_TOKEN)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .body(requestMap);
    // 3.响应体
    ResponseEntity<ResponseBody> response = restTemplate.exchange(request, ResponseBody.class);

    return response;
  }

  @Override
  protected String getClientId() {
    return null;
  }

  @Override
  void addRegisteredClientRepository() {
    registeredClientRepository.save(getRegisteredClient());
  }

  private RegisteredClient getRegisteredClient() {
    return RegisteredClient.withId(CLIENT_ID)
        .clientId(CLIENT_ID)
        .clientIdIssuedAt(Instant.MAX)
        .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
        .id(CLIENT_ID)
        .clientSecret(passwordEncoder.encode(CLIENT_SECRET))
        .clientName(CLIENT_ID)
        .redirectUri(redirectUri)
        .clientSettings(
            ClientSettings.builder()
                // oauth 和设备模式是否需要授权
                .requireAuthorizationConsent(true)
                .build())
        .scopes(strings -> strings.addAll(SCOPE))
        .clientAuthenticationMethods(builder -> builder.add(ClientAuthenticationMethod.NONE))
        .build();
  }
}
