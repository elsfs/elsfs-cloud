/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.client;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.security.interfaces.RSAPrivateKey;
import java.util.Date;
import org.elsfs.cloud.ResponseBody;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * private_key_jwt 模式
 *
 * @author zeng
 */
public class PrivateKeyJwtTests extends AbstractClientTypeAuthorizationServerTests {
  private static final String CLIENT_ID = "test_private_key_jwt";
  private static final String CLIENT_SECRET = "secret";
  private String clientSecretEncode;

  @Value("${elsfs.security.private-key-path}")
  RSAPrivateKey privateKey;

  @Override
  @BeforeEach
  public void setUp() {
    super.setUp();
    RegisteredClient registeredClient = registeredClientRepository.findByClientId(CLIENT_ID);
    if (registeredClient == null) {
      addRegisteredClient();
      registeredClient = registeredClientRepository.findByClientId(CLIENT_ID);
    }
    clientSecretEncode = registeredClient.getClientSecret();
  }

  @Override
  @Test
  @Order(50)
  void exchange() {
    String privateKeyJwtToken = createPrivateKeyJwtToken("http://localhost:7002");
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(
        OAuth2ParameterNames.CLIENT_ASSERTION_TYPE,
        "urn:ietf:params:oauth:client-assertion-type:jwt-bearer");
    requestMap.add(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID);
    requestMap.add(OAuth2ParameterNames.SCOPE, "openid profile");
    requestMap.add(
        OAuth2ParameterNames.CLIENT_ASSERTION,
        createPrivateKeyJwtToken(authorizationServerSettings.getIssuer()));
    requestMap.add(OAuth2ParameterNames.GRANT_TYPE, "client_credentials");

    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUri())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .body(requestMap);
    // 3.响应体
    ResponseEntity<ResponseBody> response = restTemplate.exchange(request, ResponseBody.class);
    assertThat(response.getBody()).isNotNull();
  }

  private void addRegisteredClient() {
    RegisteredClient registeredClient =
        RegisteredClient.withId("4")
            .clientId(CLIENT_ID)
            .clientSecret(clientSecretEncode)
            .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_JWT)
            .scope(OidcScopes.OPENID)
            .scope(OidcScopes.PROFILE)
            .clientSettings(
                ClientSettings.builder()
                    .tokenEndpointAuthenticationSigningAlgorithm(
                        SignatureAlgorithm.RS256) // private_key_jwt需要 断言对应的算法
                    .jwkSetUrl("http://localhost:7002/oauth2/jwks") // private_key_jwt 需要
                    .build())
            .tokenSettings(TokenSettings.builder().build())
            .build();
    registeredClientRepository.save(registeredClient);
  }

  /**
   * 测试时，直接使用 authorizationServerSettings.getIssuer() 即可，如果你需要使用该方法生成token则需要指定因为 测试时获取的端口是0
   *
   * @param issuer issuer
   * @return token
   */
  private String createPrivateKeyJwtToken(String issuer) {
    // 至少以下四项信息
    JWTClaimsSet claimsSet =
        new JWTClaimsSet.Builder()
            // 主体：固定clientId
            .subject(CLIENT_ID)
            // 发行者：固定clientId
            .issuer(CLIENT_ID)
            // 授权中心的地址
            .audience(issuer)
            // 过期时间 24h
            .expirationTime(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
            .build();

    SignedJWT signedJWT;
    try {
      JWSHeader header = new JWSHeader(JWSAlgorithm.RS256);
      RSASSASigner signer = new RSASSASigner(privateKey);
      String s = signer.toString();
      signedJWT = new SignedJWT(header, claimsSet);
      signedJWT.sign(signer);

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    String token = signedJWT.serialize();
    System.out.println(token);
    return token;
  }
}
