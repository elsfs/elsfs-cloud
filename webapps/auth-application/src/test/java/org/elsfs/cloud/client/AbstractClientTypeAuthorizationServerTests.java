/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.client;

import org.elsfs.cloud.EndpointConstant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

/**
 * 客户端模式测试
 *
 * @author zeng
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
abstract class AbstractClientTypeAuthorizationServerTests {

  protected static final Logger LOGGER =
      LoggerFactory.getLogger(AbstractClientTypeAuthorizationServerTests.class);

  protected RestTemplate restTemplate = new RestTemplate();

  @Autowired protected Environment environment;

  @Autowired protected RegisteredClientRepository registeredClientRepository;

  @Autowired protected AuthorizationServerSettings authorizationServerSettings;

  @Autowired(required = false)
  protected PasswordEncoder passwordEncoder;

  @BeforeEach
  public void setUp() {
    if (passwordEncoder == null) {
      passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
  }

  abstract void exchange();

  protected String getUri() {
    return new StringBuffer("http://localhost:")
        .append(this.environment.getProperty("local.server.port", "8080"))
        .append(EndpointConstant.TOKEN_ENDPOINT)
        .toString();
  }
}
