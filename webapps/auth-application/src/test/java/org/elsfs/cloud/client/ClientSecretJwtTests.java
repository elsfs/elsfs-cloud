/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.client;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import javax.crypto.spec.SecretKeySpec;
import org.elsfs.cloud.ResponseBody;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.jose.jws.MacAlgorithm;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * client secret jwt 模式
 *
 * @author zeng
 */
public class ClientSecretJwtTests extends AbstractClientTypeAuthorizationServerTests {
  private static final String CLIENT_ID = "test_client_secret_jwt";
  private static final String CLIENT_SECRET = "secret";
  private String clientSecretEncode;

  /** setUp */
  @BeforeEach
  public void setUp() {
    super.setUp();
    clientSecretEncode = passwordEncoder.encode(CLIENT_SECRET);
    addRegisteredClient();
  }

  @Override
  @Order(30)
  @Test
  void exchange() {
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(
        OAuth2ParameterNames.CLIENT_ASSERTION_TYPE,
        "urn:ietf:params:oauth:client-assertion-type:jwt-bearer");
    requestMap.add(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID);
    requestMap.add(OAuth2ParameterNames.SCOPE, "openid profile");

    requestMap.add(OAuth2ParameterNames.CLIENT_ASSERTION, createClientSecretJwtToken());

    requestMap.add(
        OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.CLIENT_CREDENTIALS.getValue());

    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUri())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .body(requestMap);
    // 3.响应体
    ResponseEntity<ResponseBody> exchange = restTemplate.exchange(request, ResponseBody.class);
    ResponseBody body = exchange.getBody();
    assertThat(body).isNotNull();
    LOGGER.info("{}", body);
  }

  private void addRegisteredClient() {
    RegisteredClient registeredClient =
        RegisteredClient.withId("2")
            .clientId(CLIENT_ID)
            .clientSecret(clientSecretEncode)
            .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_JWT)
            .scope(OidcScopes.OPENID)
            .scope(OidcScopes.PROFILE)
            .clientSettings(
                ClientSettings.builder()
                    .tokenEndpointAuthenticationSigningAlgorithm(
                        MacAlgorithm.HS256) // client_secret_jwt
                    // 需要
                    // private_key_jwt 需要
                    .build())
            .tokenSettings(
                TokenSettings.builder()
                    .accessTokenFormat(OAuth2TokenFormat.REFERENCE) // REFERENCE 不需要设置
                    // jwk SELF_CONTAINED
                    // 需要设置 jwk
                    .build())
            .build();
    registeredClientRepository.save(registeredClient);
  }

  private String createClientSecretJwtToken() {
    try {
      // algorithm 必须是 HMACSHA256
      SecretKeySpec secretKeySpec =
          new SecretKeySpec(clientSecretEncode.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
      JWSSigner signer = new MACSigner(secretKeySpec);
      // subject, issuer, audience, expirationTime 这四个参数是必须的
      // 服务器那边会校验
      JWTClaimsSet claimsSet =
          new JWTClaimsSet.Builder()
              //  // 主体：固定clientId
              .subject(CLIENT_ID)
              .jwtID(CLIENT_ID)
              // 发行者：固定clientId
              .issuer(CLIENT_ID)
              // 受众 必填 必须和 配置ISSUER一致 授权中心的地址
              .audience(authorizationServerSettings.getIssuer())
              // 过期时间
              .expirationTime(new Date(System.currentTimeMillis() + 60 * 60 * 24 * 365 * 10))
              .build();
      JWSHeader jwsHeader = new JWSHeader(JWSAlgorithm.HS256);
      SignedJWT signedJWT = new SignedJWT(jwsHeader, claimsSet);
      signedJWT.sign(signer);
      String token = signedJWT.serialize();
      LOGGER.info("{}", token);
      return token;
    } catch (Exception e) {
      throw new NullPointerException(e.getMessage());
    }
  }
}
