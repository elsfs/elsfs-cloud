/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud;

import static org.assertj.core.api.Assertions.assertThat;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * 默认 测试
 *
 * @author zeng
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(
    classes = {AuthApplication.class},
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class DefaultAuthorizationServerConsentTests {
  private static final String CODE_TITLE_NAME = "授权";
  private static final List<String> SCOPE = List.of("message.read", "message.write", "openid");
  private static final String CLIENT_ID = "messaging-client";
  private static final String STATE = new Base64StringKeyGenerator().generateKey();
  private static final String CLIENT_SECRET = "123456";
  @Autowired private WebClient webClient;
  @MockBean private OAuth2AuthorizationConsentService authorizationConsentService;
  @Autowired private RegisteredClientRepository registeredClientRepository;
  @Autowired private PasswordEncoder passwordEncoder;
  @Autowired private AuthorizationServerSettings authorizationServerSettings;
  private final String redirectUri = "https://www.baidu.com";

  /** 前置配置 */
  @BeforeEach
  public void setUp() {
    // 禁用异常
    this.webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
    // 允许重定向
    this.webClient.getOptions().setRedirectEnabled(true);
    // 清除cookie
    this.webClient.getCookieManager().clearCookies();
    registeredClientRepository.save(getRegisteredClient());
    // 设置预期结果
    Mockito.when(
            this.authorizationConsentService.findById(
                ArgumentMatchers.any(), ArgumentMatchers.any()))
        .thenReturn(null);
  }

  /**
   * 用户对所有范围的响应返回授权代码
   *
   * @throws IOException e
   */
  @Test
  @WithMockUser("admin")
  public void whenUserConsentsToAllScopesThenReturnAuthorizationCode() throws IOException {
    // 模拟用户点击同意 获取权限页面
    System.out.println("请求uri\n" + authorizationRequestUri());
    final HtmlPage consentPage = this.webClient.getPage(authorizationRequestUri());
    // 断言页面标题authorizationRequestUri
    assertThat(consentPage.getTitleText()).isEqualTo(CODE_TITLE_NAME);
    // 模拟的授权页面勾选所有范围
    List<HtmlCheckBoxInput> scopes = new ArrayList<>();
    consentPage
        .querySelectorAll("input[name='scope']")
        .forEach(scope -> scopes.add((HtmlCheckBoxInput) scope));
    for (HtmlCheckBoxInput scope : scopes) {
      scope.click();
    }

    List<String> scopeIds = new ArrayList<>();
    scopes.forEach(
        scope -> {
          assertThat(scope.isChecked()).isTrue();
          scopeIds.add(scope.getId());
        });
    assertThat(scopeIds).containsExactlyInAnyOrder("message.read", "message.write");

    DomElement submitConsentButton = consentPage.querySelector("button[id='submit-consent']");
    this.webClient.getOptions().setRedirectEnabled(false);

    WebResponse approveConsentResponse = submitConsentButton.click().getWebResponse();
    assertThat(approveConsentResponse.getStatusCode())
        .isEqualTo(HttpStatus.MOVED_PERMANENTLY.value());
    String location = approveConsentResponse.getResponseHeaderValue("location");
    assertThat(location).startsWith(this.redirectUri);
    assertThat(location).contains("code=");
  }

  /**
   * 当用户取消同意时返回拒绝授权错误
   *
   * @throws IOException e
   */
  @Test
  @WithMockUser("admin")
  public void whenUserCancelsConsentThenReturnAccessDeniedError() throws IOException {
    // 模拟用户点击同意 获取权限页面
    final HtmlPage consentPage = this.webClient.getPage(authorizationRequestUri());
    assertThat(consentPage.getTitleText()).isEqualTo(CODE_TITLE_NAME);
    // 模拟的授权页面点击取消同意
    DomElement cancelConsentButton = consentPage.querySelector("button[id='cancel-consent']");
    this.webClient.getOptions().setRedirectEnabled(false);
    WebResponse cancelConsentResponse = cancelConsentButton.click().getWebResponse();
    assertThat(cancelConsentResponse.getStatusCode())
        .isEqualTo(HttpStatus.MOVED_PERMANENTLY.value());
    String location = cancelConsentResponse.getResponseHeaderValue("location");
    assertThat(location).startsWith(this.redirectUri);
    // 断言返回错误
    assertThat(location).contains("error=access_denied");
  }

  private String authorizationRequestUri() {
    return
    //   /oauth2/authorize
    UriComponentsBuilder.fromPath(authorizationServerSettings.getAuthorizationEndpoint())
        .queryParam(OAuth2ParameterNames.RESPONSE_TYPE, "code")
        .queryParam(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID)
        .queryParam(OAuth2ParameterNames.SCOPE, StringUtils.collectionToDelimitedString(SCOPE, " "))
        .queryParam(OAuth2ParameterNames.STATE, STATE)
        .queryParam(OAuth2ParameterNames.REDIRECT_URI, this.redirectUri)
        .toUriString();
  }

  private RegisteredClient getRegisteredClient() {
    return RegisteredClient.withId(CLIENT_ID)
        .clientId(CLIENT_ID)
        .clientIdIssuedAt(Instant.MAX)
        .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
        .id(CLIENT_ID)
        .clientSecret(passwordEncoder.encode(CLIENT_SECRET))
        .clientName(CLIENT_ID)
        .redirectUri(redirectUri)
        .clientSettings(
            ClientSettings.builder()
                // oauth 和设备模式是否需要授权
                .requireAuthorizationConsent(true)
                .build())
        .scopes(strings -> strings.addAll(SCOPE))
        .clientAuthenticationMethods(
            builder -> builder.add(ClientAuthenticationMethod.CLIENT_SECRET_BASIC))
        .build();
  }
}
