/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.client;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import org.elsfs.cloud.EndpointConstant;
import org.elsfs.cloud.ResponseBody;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClient;

/**
 * 客户端模式 basic方式提交认证
 *
 * <p>OAuth2TokenEndpointFilter -> OAuth2ClientCredentialsAuthenticationConverter ->ProviderManager
 * -> OAuth2ClientCredentialsAuthenticationProvider -> DelegatingOAuth2TokenGenerator
 * ->OAuth2AccessTokenGenerator OAuth2TokenEndpointFilter::sendAccessTokenResponse
 *
 * @author zeng
 */
public class ClientSecretBasicTests extends AbstractClientTypeAuthorizationServerTests {
  protected static String CLIENT_ID = "test_client_secret_basic";
  protected static String CLIENT_SECRET = "secret";
  protected static List<String> LIST_SCOPES =
      List.of(OidcScopes.OPENID, OidcScopes.PROFILE, OidcScopes.EMAIL);
  protected static String BASIC_TOKEN =
      "Basic "
          + Base64.getEncoder()
              .encodeToString((CLIENT_ID + ":" + CLIENT_SECRET).getBytes(StandardCharsets.UTF_8));

  @Test
  @Order(10)
  public void exchange() {
    addRegisteredClient();
    RestClient restClient =
        RestClient.builder()
            .baseUrl(
                "http://localhost:" + this.environment.getProperty("local.server.port", "8080"))
            .defaultHeader(HttpHeaders.AUTHORIZATION, BASIC_TOKEN)
            .build();
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
    body.add(OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.CLIENT_CREDENTIALS.getValue());
    body.add(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID);
    body.add(OAuth2ParameterNames.SCOPE, StringUtils.collectionToDelimitedString(LIST_SCOPES, " "));
    body.add(OAuth2ParameterNames.STATE, "state");
    ResponseBody response =
        restClient
            .post()
            .uri(EndpointConstant.TOKEN_ENDPOINT)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .body(body, ParameterizedTypeReference.forType(String.class))
            .retrieve()
            .body(ResponseBody.class);
    assertThat(response).isNotNull();
    assertThat(response.getAccessToken()).isNotNull();
  }

  /** 添加测试注册客户端 */
  private void addRegisteredClient() {
    RegisteredClient registeredClient =
        RegisteredClient.withId("1")
            .clientId(CLIENT_ID)
            .clientSecret(passwordEncoder.encode(CLIENT_SECRET))
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
            .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
            .scopes(strings -> strings.addAll(LIST_SCOPES))
            .tokenSettings(
                TokenSettings.builder().accessTokenFormat(OAuth2TokenFormat.REFERENCE).build())
            .build();
    registeredClientRepository.save(registeredClient);
  }
}
