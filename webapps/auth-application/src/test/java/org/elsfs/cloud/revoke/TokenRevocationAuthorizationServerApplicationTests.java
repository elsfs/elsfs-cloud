/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.revoke;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import org.elsfs.cloud.ResponseBody;
import org.elsfs.cloud.code.AbstractCodeTypeAuthorizationServerTests;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.endpoint.PkceParameterNames;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * 默认授权测试 撤销token
 *
 * @author zeng
 */
public class TokenRevocationAuthorizationServerApplicationTests
    extends AbstractCodeTypeAuthorizationServerTests {

  @Override
  protected ResponseEntity<ResponseBody> getToken(String code) {
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(OAuth2ParameterNames.CLIENT_ID, "messaging-client");
    requestMap.add(OAuth2ParameterNames.GRANT_TYPE, "authorization_code");
    requestMap.add(OAuth2ParameterNames.CODE, code);
    requestMap.add(OAuth2ParameterNames.REDIRECT_URI, redirectUri);
    // BASE64URL-ENCODE(SHA256(ASCII(code_verifier)))
    requestMap.add(PkceParameterNames.CODE_VERIFIER, "ss");
    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUrl() + "/oauth2/token")
            .header(HttpHeaders.AUTHORIZATION, BASIC_TOKEN)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .body(requestMap);
    return restTemplate.exchange(request, ResponseBody.class);
  }

  @Test
  @WithMockUser("admin")
  @Order(100)
  public void exchange() throws IOException {
    String methodGET =
        getCodeHttpMethodGET(getCodeHttpMethodGETUnaryOperator().apply(getUriComponentsBuilder()));
    ResponseEntity<ResponseBody> responseGET = getToken(methodGET);
    assertThat(responseGET.getBody()).isNotNull();
    System.out.println(responseGET.getBody());
    tokenRevocationA(responseGET.getBody().getAccessToken());
  }

  @Override
  protected String getClientId() {
    return null;
  }

  private void tokenRevocationA(String accessToken) {
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(OAuth2ParameterNames.TOKEN, accessToken);
    requestMap.add(OAuth2ParameterNames.TOKEN_TYPE_HINT, "access_token");
    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUrl() + "/oauth2/revoke")
            .header(HttpHeaders.AUTHORIZATION, BASIC_TOKEN)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .body(requestMap);
    ResponseEntity<String> response = restTemplate.exchange(request, String.class);
    // {"active":true,"sub":"admin","aud":["messaging-client"],"nbf":1682348184,"scope":"openid
    // message.read message.write","iss":"http://localhost:0",
    // "exp":1682348484,"iat":1682348184,"client_id":"messaging-client","token_type":"Bearer"}
    assertThat(response.getStatusCode().value()).isEqualTo(200);
  }
}
