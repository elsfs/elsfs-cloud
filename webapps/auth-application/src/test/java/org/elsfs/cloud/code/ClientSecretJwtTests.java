/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.code;

import static org.assertj.core.api.Assertions.assertThat;

import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.crypto.spec.SecretKeySpec;
import org.elsfs.cloud.ResponseBody;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.endpoint.PkceParameterNames;
import org.springframework.security.oauth2.jose.jws.MacAlgorithm;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

/** ClientSecretJwtTests */
public class ClientSecretJwtTests extends AbstractCodeTypeAuthorizationServerTests {

  private static final String CLIENT_ID = "client-secret-code";
  private static final String SECRET = "secret";
  private static final List<String> SCOPE = List.of("message.read", "message.write");

  String clientSecret;

  @Test
  @WithMockUser("admin")
  @Order(100)
  protected void exchange() throws IOException {
    clientSecret = passwordEncoder.encode(SECRET);
    addRegisteredClientRepository();
    String code = getCodeHttpMethodGET(getUriComponentsBuilder());
    ResponseEntity<ResponseBody> responseGET = getToken(code);
    assertThat(responseGET.getBody()).isNotNull();
    System.out.println(responseGET.getBody());
  }

  protected UriComponentsBuilder getUriComponentsBuilder() {
    return UriComponentsBuilder.fromPath("/oauth2/authorize")
        .queryParam(OAuth2ParameterNames.RESPONSE_TYPE, OAuth2ParameterNames.CODE)
        .queryParam(OAuth2ParameterNames.SCOPE, StringUtils.collectionToDelimitedString(SCOPE, " "))
        .queryParam(OAuth2ParameterNames.STATE, "state")
        .queryParam(OAuth2ParameterNames.REDIRECT_URI, this.redirectUri)
        .queryParam(PkceParameterNames.CODE_CHALLENGE, CODE_CHALLENGE_VAlUE) // 可选
        .queryParam(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID)
        // CODE_CHALLENGE
        // CODE_CHALLENGE_METHOD
        // 一组
        .queryParam(PkceParameterNames.CODE_CHALLENGE_METHOD, "S256"); // 可选
    // CODE_CHALLENGE
    // CODE_CHALLENGE_METHOD
    // 一组 固定值 S256
  }

  @Override
  protected ResponseEntity<ResponseBody> getToken(String code) {
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(
        OAuth2ParameterNames.CLIENT_ASSERTION_TYPE,
        "urn:ietf:params:oauth:client-assertion-type:jwt-bearer");
    requestMap.add(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID);
    requestMap.add(OAuth2ParameterNames.CLIENT_ASSERTION, createClientSecretJwtToken());

    requestMap.add(
        OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.AUTHORIZATION_CODE.getValue());
    requestMap.add(OAuth2ParameterNames.CODE, code);
    requestMap.add(OAuth2ParameterNames.REDIRECT_URI, redirectUri);
    requestMap.add(PkceParameterNames.CODE_VERIFIER, CODE_VERIFIER_VALUE);

    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUrl() + "/oauth2/token")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .body(requestMap);
    return restTemplate.exchange(request, ResponseBody.class);
  }

  void addRegisteredClientRepository() {
    RegisteredClient registeredClient =
        RegisteredClient.withId(UUID.randomUUID().toString())
            .clientId(CLIENT_ID)
            .clientSecret(clientSecret)
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_JWT)
            .scopes(s -> s.addAll(SCOPE))
            .redirectUri(redirectUri)
            .clientSettings(
                ClientSettings.builder()
                    .requireAuthorizationConsent(true)
                    .tokenEndpointAuthenticationSigningAlgorithm(
                        MacAlgorithm.HS512) // client_secret_jwt
                    // 需要
                    // .tokenEndpointAuthenticationSigningAlgorithm(MacAlgorithm.HS512) //
                    // private_key_jwt 需要
                    .build())
            .tokenSettings(
                TokenSettings.builder()
                    .accessTokenFormat(OAuth2TokenFormat.SELF_CONTAINED) // REFERENCE 不需要设置
                    // jwk SELF_CONTAINED
                    // 需要设置 jwk
                    .build())
            .build();
    registeredClientRepository.save(registeredClient);
  }

  @Override
  protected String getClientId() {
    return CLIENT_ID;
  }

  private String createClientSecretJwtToken() {
    try {
      // algorithm 必须是 HMACSHA256
      SecretKeySpec secretKeySpec =
          new SecretKeySpec(clientSecret.getBytes(StandardCharsets.UTF_8), "HmacSHA512");
      JWSSigner signer = new MACSigner(secretKeySpec);
      // subject, issuer, audience, expirationTime 这四个参数是必须的
      // 服务器那边会校验
      JWTClaimsSet claimsSet =
          new JWTClaimsSet.Builder()
              // 主题
              .subject(CLIENT_ID)
              .jwtID(CLIENT_ID)
              // 签发人
              .issuer(CLIENT_ID)
              // 受众 必填 必须和 配置ISSUER一致
              .audience(authorizationServerSettings.getIssuer())
              // 过期时间
              .expirationTime(new Date(System.currentTimeMillis() + 60 * 60 * 60 * 1000))
              .build();
      JWSHeader header =
          new JWSHeader(
              JWSAlgorithm.HS512,
              JOSEObjectType.JWT,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              true,
              null,
              null);
      SignedJWT signedJWT = new SignedJWT(header, claimsSet);
      signedJWT.sign(signer);
      String token = signedJWT.serialize();
      return token;
    } catch (Exception e) {
      throw new NullPointerException(e.getMessage());
    }
  }
}
