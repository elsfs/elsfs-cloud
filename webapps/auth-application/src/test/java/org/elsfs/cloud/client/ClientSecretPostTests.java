/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.client;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.elsfs.cloud.ResponseBody;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * ClientSecretPostTests
 *
 * @author zeng
 */
public class ClientSecretPostTests extends AbstractClientTypeAuthorizationServerTests {
  private static final String CLIENT_ID = "test_client_secret_post";
  private static final String CLIENT_SECRET = "secret";

  @Override
  @Order(40)
  @Test
  void exchange() {
    addRegisteredClient();
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID);
    requestMap.add(OAuth2ParameterNames.SCOPE, "openid profile");
    requestMap.add(OAuth2ParameterNames.GRANT_TYPE, "client_credentials");
    requestMap.add(OAuth2ParameterNames.CLIENT_SECRET, CLIENT_SECRET);
    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUri())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .accept(MediaType.APPLICATION_JSON)
            .body(requestMap);
    // 3.响应体
    ResponseEntity<ResponseBody> exchange = restTemplate.exchange(request, ResponseBody.class);
    ResponseBody body = exchange.getBody();
    LOGGER.info("{}", body);
    assertThat(body).isNotNull();
  }

  /** 添加测试注册客户端 */
  private void addRegisteredClient() {
    RegisteredClient registeredClient =
        RegisteredClient.withId("3")
            .clientId(CLIENT_ID)
            .clientSecret(passwordEncoder.encode(CLIENT_SECRET))
            .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
            .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
            .scope(OidcScopes.OPENID)
            .scope(OidcScopes.PROFILE)
            .build();
    registeredClientRepository.save(registeredClient);
  }
}
