/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.code;

import static org.assertj.core.api.Assertions.assertThat;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import org.elsfs.cloud.ResponseBody;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * /oauth2/authorize OAuth2AuthorizationEndpointFilter -> DelegatingAuthenticationConverter ->
 * OAuth2AuthorizationCodeRequestAuthenticationConverter -> ProviderManager ->
 * OAuth2AuthorizationCodeRequestAuthenticationProvider
 * ->OAuth2AuthorizationCodeRequestAuthenticationValidator ////// OAuth2AuthorizationEndpointFilter
 * ->DelegatingAuthenticationConverter ->OAuth2AuthorizationConsentAuthenticationConverter
 * ProviderManager->OAuth2AuthorizationConsentAuthenticationProvider //// /oauth2/token
 * OAuth2ClientAuthenticationFilter ClientSecretAuthenticationProvider OAuth2TokenEndpointFilter
 *
 * @author zeng
 */
public class ClientSecretBasicTests extends AbstractCodeTypeAuthorizationServerTests {

  private static final String CLIENT_ID = "messaging";
  private static final List<String> SCOPE = List.of("message.read", "message.write", "openid");
  private static final String STATE = new Base64StringKeyGenerator().generateKey();
  private final String redirectUri = "https://www.baidu.com";
  private static final String SECRET = "secret";

  protected static String BASIC_TOKEN =
      "Basic "
          + Base64.getEncoder()
              .encodeToString(
                  String.format("%s:%s", CLIENT_ID, SECRET).getBytes(StandardCharsets.UTF_8));

  @Test
  @WithMockUser("admin")
  @Order(100)
  protected void exchange() throws IOException {
    addRegisteredClientRepository();
    final HtmlPage consentPage = this.webClient.getPage(authorizationRequestUri());
    String methodGET = assertThatHtmlPage(consentPage);
    ResponseEntity<ResponseBody> responseGET = getToken(methodGET);
    assertThat(responseGET.getBody()).isNotNull();
    System.out.println(responseGET.getBody());
  }

  @Override
  protected ResponseEntity<ResponseBody> getToken(String code) {
    MultiValueMap<String, Object> requestMap = new LinkedMultiValueMap<>();
    requestMap.add(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID);
    requestMap.add(OAuth2ParameterNames.GRANT_TYPE, "authorization_code");
    requestMap.add(OAuth2ParameterNames.CODE, code);
    requestMap.add(OAuth2ParameterNames.REDIRECT_URI, redirectUri);
    RequestEntity<MultiValueMap<String, Object>> request =
        RequestEntity.post(getUrl() + "/oauth2/token")
            .header(HttpHeaders.AUTHORIZATION, BASIC_TOKEN)
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .body(requestMap);
    return restTemplate.exchange(request, ResponseBody.class);
  }

  private String authorizationRequestUri() {
    return
    //   /oauth2/authorize
    UriComponentsBuilder.fromPath(authorizationServerSettings.getAuthorizationEndpoint())
        .queryParam(OAuth2ParameterNames.RESPONSE_TYPE, "code")
        .queryParam(OAuth2ParameterNames.CLIENT_ID, CLIENT_ID)
        .queryParam(OAuth2ParameterNames.SCOPE, StringUtils.collectionToDelimitedString(SCOPE, " "))
        .queryParam(OAuth2ParameterNames.STATE, STATE)
        .queryParam(OAuth2ParameterNames.REDIRECT_URI, this.redirectUri)
        .toUriString();
  }

  @Override
  void addRegisteredClientRepository() {
    RegisteredClient registeredClient =
        RegisteredClient.withId(getClientId())
            .clientId(getClientId())
            .clientSecret(passwordEncoder.encode(SECRET))
            .clientAuthenticationMethods(
                builder -> builder.add(ClientAuthenticationMethod.CLIENT_SECRET_BASIC))
            .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
            .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
            .redirectUri(redirectUri)
            .scopes(strings -> strings.addAll(SCOPE))
            .clientSettings(
                ClientSettings.builder()
                    // oauth 和设备模式是否需要授权
                    .requireAuthorizationConsent(true)
                    .build())
            .build();

    registeredClientRepository.save(registeredClient);
  }

  @Override
  protected String getClientId() {
    return CLIENT_ID;
  }
}
