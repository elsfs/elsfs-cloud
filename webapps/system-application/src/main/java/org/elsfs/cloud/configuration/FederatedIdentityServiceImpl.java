/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.configuration;

import org.elsfs.cloud.spring.common.federation.FederatedIdentity;
import org.elsfs.cloud.spring.common.federation.FederatedIdentityService;
import org.springframework.stereotype.Service;

/**
 * FederatedIdentityServiceImpl
 *
 * @author zeng
 */
@Service
public class FederatedIdentityServiceImpl implements FederatedIdentityService {
  /**
   * 查询FederatedIdentity
   *
   * @param identifier 第三方应用唯一标识
   * @param identityType 第三方应用名
   * @return {@code null} or FederatedIdentity
   */
  @Override
  public FederatedIdentity loadByIdentifierAndIdentityType(String identifier, String identityType) {
    return null;
  }

  /**
   * 保存 federatedIdentity
   *
   * @param federatedIdentity federatedIdentity
   */
  @Override
  public void save(FederatedIdentity federatedIdentity) {}

  /**
   * 更新 FederatedIdentity
   *
   * @param federatedIdentity federatedIdentity
   * @return federatedIdentity
   */
  @Override
  public FederatedIdentity updateFederatedIdentity(FederatedIdentity federatedIdentity) {
    return null;
  }
}
