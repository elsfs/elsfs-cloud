/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.mybatis.properties.MybatisPlusProperties;
import org.elsfs.cloud.spring.common.handler.JsonAuthenticationHandler;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.stereotype.Component;

/**
 * vben-admin 认证处理器
 *
 * @author zeng
 */
@Component
public class VbenAdminJsonAuthenticationHandler extends JsonAuthenticationHandler {
  public VbenAdminJsonAuthenticationHandler(
      ObjectMapper objectMapper,
      JwtEncoder jwtEncoder,
      String issuer,
      MybatisPlusProperties mybatisPlusProperties) {
    super(objectMapper, jwtEncoder, issuer, mybatisPlusProperties);
  }

  @Override
  public void onAuthenticationSuccess(
      HttpServletRequest request, HttpServletResponse response, Authentication authentication)
      throws IOException {
    Object principal = authentication.getPrincipal();
    String username = null;
    Set<String> scope = null;
    if (principal instanceof UserDetails userDetails) {
      username = userDetails.getUsername();
      scope = AuthorityUtils.authorityListToSet(userDetails.getAuthorities());
    }
    Jwt jwt = generatorToken(username, scope, request);
    writeHttpServletResponse(response, R.success(Map.of("access_token", jwt.getTokenValue())));
  }
}
