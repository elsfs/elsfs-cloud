/*
IMPORTANT:
    If using PostgreSQL, update ALL columns defined with 'blob' to 'text',
    as PostgreSQL does not support the 'blob' data type.
*/
CREATE TABLE oauth2_jwk
(
    id          varchar(100) NOT NULL,
    key_type    varchar(100) NULL,
    issue_time  datetime     null,
    json_object blob         NOT NULL,
    PRIMARY KEY (id)
);
