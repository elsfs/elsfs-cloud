/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.api.security.entity;

import java.util.Map;

/**
 * 第三方用户绑定
 *
 * @author zeng
 */
public interface FederatedIdentityInterface {

  String getId();

  String getUserId();

  /** 第三方应用的唯一标识 */
  String getIdentifier();

  /** 第三方应用名称 (微信, 微博,qq.gitee,github等) */
  String getIdentityType();

  /** 第三方登录的属性集合 */
  Map<String, Object> getAttributes();

  /** 授权客户端注册Id */
  String getAuthorizedClientRegistrationId();
}
