/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.api.security.key;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Instant;
import java.util.UUID;
import lombok.Data;

/**
 * rsa 对象
 *
 * @author zeng
 */
@Data
public class RsaKeyPair {

  private String id;

  private Instant created;

  private RSAPublicKey publicKey;

  private RSAPrivateKey privateKey;

  public RsaKeyPair() {}

  public RsaKeyPair(RSAPublicKey publicKey, RSAPrivateKey privateKey) {
    this(UUID.randomUUID().toString(), Instant.now(), publicKey, privateKey);
  }

  public RsaKeyPair(Instant created, RSAPublicKey publicKey, RSAPrivateKey privateKey) {
    this(UUID.randomUUID().toString(), created, publicKey, privateKey);
  }

  /**
   * 构造器
   *
   * @param id 唯一id
   * @param created 创建时间
   * @param publicKey pub key
   * @param privateKey pri key
   */
  public RsaKeyPair(String id, Instant created, RSAPublicKey publicKey, RSAPrivateKey privateKey) {
    this.id = id;
    this.created = created;
    this.publicKey = publicKey;
    this.privateKey = privateKey;
  }
}
