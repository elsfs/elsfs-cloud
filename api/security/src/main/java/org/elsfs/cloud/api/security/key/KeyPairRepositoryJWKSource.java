/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.api.security.key;

import com.nimbusds.jose.KeySourceException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSelector;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import java.util.List;

/**
 * JWKSource 配置使用{@link OAuth2JWKRepository}
 *
 * @author zeng
 */
public class KeyPairRepositoryJWKSource implements JWKSource<SecurityContext> {
  private final OAuth2JWKRepository keyPairRepository;

  public KeyPairRepositoryJWKSource(OAuth2JWKRepository keyPairRepository) {
    this.keyPairRepository = keyPairRepository;
  }

  @Override
  public List<JWK> get(JWKSelector jwkSelector, SecurityContext context) throws KeySourceException {
    List<JWK> jwkList = this.keyPairRepository.findJWKs();
    return jwkList.stream().filter(jwkSelector.getMatcher()::matches).toList();
  }
}
