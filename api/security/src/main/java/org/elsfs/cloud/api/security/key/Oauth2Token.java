/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.api.security.key;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * oauth2或则json请求格式 登录返回对象 和oauth2一致，便于扩展
 *
 * @author zeng
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Oauth2Token {
  /** 令牌类型 */
  @JsonProperty("token_type")
  private TokenType tokenType = TokenType.Bearer;

  /** 这是一个字符串，用于在客户端和服务器之间进行身份验证。访问令牌可以用于访问受保护的资源。 */
  @JsonProperty("access_token")
  private String accessToken;

  /** 令牌持续时间 这通常是一个表示时间间隔的整数，用于指定访问令牌的有效期。例如，如果令牌将持续 3600 秒，则 expires_in 值为 3600。 */
  @JsonProperty("expires_in")
  private Instant expiresIn;

  /** 刷新令牌 这是一个字符串，用于在访问令牌过期后获取新的访问令牌。刷新令牌通常具有较长的有效期，并且只能用于获取新的访问令牌，不能用于直接访问受保护的资源 */
  @JsonProperty("refresh_token")
  private String refreshToken;

  private Set<String> scope;

  /** oauth2 token类型 */
  public enum TokenType {
    Bearer,
    MAC
  }
}
