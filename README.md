<h1 align="center">欢迎使用elsfs-cloud</h1>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Elsfs Cloud v0.0.1</h1>
<h4 align="center">Enterprise level safety function system</h4>
<h4 align="center">企业级安全功能系统</h4>

<p align="center">
	<a href="https://gitee.com/elsfs/elsfs-cloud/stargazers"><img src="https://gitee.com/elsfs/elsfs-cloud/badge/star.svg"></a>
	<a href="https://gitee.com/elsfs/elsfs-cloud/fork"><img src="https://gitee.com/elsfs/elsfs-cloud/badge/fork.svg"></a>
    <a href="https://github.com/elsfs/elsfs-cloud/stargazers"><img src="https://img.shields.io/github/stars/elsfs/elsfs.svg?style=social&label=Stars"></a>
	<a href="https://github.com/elsfs/elsfs-cloud/fork"><img src="https://img.shields.io/github/forks/elsfs/elsfs.svg?style=social&label=Fork"></a>
    <a href="https://gitee.com/elsfs/elsfs-cloud"><img src="https://img.shields.io/badge/elsfsCloud-v0.0.1-brightgreen.svg"></a>
    <a href="https://gitee.com/elsfs/elsfs-cloud/blob/master/LICENSE"><img src="https://img.shields.io/github/license/elsfs/elsfs"></a>
</p>

<p align="center">
 <img src="https://img.shields.io/badge/Spring%20Cloud-2022-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Spring%20Boot-3.0-blue.svg" alt="Downloads">
 <img src="https://img.shields.io/badge/Vue-3.x-blue.svg" alt="Downloads">
</p>

#### 介绍

Enterprise level safety function system
企业级安全功能系统

#### star

<form method="post" about="https://gitee.com/elsfs/elsfs-cloud/star">
  <input type="submit" value="star" />
</form>

#### 系统说明

- 基于 Spring Cloud 2023、Spring Boot 3.2、Spring cloud alibaba 2022
- spring-oauth2-authorization-server 的 RBAC 权限管理系统
- 基于数据驱动视图的理念封装 ant-design，即使没有 vue 的使用经验也能快速上手
- 提供对常见容器化支持 Docker、Kubernetes、Rancher2 支持（待实现）

#### 核心依赖

| 依赖                         | 版本       |
|-----------------------------|----------|
| Spring Cloud                | 2023.0.x |
| Spring Cloud Alibaba        | 2022.0.x |
| Spring Authorization Server | 1.2.x    |
| Mybatis Plus	               | 3.5.4    |
| Spring Boot                 | 3.2.x    |

## 模块说明

```lua
elsfs-cloud
└── elsfs-business -- 业务相关
     ├── elsfs-business-api -- 公共业务api
     └──  elsfs-business-security -- 授权验证相关
        ├── elsfs-business-security-authorizationserver-server -- 授权服务配置
        ├── elsfs-business-security-common -- security 公共部分
        ├── elsfs-business-security-login -- security 登录返回 json 实现
        ├── elsfs-business-security-oauth2-resource-server -- 资源服务器公共部分
        ├── elsfs-business-sexurity-oauth2-client -- oauth客户端
     └──  elsfs-business-system -- 系统相关
        ├── elsfs-business-system-api -- 系统api
        ├── elsfs-business-system-biz -- 系统业务
     └──  elsfs-business-pay -- 支付实现相关
        ├── elsfs-business-pay-common -- 支付公共业务逻辑
        ├── elsfs-business-system-manager -- 支付管理接口
└── elsfs-common -- 系统公共基础模块
     ├── elsfs-common-core -- 系统公共核心
     ├── elsfs-common-datasource -- 数据源相关
     ├── elsfs-common-dubbo -- dubbo相关
     ├── elsfs-common-excel -- excel相关
     ├── elsfs-common-mq -- mq相关
     ├── elsfs-common-mybatis -- mybatis相关
     ├── elsfs-common-openfeign -- openfeign相关
     ├── elsfs-common-oss -- oss相关
     ├── elsfs-common-redis -- redis相关
     ├── elsfs-common-pay -- 支付相关
     ├── elsfs-common-swagger -- swagger相关
     ├── elsfs-common-validation -- validation验证相关
     ├── elsfs-common-swagger -- swagger相关
     ├── elsfs-common-web -- web公共配置相关
     ├── elsfs-common-xxljob -- xxljob配置相关
└── elsfs-demo  -- 例子
     └── elsfs-demo-dubbo -- dubbo例子
        └── elsfs-demo-dubbo-api -- 公共api
        └── elsfs-demo-dubbo-consumer -- dubbo消费端
        └── elsfs-demo-dubbo-provider -- dubbo提供端
     ├── elsfs-demo-excel -- exce demo
     └──  elsfs-demo-feign -- feign demo
        ├──  elsfs-demo-feign-api -- feign api
        ├──  lsfs-demo-feign-demo1 -- feign demo
        ├──  lsfs-demo-feign-demo2 -- feign demo
     └──  elsfs-demo-mq -- mq demo
        ├──  elsfs-demo-mq-consumer -- mq 消息提供方
        ├──  elsfs-demo-mq-producer -- mq 消费端
     └── elsfs-demo-security -- security demo
        ├── elsfs-demo-authorization-server -- oauth2 authorization demo
        ├── elsfs-demo-oauth2-client -- oauth2客户端demo
        ├── elsfs-demo-oauth2-client -- oauth2客户端demo
        ├── elsfs-demo-resource-server -- 资源服务器demo
        ├── elsfs-demo-security-login -- 登录demo 
└── elsfs-optionl-monitor  -- 可选服务
     ├── elsfs-optionl-gateway -- 网关服务
     ├── elsfs-optionl-monitor -- admin监控服务
     ├── elsfs-optionl-nacos -- nacos服务
└── starter  -- 启动相关
     ├── elsfs-cloud-oauth2-starter -- oauth2 oauth自定义授权的授权服务器 启动
     ├── elsfs-cloud-starter -- admin后台管理启动

```

#### 使用说明

1. clone 项目

| 平台          | 地址                                      |
|-------------|-----------------------------------------|
| gitee       | https://gitee.com/elsfs/elsfs-cloud     |
| github      | https://github.com/elsfs/elsfs-cloud    |
| 文档贡献        | https://gitee.com/elsfs/elsfs-cloud-doc |
| 文档地址        | https://elsfs.gitee.io                  |
| 前端代码（gitee） | https://gitee.com/elsfs/elsfs-admin     |

2. 构建项目

```shell
cd elsfs-cloud
# 构建代码
mvn package -P dev
# 代码检查
mvn  checkstyle:check -P check
#    代码格式化
mvn spotless:apply -P check

```

#### 参与贡献

1. 欢迎提交 [PR](https://gitee.com/elsfs/elsfs-cloud/pulls)，
   代码规范 [google-java-javaformat](https://github.com/google/google-java-javaformat)
   <details>
    <summary>代码规范说明</summary>

    1. 由于 <a href="https://github.com/google/google-java-javaformat" target="_blank">google-java-format</a>
       强制所有代码按照指定格式排版，未按此要求提交的代码将不能通过合并（打包）
    2. 具体配置参考<a href="https://blog.csdn.net/youxijishu/article/details/135350055" target="_blank">csdn 使用谷歌 Java 开发规范</a>
    3. 其他开发工具，请参考 <a href="https://github.com/gogle/google-java-format" target="_blank">
       google-java-format</a>
       说明，或`提交代码前`在项目根目录运行下列命令（需要开发者电脑支持`mvn`命令）进行代码格式化
       ```shell
        # 代码检查
        mvn  checkstyle:check -P check
        #    代码格式化
        mvn spotless:apply -P check
       ```
   </details>

2. 欢迎提交 [issue](https://gitee.com/elsfs/elsfs-cloud/issues)，请写清楚遇到问题的原因、开发环境、复显步骤。

3. 联系作者 <a href="mailto:maicaii@vip.qq.com">mailto:maicaii@vip.qq.com</a>

