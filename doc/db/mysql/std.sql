# 删除数据库
# DROP DATABASE IF EXISTS `e_std`;

CREATE DATABASE `e_std` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
use `e_std`;


create table std_dict
(
  dict_id             varchar(32)                     not null comment '字典ID' primary key,
  std_no              varchar(126)                    null comment '标准号',
  std_category        varchar(32)                     null comment '标准类别',
  std_replace         varchar(32)                     null comment '全部代替标准',
  ccs                 varchar(32)                     null comment '中国标准分类号(chinese classification for standards) 字典 css',
  ics                 varchar(32)                     null comment '国际标准分类号(ICS) 字典 ics',
  dict_code           varchar(32)                     not null comment '字典code',
  dict_name           varchar(32)                     not null comment '字典名称',
  dict_type           varchar(32)                     null comment '标准类型',
  dict_desc           varchar(256)                    null comment '字典描述',
  website             varchar(256)                    null comment '参考网站',
  remark              varchar(256)                    null comment '备注',
  release_date        datetime                        null comment '发布日期',
  implementation_date datetime                        null comment '实施日期',
  revocatory_date     datetime                        null comment '废止日期',
  table_name          varchar(32)                     null comment '表名 null表示在std_dict_item表，标准的基本上在该表',
  is_tree             tinyint(1)                               default 0 null comment '是否树形字典',
  is_independence     tinyint(1)                               default 0 comment '是否有独立数据表 配合table_name使用，0表示无独立数据表，1表示有独立数据表',
  allow_adding        tinyint(1)                               default 0 comment '是否允许添加，0表示不允许，1表示允许',
  pinyin_code         varchar(32)                     null comment '拼音码',
  five_strokes_code   varchar(32)                     null comment '五笔码',
  `status`            int                                      default 0 comment '状态 0有效 1无效 字典 status',
  create_by           varchar(32)                     null comment '创建人',
  create_at           datetime                        null     default current_timestamp comment '创建时间',
  update_by           varchar(32)                     null comment '修改人',
  update_at           datetime                        null on update current_timestamp comment '修改时间',
  `delete_flag`       char(1) COLLATE utf8mb4_bin     not null default '0' comment '删除时间(软删除)0 未删除 1 已删除',
  `tenant_id`         varchar(32) COLLATE utf8mb4_bin not null default '0' comment '租户id',
  constraint tenant_dict_code
    unique (tenant_id, dict_code),
  index idx_dict_code (dict_code),
  index idx_dict_name (dict_name),
  index idx_create_at (create_at),
  index idx_update_at (update_at)
)
  comment '字典管理' collate = utf8mb4_bin;

create table std_dict_item
(
  dict_item_id      varchar(32)  not null comment '字典项ID'
    primary key,
  dict_id           varchar(32)  not null comment '字典ID',
  parent_id         varchar(32)           default '0' comment '父ID',
  label             varchar(128)  not null comment '字典项名称',
  value             varchar(32)  not null comment '字典项值',
  order_no          int                   default 0 not null comment '排序',
  depth             int                   default 0 not null comment '深度 数结构深度',
  custom_type       varchar(32)  null comment '自定义字典类型或者分类',
  css               varchar(32)  null comment '样式',
  remark            varchar(256) null comment '备注',
  icon              varchar(32)  null comment '图标',
  pinyin_code       varchar(32)  null comment '拼音码',
  wubi_code varchar(32)  null comment '五笔码',
  `status`          int                   default 0 comment '状态 0有效 1无效',
  `delete_flag`     tinyint(1)   not null default 0 comment '(软删除)0 未删除 1 已删除',
  tenant_id         varchar(32)  not null comment '租户ID',
  create_by         varchar(32)  null comment '创建人',
  create_at         datetime     null     default current_timestamp comment '创建时间',
  update_by         varchar(32)  null comment '修改人',
  update_at         datetime     null on update current_timestamp comment '修改时间',
  -- 添加索引
  index idx_dict_id (dict_id),
  index idx_parent_id (parent_id),
  index idx_status (`status`),
  index idx_delete_flag (`delete_flag`)
)
  comment '字典项管理' collate = utf8mb4_general_ci;

