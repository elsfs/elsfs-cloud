CREATE database if NOT EXISTS `e_admin` default character set utf8mb4 collate utf8mb4_general_ci;
use `e_admin`;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;



-- ----------------------------
-- Table structure for pay_config
-- ----------------------------
DROP TABLE IF EXISTS `pay_config`;
CREATE TABLE `pay_config` (
  `pay_config_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '住建，appId',
  `type` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付类型',
  `config_json` text COLLATE utf8mb4_bin COMMENT '配置信息',
  `multiple` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否多实例',
  `status` int DEFAULT '0' COMMENT '状态',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`pay_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='支付配置';

-- ----------------------------
-- Records of pay_config
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '配置ID',
  `config_key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT ' 配置键',
  `config_name` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '配置名称',
  `config_value` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '配置值',
  `config_type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '配置类型',
  `remark` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `delete_flag` varchar(3) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除状态',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户ID',
  `create_by` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='配置管理';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '部门ID',
  `parent_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父级部门ID',
  `dept_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门名称',
  `org_code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门代码',
  `org_category` int DEFAULT NULL COMMENT '部门类型',
  `mobile` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门电话',
  `address` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门地址',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int DEFAULT '0' COMMENT '状态',
  `dept_desc` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  `pinyin_code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '拼音码',
  `wubi_code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '五笔码',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1', '0', '财务部', '000000', 1, '12345678901', '中国', 0, 0, '顶级部门', '2020-06-05 11:06:00', 'admin', NULL, 'admin', '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1835623282062229506', '0', '测试部门', 'A001', 0, '1321311212', 'GUIZHOU', 2, 0, '测试1', '2024-09-16 18:14:00', NULL, NULL, NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1835623422642716673', '1835623282062229506', '测试 1', 'A011', 1, NULL, NULL, 0, 0, NULL, '2024-09-16 18:15:03', NULL, '2024-09-16 18:15:03', NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1843347610153373698', '1835623282062229506', '总经理', 'A001', 1, '23221', 'GUIZHOU', 0, 0, NULL, '2024-10-08 01:48:13', NULL, '2024-10-08 01:48:13', NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1843347717959569410', '1835623282062229506', '贵阳分公司', 'A011', 1, '23221', 'GUIZHOU', 0, 0, NULL, '2024-10-08 01:48:38', NULL, '2024-10-08 01:48:38', NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1877421170419224578', '0', '测试部门', NULL, NULL, NULL, NULL, 9999, 0, NULL, '2025-01-10 02:24:22', NULL, NULL, NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1877421322794094594', '0', '搜索', NULL, NULL, NULL, NULL, 9999, 0, NULL, '2025-01-10 02:24:59', NULL, NULL, NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1878140819670794242', '0', '测试 11', NULL, NULL, NULL, NULL, 9999, 0, NULL, '2025-01-12 02:04:00', NULL, NULL, NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1878141097014951938', '0', '是的', NULL, NULL, NULL, NULL, 9999, 0, NULL, '2025-01-12 02:05:06', NULL, NULL, NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1878142055430193154', '0', 'css 娃娃菜', NULL, NULL, NULL, NULL, 9999, 0, NULL, '2025-01-12 02:08:00', NULL, '2025-01-12 02:08:00', NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1878142091622842369', '1878142055430193154', ' 火锅', NULL, NULL, NULL, NULL, 9999, 0, NULL, '2025-01-12 02:09:03', NULL, '2025-01-12 02:09:03', NULL, '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('2', '0', '人力资源部', '000000', 1, '12345678901', '中国', 0, 0, 'hr', '2020-06-05 11:06:00', 'admin', '2020-06-05 11:06:00', 'admin', '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('2000', '2', '会计部门', '000000', 1, '12345678901', '中国', 0, 0, 'hr', '2020-06-05 11:06:06', 'admin', NULL, 'admin', '0', '0', NULL, NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `dept_name`, `org_code`, `org_category`, `mobile`, `address`, `order_no`, `status`, `dept_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('2001', '2', '出纳部门', '000000', 1, '12345678901', '中国', 0, 0, 'hr', '2020-06-05 11:06:06', 'admin', NULL, 'admin', '0', '0', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_dept_dept_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_dept_role`;
CREATE TABLE `sys_dept_dept_role` (
  `dept_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '部门ID',
  `dept_role_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '部门角色ID',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`dept_id`,`dept_role_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='部门角色部门关系';

-- ----------------------------
-- Records of sys_dept_dept_role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dept_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_menu`;
CREATE TABLE `sys_dept_menu` (
  `dept_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '部门ID',
  `menu_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`dept_id`,`menu_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色菜单';

-- ----------------------------
-- Records of sys_dept_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('1802714504916643842', '1801290765519847426', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('1802714504916643842', '3000', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('1802714504916643842', '3001', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('2', '1759874756913139713', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('2', '1759875183046037505', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('2', '1759875298217431041', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('2', '1759875383483437058', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('2', '2001', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('2', '2003', '0');
INSERT INTO `sys_dept_menu` (`dept_id`, `menu_id`, `tenant_id`) VALUES ('2', '2005', '0');
COMMIT;

-- ----------------------------
-- Table structure for sys_federated_identity
-- ----------------------------
DROP TABLE IF EXISTS `sys_federated_identity`;
CREATE TABLE `sys_federated_identity` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '第三方应用 id',
  `user_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户id',
  `identifier` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '第三方应用的唯一标识',
  `identity_type` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '第三方应用名称 (微信, 微博,qq.gitee,github等)',
  `attributes` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '第三方登录的属性集合',
  `authorized_client_registration_id` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '授权客户端注册Id',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '租户id',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='联合登录信息表 第三方登录';

-- ----------------------------
-- Records of sys_federated_identity
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单id',
  `parent_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父级菜单ID',
  `menu_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '菜单名称',
  `permission` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限',
  `route_path` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '路由地址',
  `icon` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图标',
  `component` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '组件',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '是否显示',
  `keep_alive` tinyint(1) DEFAULT '1' COMMENT '是否缓存',
  `is_ext` tinyint(1) DEFAULT '0' COMMENT '是否外链',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单类型，0目录，1菜单，2按钮',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int DEFAULT '0' COMMENT '状态',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  `create_by` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
  `delete_flag` int NOT NULL DEFAULT '0',
  `pinyin_code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '拼音码',
  `wubi_code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '五笔码',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1000', '0', '仪表板', 'userinfo', '/analytics', 'ion:layers-outline', 'BasicLayout', 1, 0, 0, '0', 1, 0, '2023-11-01 02:24:31', '2023-11-01 02:24:34', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1001', '1000', '分析', NULL, '/dashboard/analysis', 'ant-design:account-book-outlined', '/dashboard/analytics/index', 1, 0, 0, '1', 1, 0, '2023-11-01 02:38:42', '2023-11-01 02:38:44', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1002', '1000', '工作台', NULL, '/dashboard/workbench', 'bx:bx-home', '/dashboard/workspace/index', 1, 0, 0, '1', 2, 0, '2023-11-01 02:45:46', '2023-11-01 02:40:10', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1753838064745844737', '2003', '修改菜单', 'menu:edit', NULL, NULL, NULL, 1, 1, 0, '2', 2, 0, '2024-02-04 01:49:16', '2024-02-04 01:49:16', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759756582691561473', '2001', '新增用户', 'user:create', '', '', '', 1, 1, 0, '2', 1, 0, '2024-02-20 09:47:19', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759796776249602050', '2001', '更新用户', 'user:update', '', '', '', 1, 1, 0, '2', 2, 0, '2024-02-20 12:27:02', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759803126056759298', '2001', '删除用户', 'user:delete', '', '', '', 1, 1, 0, '2', 3, 0, '2024-02-20 12:52:15', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759805834515984386', '2002', '添加角色', 'role:add', '', '', '', 1, 1, 0, '2', 1, 0, '2024-02-20 13:03:01', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759873891401732097', '2002', '更新角色', 'role:edit', '', '', '', 1, 1, 0, '2', 2, 0, '2024-02-20 17:33:27', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759874353278488578', '2002', '删除角色', 'role:delete', '', '', '', 1, 1, 0, '2', 3, 0, '2024-02-20 17:35:17', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759874756913139713', '2003', '删除菜单', 'menu:delete', '', '', '', 1, 1, 0, '2', 3, 0, '2024-02-20 17:36:54', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759875183046037505', '2004', '添加部门', 'dept:add', '', '', '', 1, 1, 0, '2', 1, 0, '2024-02-20 17:38:35', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759875298217431041', '2004', '更新部门', 'dept:update', '', '', '', 1, 1, 0, '2', 2, 0, '2024-02-20 17:39:03', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1759875383483437058', '2004', '删除部门', 'dept:delete', '', '', '', 1, 1, 0, '2', 3, 0, '2024-02-20 17:39:23', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1801290765519847426', '0', '类别管理', '', '/mall', 'ant-design:account-book-outlined', 'BasicLayout', 0, 1, 0, '0', 1, 1, '2024-06-14 00:29:20', NULL, '0', NULL, NULL, 1, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2000', '0', '系统管理', NULL, '/system', 'ant-design:account-book-outlined', 'BasicLayout', 1, 0, 0, '0', 2, 0, '2023-11-01 02:45:44', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('200032', '2000', '职位管理', 'SysPost:list', '/system/SysPost', 'bx:bx-home', '/system/SysPost/index', 1, 0, 0, '1', 1, 0, '2023-11-01 02:57:44', '2023-11-01 02:57:47', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2001', '2000', '账号管理', 'user:list', '/system/user', 'bx:bx-home', '/system/user/index', 1, 0, 0, '1', 1, 0, '2023-11-01 02:46:23', '2023-11-01 02:46:21', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2002', '2000', '角色管理', 'role:list', '/system/role', 'bx:bx-home', '/system/role/index', 1, 0, 0, '1', 1, 0, '2023-11-01 02:50:35', '2023-11-01 02:50:37', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2003', '2000', '菜单管理', 'menu:list', '/system/menu', 'ant-design:account-book-outlined', '/system/menu/index', 1, 0, 0, '1', 1, 0, '2023-11-01 02:51:25', '2023-11-01 02:51:23', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('20031', '2003', '添加菜单', 'menu:add', NULL, NULL, NULL, 1, 0, 0, '2', 1, 0, '2024-02-04 01:37:48', '2024-02-04 01:37:50', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2004', '2000', '部门管理', 'dept:list', '/system/dept', 'bx:bx-home', '/system/dept/index', 1, 0, 0, '1', 1, 0, '2023-11-01 02:57:44', '2023-11-01 02:57:47', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2005', '2000', '权限管理', NULL, '/system/permission', 'bx:bx-home', '/system/permission/index', 1, 0, 0, '1', 1, 0, '2024-06-17 00:14:17', '2024-06-17 00:14:18', '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('3000', '0', '内容管理', NULL, '/cms', 'ant-design:account-book-outlined', 'BasicLayout', 1, 0, 0, '0', 1, 0, '2024-02-04 02:49:59', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('30009', '3000', '编辑文章', 'cms:contentCategory', '/cms/article/ArticleModel', 'bx:bx-home', '/cms/article/ArticleModel', 0, 0, 0, '1', 1, 0, '2024-02-04 02:56:03', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('3001', '3000', '文章分类管理', 'cms:contentCategory', '/cms/contentCategory', 'bx:bx-home', '/cms/contentCategory/index', 1, 0, 0, '1', 1, 0, '2024-02-04 02:56:03', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('300100', '3000', '轮播图管理', 'cms:carousel', '/cms/carousel', 'bx:bx-home', '/cms/carousel/index', 1, 0, 0, '1', 1, 0, '2024-02-04 02:56:03', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('3003', '3000', '文章管理', 'cms:contentCategory', '/cms/article', 'bx:bx-home', '/cms/article/index', 1, 0, 0, '1', 1, 0, '2024-02-04 02:56:03', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('999', '0', '演示11', NULL, '/demos', 'ant-design:account-book-outlined', 'BasicLayout', 1, 0, 0, '0', 1, 0, '2024-02-04 02:49:59', NULL, '0', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `menu_name`, `permission`, `route_path`, `icon`, `component`, `is_show`, `keep_alive`, `is_ext`, `type`, `order_no`, `status`, `create_at`, `update_at`, `tenant_id`, `create_by`, `update_by`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('9991', '999', '表单演示', 'user:list', '/demos/form', 'bx:bx-home', '/demos/form/basic', 1, 0, 0, '1', 1, 0, '2023-11-01 02:46:23', '2023-11-01 02:46:21', '0', NULL, NULL, 0, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_menu_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_permission`;
CREATE TABLE `sys_menu_permission` (
  `menu_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单 id',
  `permission_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '权限id',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`menu_id`,`permission_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of sys_menu_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802384339682533377', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802700353909567489', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802700771033100290', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802705076406464514', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802706916997083137', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802713766639448065', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802714379326599169', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802722993034522625', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802740972954595329', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802743812469043201', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '1802747343643291649', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '20007', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '2001', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '2002', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '2003', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2001', '2006', '0');
INSERT INTO `sys_menu_permission` (`menu_id`, `permission_id`, `tenant_id`) VALUES ('2005', '2005', '0');
COMMIT;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `permission_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '权限ID',
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限名称',
  `code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限编码',
  `type` int DEFAULT NULL COMMENT '权限类型',
  `uri` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'uri',
  `pg_id` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限组id',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '排序',
  `description` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `status` int DEFAULT '0' COMMENT '状态',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='权限管理';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_permission_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission_group`;
CREATE TABLE `sys_permission_group` (
  `pg_id` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '权限组id',
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限名称',
  `description` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '排序',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`pg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='权限组管理';

-- ----------------------------
-- Records of sys_permission_group
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
  `post_code` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '岗位名称',
  `post_rank` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '职级',
  `remark` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `status` char(1) COLLATE utf8mb4_bin NOT NULL COMMENT '状态（0正常 1停用）',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '排序',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '租户ID',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户管理';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_post` (`post_id`, `post_code`, `post_name`, `post_rank`, `remark`, `status`, `order_no`, `delete_flag`, `create_by`, `create_at`, `update_by`, `update_at`, `tenant_id`) VALUES ('121', 'ss', '董事长', '1', '董事长', '1', 0, '0', 's', NULL, 's', NULL, '1');
INSERT INTO `sys_post` (`post_id`, `post_code`, `post_name`, `post_rank`, `remark`, `status`, `order_no`, `delete_flag`, `create_by`, `create_at`, `update_by`, `update_at`, `tenant_id`) VALUES ('1880679065709596673', 'ss', 'ss', 's', NULL, '0', 0, '0', NULL, '2025-01-19 02:10:05', NULL, '2025-01-19 02:10:05', '0');
INSERT INTO `sys_post` (`post_id`, `post_code`, `post_name`, `post_rank`, `remark`, `status`, `order_no`, `delete_flag`, `create_by`, `create_at`, `update_by`, `update_at`, `tenant_id`) VALUES ('1880679145778860034', 'ss', 'ss', 's', NULL, '0', 0, '1', NULL, '2025-01-19 02:10:24', NULL, NULL, '0');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '角色ID',
  `role_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色编码',
  `role_desc` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色描述',
  `order_no` int NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int DEFAULT '0' COMMENT '状态',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色管理';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`role_id`, `role_name`, `role_code`, `role_desc`, `order_no`, `status`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`) VALUES ('1836065462207672321', '测试', 'TEST1', 'cdxs', 0, 0, '2024-09-17 23:31:33', NULL, NULL, NULL, '0', '0');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '角色ID',
  `menu_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '1753838064745844737', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '1759756582691561473', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '2000', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '200032', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '2001', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '2002', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '2003', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '20031', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '2004', '0');
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`, `tenant_id`) VALUES ('1836065462207672321', '2005', '0');
COMMIT;

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant` (
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户id',
  `tenant_name` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户名称',
  `tenant_code` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户编码',
  `tenant_domain` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '租户域名',
  `tenant_contact` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户联系人',
  `tenant_contact_phone` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户联系电话',
  `tenant_contact_email` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户联系邮箱',
  `tenant_logo` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '租户logo',
  `tenant_type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '租户类型',
  `memo` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `valid_flag` varchar(3) COLLATE utf8mb4_bin NOT NULL DEFAULT '1' COMMENT '状态 0无效 1有效',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='租户管理';

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
BEGIN;
INSERT INTO `sys_tenant` (`tenant_id`, `tenant_name`, `tenant_code`, `tenant_domain`, `tenant_contact`, `tenant_contact_phone`, `tenant_contact_email`, `tenant_logo`, `tenant_type`, `memo`, `valid_flag`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`) VALUES ('1817622246370426882', 'asas', 'sa', 'assa', 'sa', 'as', 'as', 'sa', 'as', 'asas', '0', NULL, '2024-07-29 02:04:48', NULL, NULL, '0');
INSERT INTO `sys_tenant` (`tenant_id`, `tenant_name`, `tenant_code`, `tenant_domain`, `tenant_contact`, `tenant_contact_phone`, `tenant_contact_email`, `tenant_logo`, `tenant_type`, `memo`, `valid_flag`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`) VALUES ('1817648592681267201', 'ass', 'as', 'asas', 'as', '14345678901', 'as', 'as', 'sa', NULL, '1', NULL, '2024-07-29 03:49:29', NULL, NULL, '1');
COMMIT;

-- ----------------------------
-- Table structure for sys_tenant_applications
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_applications`;
CREATE TABLE `sys_tenant_applications` (
  `application_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '申请id',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '租户id',
  `identifying_code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '识别码',
  `application_date` date NOT NULL COMMENT '申请日期',
  `company_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '公司名称',
  `contact_person` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '联系人姓名',
  `contact_email` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '联系人邮箱',
  `contact_phone` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT '联系电话',
  `industry` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '行业',
  `website` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '公司网站',
  `service_interests` text COLLATE utf8mb4_bin COMMENT '感兴趣的服务',
  `user_estimate` int DEFAULT NULL COMMENT '用户估计数量',
  `data_storage_requirement` int DEFAULT NULL COMMENT '数据存储需求,单位GB',
  `sla_requirements` text COLLATE utf8mb4_bin COMMENT '服务级别协议要求',
  `security_requirements` text COLLATE utf8mb4_bin COMMENT '安全合规性要求',
  `special_notes` text COLLATE utf8mb4_bin COMMENT '特殊需求或说明',
  `status` varchar(32) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '申请状态 0 Pending待定, 1 Approved批准 -1 Rejected拒绝',
  `valid_flag` varchar(3) COLLATE utf8mb4_bin NOT NULL DEFAULT '1' COMMENT '状态 0无效 1有效',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`application_id`),
  UNIQUE KEY `contact_email` (`contact_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='租户申请表';

-- ----------------------------
-- Records of sys_tenant_applications
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户ID',
  `username` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `nickname` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '昵称',
  `sex` int DEFAULT NULL COMMENT '性别',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `password` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `avatar` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
  `email` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
  `status` int DEFAULT '0' COMMENT '状态',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户管理';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` (`user_id`, `username`, `nickname`, `sex`, `birthday`, `password`, `avatar`, `email`, `phone`, `status`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`) VALUES ('1123', 'admin', '管理员121', 1, '2024-09-19 00:00:00', '{bcrypt}$2a$10$0INtS9wxpbmvMrYSWNWqhOB90UppvT3YstBz5eZLzzFjfFlJcSEY2', NULL, '267@qq.comq', '13212341235', 0, NULL, NULL, NULL, NULL, '0', '0');
INSERT INTO `sys_user` (`user_id`, `username`, `nickname`, `sex`, `birthday`, `password`, `avatar`, `email`, `phone`, `status`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`, `tenant_id`) VALUES ('1843342291209658369', 'root232', '管理', 0, '2024-10-04 00:00:00', '{bcrypt}$2a$10$YxSsAtMPSBlDcoMt0JoQwubQporNpiVrzVF6iQ5WovoP34gMXCXa2', NULL, '267@qq.comsf', '13212341238', 0, '2024-10-08 01:27:05', NULL, NULL, NULL, '0', '0');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_dept`;
CREATE TABLE `sys_user_dept` (
  `user_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户ID',
  `dept_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '部门ID',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  `pinyin_code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '拼音码',
  PRIMARY KEY (`user_id`,`dept_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户部门关系';

-- ----------------------------
-- Records of sys_user_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_dept` (`user_id`, `dept_id`, `tenant_id`, `pinyin_code`) VALUES ('1123', '1835623422642716673', '0', NULL);
INSERT INTO `sys_user_dept` (`user_id`, `dept_id`, `tenant_id`, `pinyin_code`) VALUES ('1123', '2', '0', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_menu`;
CREATE TABLE `sys_user_menu` (
  `user_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户id',
  `menu_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`user_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户权限表';

-- ----------------------------
-- Records of sys_user_menu
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户ID',
  `role_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '角色ID',
  `tenant_id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`user_id`,`role_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色用户';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` (`user_id`, `role_id`, `tenant_id`) VALUES ('1123', '1836065462207672321', '0');
INSERT INTO `sys_user_role` (`user_id`, `role_id`, `tenant_id`) VALUES ('1843342291209658369', '1836065462207672321', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
