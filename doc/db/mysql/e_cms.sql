/*
 Navicat Premium Dump SQL

 Source Server         : docker-mysql
 Source Server Type    : MySQL
 Source Server Version : 90100 (9.1.0)
 Source Host           : localhost:3306
 Source Schema         : e_cms

 Target Server Type    : MySQL
 Target Server Version : 90100 (9.1.0)
 File Encoding         : 65001

 Date: 22/02/2025 17:54:49
*/
CREATE database if NOT EXISTS `e_cms` default character set utf8mb4 collate utf8mb4_general_ci;
use `e_cms`;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;


-- ----------------------------
-- Table structure for cms_article
-- ----------------------------
DROP TABLE IF EXISTS `cms_article`;
CREATE TABLE `cms_article` (
  `article_id` varchar(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章id',
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章标题',
  `content` text COLLATE utf8mb4_general_ci COMMENT '文章内容',
  `category_id` varchar(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类id',
  `cover_image` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '封面图片',
  `summary` varchar(356) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '摘要',
  `create_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除状态',
  `tenant_id` varchar(32) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '租户ID',
  `pinyin_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拼音码',
  `wubi_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '五笔码',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文章表';

-- ----------------------------
-- Records of cms_article
-- ----------------------------
BEGIN;
INSERT INTO `cms_article` (`article_id`, `title`, `content`, `category_id`, `cover_image`, `summary`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('1', '测试', '<p>去价格就会</p>', '12', 'https://ai-public.mastergo.com/ai/img_res/e5a6d23743ca29968a25d509959204ca.jpg', '摘要', NULL, '2025-01-27 14:22:00', NULL, '2025-01-27 14:22:00', '0', '0', NULL, NULL);
INSERT INTO `cms_article` (`article_id`, `title`, `content`, `category_id`, `cover_image`, `summary`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `tenant_id`, `pinyin_code`, `wubi_code`) VALUES ('2', '测试 1', '<p><span style=\"color: rgb(235, 144, 58);\"><u><strong>弟弟对对对</strong></u></span></p>', '12', 'https://ai-public.mastergo.com/ai/img_res/e5a6d23743ca29968a25d509959204ca.jpg', '摘要', NULL, '2025-01-27 14:23:00', NULL, '2025-01-27 14:23:00', '0', '0', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for cms_attachment
-- ----------------------------
DROP TABLE IF EXISTS `cms_attachment`;
CREATE TABLE `cms_attachment` (
  `attachment_id` varchar(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件id',
  `article_id` varchar(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章id',
  `file_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件名称',
  `attachment_url` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件url',
  `file_size` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件大小',
  `create_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除状态',
  `tenant_id` varchar(32) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '租户ID',
  `pinyin_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拼音码',
  `wubi_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '五笔码',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文章附件表';

-- ----------------------------
-- Records of cms_attachment
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cms_carousel
-- ----------------------------
DROP TABLE IF EXISTS `cms_carousel`;
CREATE TABLE `cms_carousel` (
  `carousel_id` varchar(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '轮播id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标题',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图片url',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '描述',
  `css` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'css',
  `jump_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '跳转的url',
  `sort_value` int DEFAULT '1' COMMENT '排序值',
  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '租户ID',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除状态',
  `pinyin_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拼音码',
  `wubi_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '五笔码',
  PRIMARY KEY (`carousel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='轮播图表';

-- ----------------------------
-- Records of cms_carousel
-- ----------------------------
BEGIN;
INSERT INTO `cms_carousel` (`carousel_id`, `title`, `image_url`, `description`, `css`, `jump_url`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1', '立德树人 培育英才', 'https://ai-public.mastergo.com/ai/img_res/d80eff8ce6339a45110d682a4803df8d.jpg', '坚持高质量发展，培养创新型人才', NULL, NULL, 1, '0', NULL, '2025-02-02 13:37:47', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_carousel` (`carousel_id`, `title`, `image_url`, `description`, `css`, `jump_url`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2', '科教融合 追求卓越', 'https://ai-public.mastergo.com/ai/img_res/4371b493e16f0b410a879cd2a4339875.jpg', '面向世界科技前沿，推动科技创新发展', NULL, NULL, 1, '0', NULL, '2025-02-02 13:37:49', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_carousel` (`carousel_id`, `title`, `image_url`, `description`, `css`, `jump_url`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('3', '开放合作 共创未来', 'https://ai-public.mastergo.com/ai/img_res/b51975f4ead3b4ac60542346e2b5ba42.jpg', '促进国内外交流，服务国家发展战略', NULL, NULL, 1, '0', NULL, '2025-02-02 13:37:51', NULL, NULL, '0', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for cms_content_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_content_category`;
CREATE TABLE `cms_content_category` (
  `category_id` varchar(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类id',
  `category_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类名称',
  `icon` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
  `description` text COLLATE utf8mb4_general_ci COMMENT '描述',
  `type` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型',
  `parent_id` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '父节点id',
  `sort_value` int DEFAULT '1' COMMENT '排序值',
  `tenant_id` varchar(32) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '租户ID',
  `create_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除状态',
  `pinyin_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拼音码',
  `wubi_code` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '五笔码',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='内容分类表';

-- ----------------------------
-- Records of cms_content_category
-- ----------------------------
BEGIN;
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('12', '学校机构', 'ant-design:account-book-outlined', '学校机构', '0', '0', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1201', '党群机构', 'ant-design:account-book-outlined', '党群机构', '0', '12', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1202', '行政机构', 'ant-design:account-book-outlined', 'ss', '1', '12', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1203', '学术机构', 'ant-design:account-book-outlined', NULL, '0', '12', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '1', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('1882073420882186242', '测试', NULL, NULL, '2', '12', 1, '0', NULL, '2025-01-22 22:30:45', NULL, NULL, '1', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2323', '教育教学', 'ant-design:account-book-outlined', NULL, '0', '0', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('23231', '师资队伍', 'ant-design:account-book-outlined', NULL, '1', '2323', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('23232', '人物风采', 'ant-design:account-book-outlined', NULL, '1', '2323', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('23233', '启真新论', 'ant-design:account-book-outlined', NULL, '1', '2323', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('233', '校情总览', 'ant-design:account-book-outlined', NULL, '0', '0', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2330', '学校概况', 'ant-design:account-book-outlined', NULL, '0', '233', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2331', '历史沿革\n', 'ant-design:account-book-outlined', NULL, '0', '233', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2332', '历任领导', 'ant-design:account-book-outlined', NULL, '0', '233', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2333', '学校章程', 'ant-design:account-book-outlined', NULL, '0', '233', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2334', '现任领导', 'ant-design:account-book-outlined', NULL, '2', '233', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2335', '学校标识', 'ant-design:account-book-outlined', NULL, '0', '233', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2336', '统计公报', 'ant-design:account-book-outlined', NULL, '0', '233', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('243', '校园生活', 'ant-design:account-book-outlined', NULL, '0', '0', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2431', '工会活动', 'ant-design:account-book-outlined', NULL, '0', '243', 0, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2432', '学生党建', 'ant-design:account-book-outlined', NULL, '0', '243', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2433', '学生会', 'ant-design:account-book-outlined', NULL, '0', '243', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2434', '校园文化', 'ant-design:account-book-outlined', NULL, '0', '243', 0, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2435', '体育活动', 'ant-design:account-book-outlined', NULL, '0', '243', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('2555', '校庆献辞', 'ant-design:account-book-outlined', NULL, '0', '233', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('343', '文化资源', 'ant-design:account-book-outlined', NULL, '0', '0', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('3431', '图书馆', 'ant-design:account-book-outlined', NULL, NULL, '343', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('3432', '转校招生', 'ant-design:account-book-outlined', NULL, '0', '0', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('34321', '报考指南', 'ant-design:account-book-outlined', NULL, '0', '3432', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('343211', '招生政策 ', 'ant-design:account-book-outlined', NULL, '0', '34321', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('343212', '常见问题 ', 'ant-design:account-book-outlined', NULL, '0', '343211', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('343213', '录取情况 ', 'ant-design:account-book-outlined', NULL, '0', '343212', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('34322', '录取查询', 'ant-design:account-book-outlined', NULL, '0', '3432', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('34323', '转校政策', 'ant-design:account-book-outlined', NULL, '0', '3432', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('3433', '教学讲坛 ', 'ant-design:account-book-outlined', NULL, NULL, '343', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('411', '走进学校', 'ant-design:account-book-outlined', NULL, '0', '0', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('4110', '进校预约', 'ant-design:account-book-outlined', NULL, '0', '411', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('4112', '周边交通', 'ant-design:account-book-outlined', NULL, '0', '411', 2, '', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('4113', '校园风光', 'ant-design:account-book-outlined', NULL, '0', '411', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('41130', '校园地图', 'ant-design:account-book-outlined', NULL, '0', '4113', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('41132', '校园景观', 'ant-design:account-book-outlined', NULL, '0', '4113', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('4432', '校园光影', 'ant-design:account-book-outlined', NULL, '0', '343', 0, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('64', '新闻动态', 'ant-design:account-book-outlined', NULL, '0', '0', 3, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('6401', '综合时讯\n', 'ant-design:account-book-outlined', NULL, '0', '64', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('6402', '人才培养', 'ant-design:account-book-outlined', NULL, '0', '64', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('6403', '人物风采\n', 'ant-design:account-book-outlined', NULL, '0', '64', 1, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', '2', NULL);
INSERT INTO `cms_content_category` (`category_id`, `category_name`, `icon`, `description`, `type`, `parent_id`, `sort_value`, `tenant_id`, `create_by`, `create_at`, `update_by`, `update_at`, `delete_flag`, `pinyin_code`, `wubi_code`) VALUES ('6404', '菁菁校园', 'ant-design:account-book-outlined', NULL, '0', '64', 2, '0', NULL, '2025-01-20 21:23:48', NULL, NULL, '0', NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
