CREATE database if NOT EXISTS `els_codegen` default character set utf8mb4 collate utf8mb4_general_ci;
use `els_codegen`;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_datasource_conf
-- ----------------------------
DROP TABLE IF EXISTS `gen_datasource_conf`;
CREATE TABLE `gen_datasource_conf` (
  `id` bigint NOT NULL COMMENT '主键',
  `name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '别名',
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jdbcurl',
  `username` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `ds_type` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据库类型',
  `conf_type` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '配置类型',
  `ds_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据库名称',
  `instance` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '实例',
  `port` int DEFAULT NULL COMMENT '端口',
  `host` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主机',
  `state` char(1) COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '状态 1 正常 0 删除',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据源表';

-- ----------------------------
-- Records of gen_datasource_conf
-- ----------------------------
BEGIN;
INSERT INTO `gen_datasource_conf` (`id`, `name`, `url`, `username`, `password`, `ds_type`, `conf_type`, `ds_name`, `instance`, `port`, `host`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (1821537418570964991, 'codeGen', 'jdbc:mysql://127.0.0.1:3306/elsfs-codegen?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai', 'root', 'GoSlZMDk/m7l3Off53YtlTY8u5730JQH8jzjqz78RVxrBUdnim3suVEPkLO10uR9', 'mysql', '1', 'elsfs-admin', NULL, NULL, NULL, '1', '2024-08-08 21:22:18', NULL, NULL, NULL, '0');
INSERT INTO `gen_datasource_conf` (`id`, `name`, `url`, `username`, `password`, `ds_type`, `conf_type`, `ds_name`, `instance`, `port`, `host`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (1821537418570964994, 'admin', 'jdbc:mysql://127.0.0.1:3306/elsfs-admin?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai', 'root', 'GoSlZMDk/m7l3Off53YtlTY8u5730JQH8jzjqz78RVxrBUdnim3suVEPkLO10uR9', 'mysql', '1', 'elsfs-admin', NULL, NULL, NULL, '1', '2024-08-08 21:22:18', NULL, NULL, NULL, '0');
COMMIT;

-- ----------------------------
-- Table structure for gen_field_type
-- ----------------------------
DROP TABLE IF EXISTS `gen_field_type`;
CREATE TABLE `gen_field_type` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `column_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字段类型',
  `attr_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '属性包名',
  `state` char(1) COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '状态 1 启用 0 禁用',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `column_type` (`column_type`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字段类型管理';

-- ----------------------------
-- Records of gen_field_type
-- ----------------------------
BEGIN;
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (1, 'datetime', 'ZonedDateTime', 'java.time.ZonedDateTime', '1', '2024-08-16 08:45:10', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (2, 'date', 'LocalDate', 'java.time.LocalDate', '1', '2024-08-16 08:45:10', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (3, 'tinyint', 'Integer', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (4, 'smallint', 'Integer', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (5, 'mediumint', 'Integer', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (6, 'int', 'Integer', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (7, 'integer', 'Integer', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (8, 'bigint', 'Long', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (9, 'float', 'Float', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (10, 'double', 'Double', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (11, 'decimal', 'BigDecimal', 'java.math.BigDecimal', '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (12, 'bit', 'Boolean', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (13, 'char', 'String', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (14, 'varchar', 'String', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (15, 'tinytext', 'String', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (16, 'text', 'String', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (17, 'mediumtext', 'String', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (18, 'longtext', 'String', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (19, 'timestamp', 'LocalDateTime', 'java.time.LocalDateTime', '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (20, 'NUMBER', 'Integer', NULL, '1', '2024-08-16 20:45:11', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (21, 'BINARY_INTEGER', 'Integer', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (22, 'BINARY_FLOAT', 'Float', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (23, 'BINARY_DOUBLE', 'Double', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (24, 'VARCHAR2', 'String', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (25, 'NVARCHAR', 'String', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (26, 'NVARCHAR2', 'String', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (27, 'CLOB', 'String', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (28, 'int8', 'Long', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (29, 'int4', 'Integer', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (30, 'int2', 'Integer', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (31, 'numeric', 'BigDecimal', 'java.math.BigDecimal', '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
INSERT INTO `gen_field_type` (`id`, `column_type`, `attr_type`, `package_name`, `state`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (32, 'json', 'String', NULL, '1', '2024-08-16 08:45:12', NULL, NULL, NULL, '0');
COMMIT;

-- ----------------------------
-- Table structure for gen_group
-- ----------------------------
DROP TABLE IF EXISTS `gen_group`;
CREATE TABLE `gen_group` (
  `id` bigint NOT NULL,
  `group_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分组名称',
  `group_desc` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分组描述',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='模板分组';

-- ----------------------------
-- Records of gen_group
-- ----------------------------
BEGIN;
INSERT INTO `gen_group` (`id`, `group_name`, `group_desc`, `create_at`, `create_by`, `update_at`, `update_by`, `delete_flag`) VALUES (1, '增删改查', '单表增删改查', '2024-08-14 13:04:41', NULL, NULL, NULL, '0');
COMMIT;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `table_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表名',
  `class_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类名',
  `db_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据库类型',
  `table_comment` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '说明',
  `author` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作者',
  `email` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `package_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '项目包名',
  `version` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '项目版本号',
  `i18n` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '是否生成带有i18n 0 不带有 1带有',
  `style` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '代码风格',
  `child_table_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子表名称',
  `main_field` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主表关联键',
  `child_field` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子表关联键',
  `generator_type` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '生成方式  0：zip压缩包   1：自定义目录',
  `backend_path` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '后端生成路径',
  `frontend_path` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '前端生成路径',
  `module_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '模块名',
  `function_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '功能名',
  `form_layout` tinyint DEFAULT NULL COMMENT '表单布局  1：一列   2：两列',
  `ds_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据源ID',
  `primary_key` bigint DEFAULT NULL COMMENT '表主键',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_name` (`table_name`,`ds_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='代码生成表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `ds_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据源名称',
  `table_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表名称',
  `field_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字段名称',
  `field_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字段类型',
  `field_comment` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字段说明',
  `attr_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '属性名',
  `attr_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '属性包名',
  `sort` int DEFAULT NULL COMMENT '排序',
  `auto_fill` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '自动填充  DEFAULT、INSERT、UPDATE、INSERT_UPDATE',
  `primary_pk` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '主键 0：否  1：是',
  `base_field` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '基类字段 0：否  1：是',
  `form_item` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '表单项 0：否  1：是',
  `form_required` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '表单必填 0：否  1：是',
  `form_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表单类型',
  `form_validator` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表单效验',
  `grid_item` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '列表项 0：否  1：是',
  `grid_sort` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '列表排序 0：否  1：是',
  `query_item` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '查询项 0：否  1：是',
  `query_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '查询方式',
  `query_form_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '查询表单类型',
  `field_dict` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典类型',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='代码生成表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for gen_template
-- ----------------------------
DROP TABLE IF EXISTS `gen_template`;
CREATE TABLE `gen_template` (
  `id` bigint NOT NULL COMMENT '主键',
  `template_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板名称',
  `generator_path` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板路径',
  `template_desc` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板描述',
  `template_code` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板代码',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `delete_flag` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除时间(软删除)0 未删除 1 已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='模板';

-- ----------------------------
-- Records of gen_template
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for gen_template_group
-- ----------------------------
DROP TABLE IF EXISTS `gen_template_group`;
CREATE TABLE `gen_template_group` (
  `group_id` bigint NOT NULL COMMENT '分组id',
  `template_id` bigint NOT NULL COMMENT '模板id',
  PRIMARY KEY (`group_id`,`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='模板分组关联表';

-- ----------------------------
-- Records of gen_template_group
-- ----------------------------
BEGIN;
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 1);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 2);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 3);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 4);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 5);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 6);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 7);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 8);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 9);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 10);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 11);
INSERT INTO `gen_template_group` (`group_id`, `template_id`) VALUES (1, 12);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
