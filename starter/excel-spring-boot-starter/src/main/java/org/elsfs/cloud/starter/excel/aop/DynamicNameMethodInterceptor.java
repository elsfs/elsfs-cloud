package org.elsfs.cloud.starter.excel.aop;

import lombok.AllArgsConstructor;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.elsfs.cloud.starter.excel.annotation.ResponseExcel;
import org.elsfs.cloud.starter.excel.processor.NameProcessor;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 动态名称方法拦截器
 *
 * @author zeng
 */
@AllArgsConstructor
public class DynamicNameMethodInterceptor implements MethodInterceptor {
  public static final String EXCEL_NAME_KEY = "__EXCEL_NAME_KEY__";

  private final NameProcessor processor;

  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    Object returnVar = invocation.proceed();
    ResponseExcel excel = getAnnotation(invocation);
    if (excel == null) {
      return returnVar;
    }
    String name = excel.name();
    // 当配置的 excel 名称为空时，取当前时间
    if (!StringUtils.hasText(name)) {
      name = LocalDateTime.now().toString();
    } else {
      name = processor.doDetermineName(invocation.getArguments(), invocation.getMethod(), excel.name());
    }

    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    Objects.requireNonNull(requestAttributes).setAttribute(EXCEL_NAME_KEY, name, RequestAttributes.SCOPE_REQUEST);
    return returnVar;
  }

  private ResponseExcel getAnnotation(MethodInvocation invocation) {
    ResponseExcel annotation = invocation.getMethod().getAnnotation(ResponseExcel.class);
    if (annotation == null) {
      annotation = Objects.requireNonNull(invocation.getThis()).getClass().getAnnotation(ResponseExcel.class);
    }
    return annotation;
  }
}
