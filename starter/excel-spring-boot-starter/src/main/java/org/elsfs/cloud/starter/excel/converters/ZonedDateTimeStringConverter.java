/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.starter.excel.converters;


import cn.idev.excel.converters.Converter;
import cn.idev.excel.enums.CellDataTypeEnum;
import cn.idev.excel.metadata.GlobalConfiguration;
import cn.idev.excel.metadata.data.ReadCellData;
import cn.idev.excel.metadata.data.WriteCellData;
import cn.idev.excel.metadata.property.ExcelContentProperty;
import cn.idev.excel.util.DateUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * {@link LocalDateTime} 转换
 *
 * @author zeng
 */
public enum ZonedDateTimeStringConverter implements Converter<ZonedDateTime> {

  INSTANCE;
  private static final String MINUS = "-";

  @Override
  public Class<?> supportJavaTypeKey() {
    return ZonedDateTime.class;
  }

  @Override
  public CellDataTypeEnum supportExcelTypeKey() {
    return CellDataTypeEnum.STRING;
  }

  @Override
  public ZonedDateTime convertToJavaData(
      ReadCellData cellData,
      ExcelContentProperty contentProperty,
      GlobalConfiguration globalConfiguration) {
    String stringValue = cellData.getStringValue();
    String pattern =
        contentProperty == null || contentProperty.getDateTimeFormatProperty() == null
            ? switchDateFormat(stringValue)
            : contentProperty.getDateTimeFormatProperty().getFormat();
    DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern(pattern).withZone(ZoneId.systemDefault());
    return ZonedDateTime.parse(cellData.getStringValue(), formatter);
  }

  @Override
  public WriteCellData<String> convertToExcelData(
      ZonedDateTime value,
      ExcelContentProperty contentProperty,
      GlobalConfiguration globalConfiguration) {
    String pattern =
        contentProperty == null || contentProperty.getDateTimeFormatProperty() == null
            ? DateUtils.DATE_FORMAT_19
            : contentProperty.getDateTimeFormatProperty().getFormat();

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
    return new WriteCellData<>(value.format(formatter));
  }

  /**
   * switch date format
   *
   * @param dateString dateString
   * @return pattern
   */
  private static String switchDateFormat(String dateString) {
    int length = dateString.length();
    return switch (length) {
      case 19 ->
          dateString.contains(MINUS)
              ? DateUtils.DATE_FORMAT_19
              : DateUtils.DATE_FORMAT_19_FORWARD_SLASH;
      case 17 -> DateUtils.DATE_FORMAT_17;
      case 14 -> DateUtils.DATE_FORMAT_14;
      case 10 -> DateUtils.DATE_FORMAT_10;
      default -> throw new IllegalArgumentException("can not find date format for：" + dateString);
    };
  }
}
