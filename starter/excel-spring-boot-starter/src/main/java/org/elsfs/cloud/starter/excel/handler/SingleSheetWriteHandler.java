package org.elsfs.cloud.starter.excel.handler;


import cn.idev.excel.ExcelWriter;
import cn.idev.excel.FastExcel;
import cn.idev.excel.converters.Converter;
import cn.idev.excel.write.metadata.WriteSheet;
import jakarta.servlet.http.HttpServletResponse;
import org.elsfs.cloud.starter.excel.annotation.ResponseExcel;
import org.elsfs.cloud.starter.excel.configuration.ExcelConfigProperties;
import org.elsfs.cloud.starter.excel.enhance.WriterBuilderEnhancer;
import org.elsfs.cloud.starter.excel.kit.ExcelException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 处理单sheet 页面
 *
 * @author zeng
 */
public class SingleSheetWriteHandler extends AbstractSheetWriteHandler {

  public SingleSheetWriteHandler(ExcelConfigProperties configProperties,
                                 List<Converter<?>> converterProvider, WriterBuilderEnhancer excelWriterBuilderEnhance) {
    super(configProperties, converterProvider, excelWriterBuilderEnhance);
  }

  /**
   * obj 是List 且list不为空同时list中的元素不是是List 才返回true
   * @param obj 返回对象
   * @return boolean
   */
  @Override
  public boolean support(Object obj) {
    if (obj instanceof List) {
      List<?> objList = (List<?>) obj;
      return !objList.isEmpty() && !(objList.get(0) instanceof List);
    }
    else {
      throw new ExcelException("@ResponseExcel 返回值必须为List类型");
    }
  }

  @Override
  public void write(Object obj, HttpServletResponse response, ResponseExcel responseExcel) {
    List<?> eleList = (List<?>) obj;
    ExcelWriter excelWriter = getExcelWriter(response, responseExcel);

    WriteSheet sheet;
    if (CollectionUtils.isEmpty(eleList)) {
      sheet = FastExcel.writerSheet(responseExcel.sheets()[0].sheetName()).build();
    }
    else {
      // 有模板则不指定sheet名
      Class<?> dataClass = eleList.get(0).getClass();
      sheet = this.sheet(responseExcel.sheets()[0], dataClass, responseExcel.template(),
        responseExcel.headGenerator());
    }

    // 填充 sheet
    if (responseExcel.fill()) {
      excelWriter.fill(eleList, sheet);
    }
    else {
      // 写入sheet
      excelWriter.write(eleList, sheet);
    }
    excelWriter.finish();
  }

}
