package org.elsfs.cloud.starter.excel.kit;

public class ExcelException extends RuntimeException {

  public ExcelException(String message) {
    super(message);
  }

}
