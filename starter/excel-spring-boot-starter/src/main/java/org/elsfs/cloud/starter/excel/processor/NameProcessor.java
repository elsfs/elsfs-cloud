package org.elsfs.cloud.starter.excel.processor;

import java.lang.reflect.Method;

/**
 * 名称处理器
 */
public interface NameProcessor {

  /**
   * 解析名称
   * @param args 拦截器对象
   * @param method 方法
   * @param key 表达式
   * @return
   */
  String doDetermineName(Object[] args, Method method, String key);

}
