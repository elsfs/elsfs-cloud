package org.elsfs.cloud.starter.excel.handler;


import cn.idev.excel.context.AnalysisContext;
import cn.idev.excel.read.listener.ReadListener;
import org.elsfs.cloud.starter.excel.converters.DictTypeConvert;

/**
 * dict cache clear analysis 事件监听器
 *
 * @author zeng
 */
public class DictCacheClearAnalysisEventListener implements ReadListener<Object> {

  @Override
  public void invoke(Object o, AnalysisContext analysisContext) {
  }

  @Override
  public void doAfterAllAnalysed(AnalysisContext analysisContext) {
    DictTypeConvert.cache.clear();
  }

}
