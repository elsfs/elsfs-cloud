/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.starter.excel.converters;


import cn.idev.excel.converters.Converter;
import cn.idev.excel.enums.CellDataTypeEnum;
import cn.idev.excel.metadata.GlobalConfiguration;
import cn.idev.excel.metadata.data.ReadCellData;
import cn.idev.excel.metadata.data.WriteCellData;
import cn.idev.excel.metadata.property.ExcelContentProperty;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 转换器
 *
 * @author zeng
 */
public enum InstantConverter implements Converter<Instant> {
  /**
   * 实例
   */
  INSTANCE;
  @Override
  public Class<Instant> supportJavaTypeKey() {
    return Instant.class;
  }

  /**
   * excel中日期指定为文本格式，因此指定为DATE，如果为其他格式则需要指定为其他格式
   *
   * @return CellDataTypeEnum
   */
  @Override
  public CellDataTypeEnum supportExcelTypeKey() {
    return CellDataTypeEnum.DATE;
  }

  @Override
  public Instant convertToJavaData(
      ReadCellData<?> cellData,
      ExcelContentProperty contentProperty,
      GlobalConfiguration globalConfiguration)
      throws Exception {
    // 将字符串转换为Instant
    Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cellData.getStringValue());
    return date.toInstant();
  }

  @Override
  public WriteCellData<String> convertToExcelData(
      Instant value,
      ExcelContentProperty contentProperty,
      GlobalConfiguration globalConfiguration) {
    // 将Instant转换为字符串
    if (value == null) {
      return new WriteCellData<>();
    }
    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    String out =
        dateFormat.format(ZonedDateTime.ofInstant(value, ZoneId.systemDefault()).toLocalDateTime());
    return new WriteCellData<>(out);
  }
}
