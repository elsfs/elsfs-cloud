package org.elsfs.cloud.starter.excel.handler;

import cn.idev.excel.write.handler.WorkbookWriteHandler;
import cn.idev.excel.write.handler.context.WorkbookWriteHandlerContext;
import org.elsfs.cloud.starter.excel.converters.DictTypeConvert;

/**
 * dict 缓存清空
 * @author zeng
 */
public class DictCacheClearSheetWriteHandler implements WorkbookWriteHandler {

  /**
   * Called after all operations on the workbook have been completed
   * @param context
   */
  @Override
  public void afterWorkbookDispose(WorkbookWriteHandlerContext context) {
    DictTypeConvert.cache.clear();
  }

}
