package org.elsfs.cloud.starter.excel.handler;

import org.elsfs.cloud.starter.excel.vo.DictEnum;

/**
 * dict 数据提供程序
 *
 * @author zeng
 */
public interface DictDataProvider {

  /**
   * 获取 dict
   * @param type 类型
   * @return {@link DictEnum[] }
   */
  DictEnum[] getDict(String type);

}
