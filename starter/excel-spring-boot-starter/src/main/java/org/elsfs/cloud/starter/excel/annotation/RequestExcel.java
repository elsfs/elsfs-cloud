package org.elsfs.cloud.starter.excel.annotation;


import org.elsfs.cloud.starter.excel.handler.DefaultAnalysisEventListener;
import org.elsfs.cloud.starter.excel.handler.ListAnalysisEventListener;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 导入excel
 *
 */
@Documented
@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestExcel {

  /**
   * 前端上传字段名称 file
   */
  String fileName() default "file";

  /**
   * 读取的监听器类
   * @return readListener
   */
  Class<? extends ListAnalysisEventListener<?>> readListener() default DefaultAnalysisEventListener.class;

  /**
   * 是否跳过空行
   * @return 默认跳过
   */
  boolean ignoreEmptyRow() default false;

  /**
   * 读取的标题行数
   * @return
   */
  int headRowNumber() default 1;

}
