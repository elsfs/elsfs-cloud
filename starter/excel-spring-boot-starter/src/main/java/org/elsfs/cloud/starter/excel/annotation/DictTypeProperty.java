package org.elsfs.cloud.starter.excel.annotation;

import org.elsfs.cloud.starter.excel.vo.DictEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DictTypeProperty {

  /**
   * 字典类型字段
   * @return {@link String }
   */
  String value() default "";

  /**
   * [有限]直接从系统字典枚举解析，不走缓存
   * @return 与当前字典有关的系统字典枚举列表
   */
  Class<? extends DictEnum>[] enums() default {};

}
