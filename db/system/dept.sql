create table sys_dept
(
  dept_id      varchar(32)             not null comment '部门ID'
    primary key,
  parent_id    varchar(32)             null comment '父级部门ID',
  dept_name    varchar(50)             null comment '部门名称',
  org_code     varchar(32)             null comment '部门代码',
  org_category int                     null comment '部门类型',
  mobile       varchar(128)            null comment '部门电话',
  address      varchar(256)            null comment '部门地址',
  order_no     int         default 0   not null comment '排序',
  status       int         default 0   null comment '状态',
  dept_desc    varchar(256)            null comment '备注',
  `create_at` datetime     default null comment '创建时间',
  `create_by` varchar(64)  default null comment '创建人',
  `update_at` datetime     default null comment '修改时间',
  `update_by` varchar(64)  default null comment '修改人',
  delete_flag char(1)      default '0' not null comment '删除时间(软删除)0 未删除 1 已删除',
  tenant_id    varchar(32) default '0' not null comment '租户id'
)
  comment '部门管理';

BEGIN;
insert into sys_dept(dept_id, parent_id, dept_name, org_code, org_category, mobile, address, order_no, status, dept_desc, create_at, create_by, update_at, update_by, delete_flag, tenant_id)
values ('1', '0', '财务部', '000000', 1, '12345678901', '中国', 0, 0, '顶级部门', '2020-06-05 11:06:06', 'admin', '2020-06-05 11:06:06', 'admin', '0', '0');
insert into sys_dept(dept_id, parent_id, dept_name, org_code, org_category, mobile, address, order_no, status, dept_desc, create_at, create_by, update_at, update_by, delete_flag, tenant_id)
values ('2', '0', '人力资源部', '000000', 1, '12345678901', '中国', 0, 0, 'hr', '2020-06-05 11:06:06', 'admin', '2020-06-05 11:06:06', 'admin', '0', '0');
insert into sys_dept(dept_id, parent_id, dept_name, org_code, org_category, mobile, address, order_no, status, dept_desc, create_at, create_by, update_at, update_by, delete_flag, tenant_id)
values ('2000', '2', '会计部门', '000000', 1, '12345678901', '中国', 0, 0, 'hr', '2020-06-05 11:06:06', 'admin', '2020-06-05 11:06:06', 'admin', '0', '0');
insert into sys_dept(dept_id, parent_id, dept_name, org_code, org_category, mobile, address, order_no, status, dept_desc, create_at, create_by, update_at, update_by, delete_flag, tenant_id)
values ('2001', '2', '出纳部门', '000000', 1, '12345678901', '中国', 0, 0, 'hr', '2020-06-05 11:06:06', 'admin', '2020-06-05 11:06:06', 'admin', '0', '0');
COMMIT;
