create table sys_permission
(
  permission_id varchar(32)             not null comment '权限ID'
    primary key,
  name          varchar(50)             null comment '权限名称',
  code          varchar(32)             null comment '权限编码',
  type          int                     null comment '权限类型',
  uri           varchar(128)            null comment 'uri',
  pg_id         varchar(256)            null comment '权限组id',
  order_no      int         default 0   not null comment '排序',
  `description` varchar(256)            null comment '备注',
  status        int         default 0   null comment '状态',
  `create_at`   datetime    default null comment '创建时间',
  `create_by`   varchar(64) default null comment '创建人',
  `update_at`   datetime    default null comment '修改时间',
  `update_by`   varchar(64) default null comment '修改人',
  delete_flag   char(1)     default '0' not null comment '删除时间(软删除)0 未删除 1 已删除',
  tenant_id     varchar(32) default '0' not null comment '租户id'
)
  comment '权限管理';

create table sys_permission_group
(
  pg_id         varchar(256)            null comment '权限组id'
    primary key,
  name          varchar(50)             null comment '权限名称',
  `description` varchar(256)            null comment '备注',
  order_no      int         default 0   not null comment '排序',
  `create_at`   datetime    default null comment '创建时间',
  `create_by`   varchar(64) default null comment '创建人',
  `update_at`   datetime    default null comment '修改时间',
  `update_by`   varchar(64) default null comment '修改人',
  delete_flag   char(1)     default '0' not null comment '删除时间(软删除)0 未删除 1 已删除',
  tenant_id     varchar(32) default '0' not null comment '租户id'
)
  comment '权限组管理';
