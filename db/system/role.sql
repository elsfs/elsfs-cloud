create table sys_role
(
  role_id     varchar(32)             not null comment '角色ID' primary key,
  role_name   varchar(50)             null comment '角色名称',
  role_code   varchar(128)            null comment '角色编码',
  role_desc   varchar(256)            null comment '角色描述',
  order_no    int         default 0   not null comment '排序',
  status      int         default 0   null comment '状态',
  `create_at` datetime    default null comment '创建时间',
  `create_by` varchar(64) default null comment '创建人',
  `update_at` datetime    default null comment '修改时间',
  `update_by` varchar(64) default null comment '修改人',
  delete_flag char(1)     default '0' not null comment '删除时间(软删除)0 未删除 1 已删除',
  tenant_id   varchar(32) default '0' not null comment '租户id'
)
  comment '角色管理';

