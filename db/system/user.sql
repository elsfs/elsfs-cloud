
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
  `user_id`   varchar(32) NOT NULL COMMENT '用户ID',
  `username`  varchar(32) NOT NULL COMMENT '用户名',
  `nickname`  varchar(32) NOT NULL COMMENT '昵称',
  `sex`       integer(1)    DEFAULT NULL COMMENT '性别',
  `birthday`  datetime      DEFAULT NULL COMMENT '生日',
  `password`  varchar(256)  DEFAULT NULL COMMENT '密码',
  `avatar`    varchar(1024) DEFAULT NULL COMMENT '头像',
  `email`     varchar(256)  DEFAULT NULL COMMENT '邮箱',
  `phone`     varchar(11)   DEFAULT NULL COMMENT '手机号',
  `status`    int           DEFAULT 0 COMMENT '状态',
  `create_at` datetime    default null comment '创建时间',
  `create_by` varchar(64) default null comment '创建人',
  `update_at` datetime    default null comment '修改时间',
  `update_by` varchar(64) default null comment '修改人',
  delete_flag char(1)     default '0' not null comment '删除时间(软删除)0 未删除 1 已删除',
  tenant_id   varchar(32) default '0' not null comment '租户id',
    PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT ='用户管理';
