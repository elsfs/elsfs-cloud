
create table sys_tenant
(
  tenant_id            varchar(32)            not null comment '租户id'
    primary key,
  tenant_name          varchar(32)            not null comment '租户名称',
  tenant_code          varchar(32)            not null comment '租户编码',
  tenant_domain        varchar(256)           not null comment '租户域名',
  tenant_contact       varchar(32)            not null comment '租户联系人',
  tenant_contact_phone varchar(32)            not null comment '租户联系电话',
  tenant_contact_email varchar(32)            not null comment '租户联系邮箱',
  tenant_logo          varchar(256)           not null comment '租户logo',
  tenant_type          varchar(32)            not null comment '租户类型',
  memo                 varchar(256)           null comment '备注',
  valid_flag           varchar(3) default '1' not null comment '状态 0无效 1有效',
  create_by            varchar(32)            null comment '创建人',
  create_at            datetime               null comment '创建时间',
  update_by            varchar(32)            null comment '修改人',
  update_at            datetime               null comment '修改时间',
  delete_flag          char(1)    default '0' not null comment '删除时间(软删除)0 未删除 1 已删除'
)
  comment '租户信息';
CREATE TABLE sys_tenant_applications
(
  application_id           varchar(32)             not null comment '申请id' primary key,
  tenant_id                varchar(32)             null comment '租户id',
  identifying_code         varchar(32)             null comment '识别码',
  application_date         DATE                    NOT NULL comment '申请日期',
  company_name             VARCHAR(255)            NOT NULL comment '公司名称',
  contact_person           VARCHAR(255)            NOT NULL comment '联系人姓名',
  contact_email            VARCHAR(255) UNIQUE     NOT NULL comment '联系人邮箱',
  contact_phone            VARCHAR(20)             NOT NULL comment '联系电话',
  industry                 VARCHAR(255) comment '行业',
  website                  VARCHAR(255) comment '公司网站',
  service_interests        TEXT comment '感兴趣的服务', -- 感兴趣的服务列表
  user_estimate            INT comment '用户估计数量',
  data_storage_requirement INT comment '数据存储需求,单位GB',
  sla_requirements         TEXT comment '服务级别协议要求',
  security_requirements    TEXT comment '安全合规性要求',
  special_notes            TEXT comment '特殊需求或说明',
  status                   varchar(32) default '0' comment '申请状态 0 Pending待定, 1 Approved批准 -1 Rejected拒绝',
  valid_flag               varchar(3)  default '1' not null comment '状态 0无效 1有效',
  create_by                varchar(32)             null comment '创建人',
  create_at                datetime                null comment '创建时间',
  update_by                varchar(32)             null comment '修改人',
  update_at                datetime                null comment '修改时间',
  delete_flag              char(1)     default '0' not null comment '删除时间(软删除)0 未删除 1 已删除'
)
  comment '租户申请表';

CREATE TABLE sys_tenant_charges
(
  tenant_charges_id varchar(32)             not null comment '租户费用id' primary key,
  tenant_id         varchar(32)             NOT NULL comment '租户id ',
  charge_type       varchar(32)             NOT NULL default '0' comment '费用类型 0 Usage使用 1 Subscription 订阅 2 Penalty罚款',
  charge_amount     DECIMAL(10, 2)          NOT NULL comment '费用金额',
  charge_time       datetime                NOT NULL comment '费用发生时间',
  billing_period    varchar(32) default '0' NOT NULL comment '计费周期 -1 permanent 0 Yearly 年度 1 Quarterly 季度 2 Monthly 月度',
  payment_method    varchar(32) default '0' NOT NULL comment '支付方式 1 BankTransfer银行转账',
  payment_status    varchar(32) default '0' NOT NULL comment '支付状态 0 Pending待支付 1 Paid已支付 2 Overdue逾期',
  invoice_number    VARCHAR(255) comment '发票编号',
  memo              varchar(256)            null comment '备注',
  create_by         varchar(32)             null comment '创建人',
  create_at         datetime                null comment '创建时间',
  update_by         varchar(32)             null comment '修改人',
  update_at         datetime                null comment '修改时间',
  delete_flag       char(1)     default '0' not null comment '删除时间(软删除)0 未删除 1 已删除'
)
  comment '租户费用表';
