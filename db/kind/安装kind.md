# 命令安装
## 在 Linux 上：

```shell
# For AMD64 / x86_64
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.25.0/kind-linux-amd64
# For ARM64
[ $(uname -m) = aarch64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.25.0/kind-linux-arm64
chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind
```
## 在 macOS 上：

```shell
# For Intel Macs
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.25.0/kind-darwin-amd64
# For M1 / ARM Macs
[ $(uname -m) = arm64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.25.0/kind-darwin-arm64
chmod +x ./kind
mv ./kind /some-dir-in-your-PATH/kind
```
## 在 Windows 上，使用[PowerShell](https://zh.wikipedia.org/wiki/PowerShell)：
```shell
curl.exe -Lo kind-windows-amd64.exe https://kind.sigs.k8s.io/dl/v0.25.0/kind-windows-amd64
Move-Item .\kind-windows-amd64.exe c:\some-dir-in-your-PATH\kind.exe
```
# 包管理器安装
 在 macOS 上通过 Homebrew：
```shell
brew install kind
```
在 macOS 上通过 MacPorts：
```shell
sudo port selfupdate && sudo port install kind
```
在 Windows 上通过 Chocolatey（https://chocolatey.org/packages/kind）
```shell
choco install kind
```
在 Windows 上通过 Scoop（https://scoop.sh/#/apps?q=kind&id=faec311bb7c6b4a174169c8c02358c74a78a10c2）
```shell
scoop bucket add main
scoop install main/kind
```
在 Windows 上通过 Winget（https://github.com/microsoft/winget-pkgs/tree/master/manifests/k/Kubernetes/kind）
```shell
winget install Kubernetes.kind
```





