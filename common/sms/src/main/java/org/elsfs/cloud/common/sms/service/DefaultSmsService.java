/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.sms.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.sms.entity.SendSmsResponse;

/**
 * 默认的短信发送
 *
 * @author zeng
 */
@Slf4j
public class DefaultSmsService implements ISmsService {
  private static SendSmsResponse sendSmsResponseOk() {
    SendSmsResponse response = new SendSmsResponse();
    response.setHeaders(Map.of());
    SendSmsResponse.SendSmsResponseBody body = new SendSmsResponse.SendSmsResponseBody();
    body.setCode("OK");
    body.setMessage("OK");
    body.setRequestId(UUID.randomUUID().toString());
    response.setBody(body);
    return response;
  }

  @Override
  public SendSmsResponse sendSms(
      String phone, String templateCode, Map<String, Object> templateParam) {
    StringBuffer stringBuffer = new StringBuffer("短信发送成功:");
    templateParam.forEach(
        (key, value) -> stringBuffer.append(key).append("=").append(value).append("   "));
    LOGGER.info(stringBuffer.toString());

    return sendSmsResponseOk();
  }

  @Override
  public SendSmsResponse sendBatchSms(
      List<String> phoneNumbers, String templateCode, List<Map<String, Object>> templateParam) {
    StringBuffer stringBuffer = new StringBuffer("短信发送成功:");
    templateParam.forEach(
        stringObjectMap ->
            stringObjectMap.forEach(
                (key, value) -> stringBuffer.append(key).append("=").append(value).append("   ")));
    LOGGER.info(stringBuffer.toString());
    return sendSmsResponseOk();
  }
}
