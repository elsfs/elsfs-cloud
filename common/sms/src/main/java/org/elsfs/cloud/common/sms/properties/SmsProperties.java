/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.sms.properties;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 短信发送 properties
 *
 * @author zeng
 */
@Data
@ConfigurationProperties(prefix = "elsfs.sms")
public class SmsProperties {
  /**
   * key <a href="https://dysms.console.aliyun.com/domestic/text/template">模板Code</a>
   *
   * <p>values 阿里云配置
   */
  private Map<String, Aliyun> aliyuns = new HashMap<>();

  private Map<String, Tencent> tencents = new HashMap<>();
  private Type type = Type.DEFAULT;

  /** type */
  public enum Type {
    /** 阿里云 */
    ALIYUN,
    /** 默认 */
    DEFAULT,
    /** 腾讯云 */
    TENCENT
  }

  /** Tencent */
  @Slf4j
  @Data
  public static class Tencent {
    String secretId;
    String appId;
    String secretKey;
    String templateId;
    private String signName;
  }

  /** Aliyun */
  @Data
  public static class Aliyun {
    /** <a href="https://ram.console.aliyun.com/manage/ak">阿里云AK，SK</a> */
    private String accessKeyId;

    /** <a href="https://ram.console.aliyun.com/manage/ak">阿里云AK，SK</a> */
    private String accessKeySecret;

    /** <a href="https://dysms.console.aliyun.com/domestic/text/sign">阿里云签名名</a> */
    private String signName;
  }
}
