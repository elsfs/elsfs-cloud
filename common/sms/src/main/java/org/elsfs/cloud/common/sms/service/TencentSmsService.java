/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.sms.service;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendStatus;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.sms.entity.SendSmsResponse;
import org.elsfs.cloud.common.sms.properties.SmsProperties;

/**
 * 腾讯短信发送
 *
 * @author zeng
 */
@RequiredArgsConstructor
public class TencentSmsService implements ISmsService {
  private final SmsProperties smsProperties;

  @Override
  public SendSmsResponse sendSms(
      String phone, String templateCode, Map<String, Object> templateParam) {

    SmsProperties.Tencent tencent = smsProperties.getTencents().get(templateCode);
    if (tencent == null) {
      throw new RuntimeException("没有配置");
    }
    Credential cred = new Credential(tencent.getSecretId(), tencent.getSecretKey());
    ClientProfile clientProfile = new ClientProfile();
    /* SDK默认用TC3-HMAC-SHA256进行签名
     * 非必要请不要修改这个字段 */
    clientProfile.setSignMethod("HmacSHA256");
    /* 实例化要请求产品(以sms为例)的client对象
     * 第二个参数是地域信息，可以直接填写字符串ap-guangzhou，支持的地域列表参考 https://cloud.tencent.com/document/api/382/52071#.E5.9C.B0.E5.9F.9F.E5.88.97.E8.A1.A8 */
    SmsClient client = new SmsClient(cred, "ap-guangzhou", clientProfile);
    /* 实例化一个请求对象，根据调用的接口和实际情况，可以进一步设置请求参数
     * 您可以直接查询SDK源码确定接口有哪些属性可以设置
     * 属性可能是基本类型，也可能引用了另一个数据结构
     * 推荐使用IDE进行开发，可以方便的跳转查阅各个接口和数据结构的文档说明 */
    SendSmsRequest req = new SendSmsRequest();
    req.setSmsSdkAppId(tencent.getAppId());
    req.setSignName(tencent.getSignName());
    req.setTemplateId(tencent.getTemplateId());
    /* 模板参数: 模板参数的个数需要与 TemplateId 对应模板的变量个数保持一致，若无模板参数，则设置为空 */
    List<String> list = templateParam.values().stream().map(Object::toString).toList();
    req.setTemplateParamSet(list.toArray(new String[0]));
    /* 下发手机号码，采用 E.164 标准，+[国家或地区码][手机号]
     * 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号 */
    req.setPhoneNumberSet(new String[] {phone});
    try {
      com.tencentcloudapi.sms.v20210111.models.SendSmsResponse res = client.SendSms(req);
      return sendSmsResponse(res);
    } catch (TencentCloudSDKException e) {
      throw new RuntimeException(e);
    }
  }

  private static SendSmsResponse sendSmsResponse(
      com.tencentcloudapi.sms.v20210111.models.SendSmsResponse res) {
    SendSmsResponse smsResponse = new SendSmsResponse();
    smsResponse.setHeaders(res.GetHeader());
    SendSmsResponse.SendSmsResponseBody body = new SendSmsResponse.SendSmsResponseBody();
    SendStatus[] statuses = res.getSendStatusSet();
    if (statuses != null && statuses.length > 0) {
      SendStatus status = statuses[0];
      body.setCode(status.getCode());
      body.setMessage(status.getMessage());
      body.setRequestId(res.getRequestId());
    }
    smsResponse.setBody(body);
    return smsResponse;
  }

  @Override
  public SendSmsResponse sendBatchSms(
      List<String> phoneNumbers, String templateCode, List<Map<String, Object>> templateParam) {
    // TODO
    return null;
  }
}
