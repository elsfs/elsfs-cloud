/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.sms.service;

import java.util.List;
import java.util.Map;
import org.elsfs.cloud.common.sms.entity.SendSmsResponse;

/**
 * 短信发送配置
 *
 * @author zeng
 */
public interface ISmsService {

  /**
   * 发送前请申请短信签名和短信模板，并确保签名和模板已审核通过 发送短信
   *
   * @param phone 手机号
   * @param templateCode 模板code
   * @param templateParam 发送短信的参数
   * @return s
   */
  SendSmsResponse sendSms(String phone, String templateCode, Map<String, Object> templateParam);

  /**
   * 批量发送短信
   *
   * @param phoneNumbers 发送的手机号列表
   * @param templateCode 模板code
   * @param templateParam 发送参数
   * @return s
   */
  SendSmsResponse sendBatchSms(
      List<String> phoneNumbers, String templateCode, List<Map<String, Object>> templateParam);
}
