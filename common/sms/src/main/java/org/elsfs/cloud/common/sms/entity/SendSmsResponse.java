/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.sms.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import lombok.Data;

/**
 * 发送短信返回值
 *
 * @author zeng
 */
@Data
public class SendSmsResponse {
  @JsonProperty("headers")
  private Map<String, String> headers;

  @JsonProperty("body")
  private SendSmsResponseBody body;

  /** 发送短信返回值body */
  @Data
  public static class SendSmsResponseBody {
    private String bizId;

    private String code;

    private String message;

    private String requestId;
  }
}
