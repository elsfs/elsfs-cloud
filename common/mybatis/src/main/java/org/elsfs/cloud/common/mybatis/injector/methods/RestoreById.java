/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.injector.methods;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/** 恢复数据 */
public class RestoreById extends AbstractMethod {
  private static final String METHOD_NAME = "restoreById";

  public RestoreById() {
    super(METHOD_NAME);
  }

  /**
   * 注入自定义 MappedStatement
   *
   * @param mapperClass mapper 接口
   * @param modelClass mapper 泛型
   * @param tableInfo 数据库表反射信息
   * @return MappedStatement
   */
  @Override
  public MappedStatement injectMappedStatement(
      Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
    String format = "<script>\nUPDATE %s SET  %s WHERE %s=#{%s} \n</script>";
    String sql =
        String.format(
            format,
            tableInfo.getTableName(),
            tableInfo.getLogicDeleteSql(false, true),
            tableInfo.getKeyColumn(),
            tableInfo.getKeyProperty());
    SqlSource sqlSource = super.createSqlSource(configuration, sql, modelClass);
    return addUpdateMappedStatement(mapperClass, modelClass, methodName, sqlSource);
  }
}
