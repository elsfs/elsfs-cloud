/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.injector;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.Configuration;
import org.elsfs.cloud.common.mybatis.injector.methods.DeleteBatchByIdIgnoreLogicDelete;
import org.elsfs.cloud.common.mybatis.injector.methods.DeleteByIdIgnoreLogicDelete;
import org.elsfs.cloud.common.mybatis.injector.methods.RestoreBatchById;
import org.elsfs.cloud.common.mybatis.injector.methods.RestoreById;
import org.elsfs.cloud.common.mybatis.injector.methods.SelectListIgnoreLogicDelete;

/**
 * 自定义sql注入器
 *
 * @author zeng
 */
@Slf4j
public class ElsfsSqlInjector extends DefaultSqlInjector {
  public ElsfsSqlInjector() {
    LOGGER.info("elsfs sql injector init");
  }

  @Override
  public List<AbstractMethod> getMethodList(
      Configuration configuration, Class<?> mapperClass, TableInfo tableInfo) {
    List<AbstractMethod> methods = super.getMethodList(configuration, mapperClass, tableInfo);
    methods.add(new SelectListIgnoreLogicDelete());
    methods.add(new DeleteByIdIgnoreLogicDelete());
    methods.add(new RestoreById());
    methods.add(new RestoreBatchById());
    methods.add(new DeleteBatchByIdIgnoreLogicDelete());
    return methods;
  }
}
