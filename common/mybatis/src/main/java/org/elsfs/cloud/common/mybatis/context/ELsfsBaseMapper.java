/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.context;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 自定义mapper
 *
 * @param <T> 实体类型
 */
public interface ELsfsBaseMapper<T> extends BaseMapper<T> {
  /**
   * 根据 entity 条件，查询全部记录 忽略软删除
   *
   * @param queryWrapper 实体对象封装操作类（可以为 null）
   */
  List<T> selectListIgnoreLogicDelete(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

  /**
   * 根据 entity 条件，查询全部记录（并翻页） 忽略软删除
   *
   * @param page 分页查询条件
   * @param queryWrapper 实体对象封装操作类（可以为 null）
   * @since 3.5.3.2
   */
  List<T> selectListIgnoreLogicDelete(
      IPage<T> page, @Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

  /**
   * 根据 entity 条件，查询全部记录（并翻页）
   *
   * @param page 分页查询条件
   * @param queryWrapper 实体对象封装操作类（可以为 null）
   */
  default <P extends IPage<T>> P selectPageIgnoreLogicDelete(
      P page, @Param(Constants.WRAPPER) Wrapper<T> queryWrapper) {
    List<T> list = selectListIgnoreLogicDelete(page, queryWrapper);
    page.setRecords(list);
    return page;
  }

  /**
   * 根据 id 删除 忽略软删除
   *
   * @param id id
   * @return int
   */
  int deleteByIdIgnoreLogicDelete(Serializable id);

  /**
   * 根据 id 删除 忽略软删除
   *
   * @param entity 实体对象
   * @return int
   */
  int deleteByIdIgnoreLogicDelete(T entity);

  /**
   * 根据 id 批量删除 忽略软删除
   *
   * @param idList ids
   * @return int
   */
  int deleteBatchByIdIgnoreLogicDelete(@Param(Constants.COLL) Collection<?> idList);

  /**
   * 根据 id 恢复
   *
   * @param id id
   * @return int
   */
  int restoreById(Serializable id);

  /**
   * \* 根据 id 批量恢复
   *
   * @param idList ids
   * @return int
   */
  int restoreBatchById(@Param(Constants.COLL) Collection<?> idList);
}
