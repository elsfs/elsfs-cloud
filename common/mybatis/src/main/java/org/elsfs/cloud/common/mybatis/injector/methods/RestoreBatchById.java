/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.injector.methods;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlScriptUtils;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/** 批量恢复 */
public class RestoreBatchById extends AbstractMethod {
  private static final String METHOD_NAME = "restoreBatchById";

  public RestoreBatchById() {
    super(METHOD_NAME);
  }

  /**
   * 注入自定义 MappedStatement
   *
   * @param mapperClass mapper 接口
   * @param modelClass mapper 泛型
   * @param tableInfo 数据库表反射信息
   * @return MappedStatement
   */
  @Override
  public MappedStatement injectMappedStatement(
      Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
    String sql;
    String format = "<script>\nUPDATE %s SET %s WHERE %s IN (%s) %s\n</script>";
    sql =
        String.format(
            format,
            tableInfo.getTableName(),
            tableInfo.getLogicDeleteSql(false, true),
            tableInfo.getKeyColumn(),
            SqlScriptUtils.convertForeach(
                SqlScriptUtils.convertChoose(
                    "@org.apache.ibatis.type.SimpleTypeRegistry@isSimpleType(item.getClass())",
                    "#{item}",
                    "#{item." + tableInfo.getKeyProperty() + "}"),
                COLL,
                null,
                "item",
                COMMA),
            tableInfo.getLogicDeleteSql(true, false));

    SqlSource sqlSource = super.createSqlSource(configuration, sql, Object.class);
    return addUpdateMappedStatement(mapperClass, modelClass, methodName, sqlSource);
  }

  /**
   * 逻辑删除的 sql 脚本片段
   *
   * @param tableInfo 表信息
   * @return 逻辑删除脚本
   * @since 3.5.0
   */
  public String logicDeleteScript(TableInfo tableInfo, String format) {
    return String.format(
        format,
        tableInfo.getTableName(),
        sqlLogicSet(tableInfo),
        tableInfo.getKeyColumn(),
        SqlScriptUtils.convertForeach(
            SqlScriptUtils.convertChoose(
                "@org.apache.ibatis.type.SimpleTypeRegistry@isSimpleType(item.getClass())",
                "#{item}",
                "#{item." + tableInfo.getKeyProperty() + "}"),
            COLL,
            null,
            "item",
            COMMA),
        tableInfo.getLogicDeleteSql(true, false));
  }
}
