/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.repository;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.repository.IRepository;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import org.elsfs.cloud.common.mybatis.co.EditStateCo;
import org.elsfs.cloud.common.mybatis.context.ELsfsBaseMapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * IRepository
 *
 * @param <T> 实体类型
 * @param <ID> id类型
 */
public interface IElsfsRepository<T, ID> extends IRepository<T> {
  ELsfsBaseMapper<T> getBaseMapper();

  default boolean getAllowEditState() {
    return false;
  }

  @Transactional(rollbackFor = {Exception.class})
  default boolean saveBatch(Collection<T> entityList) {
    return this.saveBatch(entityList, 1000);
  }

  @Transactional(rollbackFor = {Exception.class})
  default boolean updateBatchById(Collection<T> entityList) {
    return updateBatchById(entityList, 1000);
  }

  @Transactional(rollbackFor = {Exception.class})
  default boolean saveOrUpdateBatch(Collection<T> entityList) {
    return saveOrUpdateBatch(entityList, 1000);
  }

  /**
   * 修改状态
   *
   * @param co 参数
   * @return 是否成功
   */
  Boolean editState(EditStateCo co);

  /**
   * 查询列表 忽略软删除
   *
   * @param queryWrapper 实体对象封装操作类 {@link
   *     com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
   */
  List<T> ignoreLogicDeleteList(Wrapper<T> queryWrapper);

  /**
   * 查询所有 忽略软删除
   *
   * @see Wrappers#emptyWrapper()
   */
  default List<T> ignoreLogicDeleteList() {
    return ignoreLogicDeleteList(Wrappers.emptyWrapper());
  }

  /**
   * 无条件翻页查询 忽略软删除
   *
   * @param page 翻页对象
   * @param <E> 实体
   * @return IPage
   */
  default <E extends IPage<T>> E ignoreLogicDeletPage(E page) {
    return ignoreLogicDeletPage(page, Wrappers.emptyWrapper());
  }

  <E extends IPage<T>> E ignoreLogicDeletPage(E page, Wrapper<T> queryWrapper);

  /**
   * 根据 ID 删除 忽略软删除 彻底删除
   *
   * @param id 主键ID
   */
  default boolean deleteByIdIgnoreLogicDelete(Serializable id) {
    return SqlHelper.retBool(getBaseMapper().deleteByIdIgnoreLogicDelete(id));
  }

  /**
   * 根据 ID 删除 忽略软删除 彻底删除
   *
   * @param entity 实体
   * @since 3.4.4
   */
  default boolean deleteByIdIgnoreLogicDelete(T entity) {
    return SqlHelper.retBool(getBaseMapper().deleteByIdIgnoreLogicDelete(entity));
  }

  /**
   * 根据 ID 批量删除 忽略软删除 彻底删除
   *
   * @param idList ids
   * @return boolean
   */
  default boolean deleteBatchByIdIgnoreLogicDelete(Collection<?> idList) {
    return SqlHelper.retBool(getBaseMapper().deleteBatchByIdIgnoreLogicDelete(idList));
  }

  /**
   * 根据 id 恢复
   *
   * @param id id
   * @return boolean
   */
  default boolean restoreById(Serializable id) {
    return SqlHelper.retBool(getBaseMapper().restoreById(id));
  }

  /**
   * 根据 id 批量恢复
   *
   * @param idList ids
   * @return boolean
   */
  default boolean restoreBatchById(Collection<?> idList) {
    return SqlHelper.retBool(getBaseMapper().restoreBatchById(idList));
  }
}
