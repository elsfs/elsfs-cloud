/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.injector.methods;

import com.baomidou.mybatisplus.core.injector.methods.DeleteById;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlScriptUtils;
import org.apache.ibatis.mapping.MappedStatement;

/** 删除软删除数据 */
public class DeleteBatchByIdIgnoreLogicDelete extends DeleteById {
  private static final String METHOD_NAME = "deleteBatchByIdIgnoreLogicDelete";

  public DeleteBatchByIdIgnoreLogicDelete() {
    super(METHOD_NAME);
  }

  @Override
  public MappedStatement injectMappedStatement(
      Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
    var format = "<script>\n DELETE FROM %s WHERE %s  IN (%s) AND %s\n</script>";
    var sql =
        String.format(
            format,
            tableInfo.getTableName(),
            tableInfo.getKeyColumn(),
            SqlScriptUtils.convertForeach(
                SqlScriptUtils.convertChoose(
                    "@org.apache.ibatis.type.SimpleTypeRegistry@isSimpleType(item.getClass())",
                    "#{item}",
                    "#{item." + tableInfo.getKeyProperty() + "}"),
                COLL,
                null,
                "item",
                COMMA),
            tableInfo.getLogicDeleteSql(false, false));
    return this.addDeleteMappedStatement(
        mapperClass, methodName, createSqlSource(configuration, sql, Object.class));
  }
}
