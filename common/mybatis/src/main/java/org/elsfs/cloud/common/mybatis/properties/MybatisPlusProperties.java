/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.properties;

import java.util.HashSet;
import java.util.Set;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * mybatis plus配置参数
 *
 * @author zeng
 */
@Data
@ConfigurationProperties("elsfs.mybatis")
public class MybatisPlusProperties {
  @NestedConfigurationProperty private final Tenant tenant = new Tenant();

  /** 租户配置 */
  @Data
  public static class Tenant {
    /** 是否启用多租户 */
    private Boolean enabled = true;

    /** 获取租户字段名 */
    private String tenantIdColumn = "tenant_id";

    /** 忽略多租户表 ，一般情况主要是忽略中间表，也可以添加中间表的租户字段 */
    private Set<String> ignoreTable = new HashSet<>();
  }
}
