/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.repository;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.repository.CrudRepository;
import java.util.List;
import org.elsfs.cloud.common.mybatis.co.EditStateCo;
import org.elsfs.cloud.common.mybatis.context.ELsfsBaseMapper;

/**
 * 默认实现
 *
 * @author zeng
 * @param <M> mapper
 * @param <T> entity
 * @param <ID> id
 */
public abstract class ElsfsCrudRepositoryImpl<M extends ELsfsBaseMapper<T>, T, ID>
    extends CrudRepository<M, T> implements IElsfsRepository<T, ID> {

  @Override
  public Boolean editState(EditStateCo co) {
    if (getAllowEditState()) {
      TableInfo tableInfo = TableInfoHelper.getTableInfo(getEntityClass());
      String keyColumn = tableInfo.getKeyColumn();
      return update(
          Wrappers.<T>update().in(keyColumn, co.getIds()).set(co.getColumn(), co.getStatus()));
    }
    throw new IllegalArgumentException("不支持修改状态");
  }

  /**
   * 查询列表 忽略软删除
   *
   * @param queryWrapper 实体对象封装操作类 {@link QueryWrapper}
   */
  @Override
  public List<T> ignoreLogicDeleteList(Wrapper<T> queryWrapper) {
    return getBaseMapper().selectListIgnoreLogicDelete(queryWrapper);
  }

  @Override
  public <E extends IPage<T>> E ignoreLogicDeletPage(E page, Wrapper<T> queryWrapper) {
    return getBaseMapper().selectPageIgnoreLogicDelete(page, queryWrapper);
  }
}
