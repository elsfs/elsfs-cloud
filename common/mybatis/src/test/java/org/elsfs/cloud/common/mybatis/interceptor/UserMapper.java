/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.interceptor;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.io.Serializable;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.elsfs.cloud.common.annotations.EnableCipher;

/** UserMapper */
@Mapper
public interface UserMapper extends BaseMapper<User> {
  /**
   * 添加加密数据
   *
   * @param entity 实体
   * @return 成功条数
   */
  @Override
  @EnableCipher(parameter = EnableCipher.CipherType.ENCRYPT)
  int insert(User entity);

  /**
   * 获取添加的加密数据
   *
   * @param id id
   * @return 获取添加的加密数据
   */
  @Override
  User selectById(Serializable id);

  /**
   * 获取解密数据
   *
   * @param queryWrapper 条件
   * @return 解密数据
   */
  @EnableCipher(result = EnableCipher.CipherType.DECRYPT)
  @Override
  User selectOne(@Param("ew") Wrapper<User> queryWrapper);
}
