/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.mybatis.interceptor;

import org.elsfs.cloud.common.mybatis.configuration.MybatisPlusConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@MapperScan("org.elsfs.cloud.common.mybatis.interceptor")
@Import(MybatisPlusConfiguration.class)
class MybatisSensitiveInterceptorTest {

  @Autowired private UserMapper userMapper;

  /** 数据加密测试 */
  @Test
  public void testEncrypt() {
    var name = "Elsfs-Cloud";
    userMapper.insert(User.builder().id("100").name(name).build());
    User user = userMapper.selectById("100");
    Assertions.assertEquals(user.getName(), new NameCipherAlgorithm().encrypt(name));
  }
}
