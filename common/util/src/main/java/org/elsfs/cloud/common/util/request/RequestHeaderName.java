/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.request;

/**
 * 请求头名称
 *
 * @author zeng
 */
public class RequestHeaderName {
  /**
   * X-Forwarded-For（XFF）是用来识别通过HTTP代理或负载均衡方式连接到Web服务器的客户端最原始的IP地址的HTTP请求头字段。 Squid
   * 缓存代理服务器的开发人员最早引入了这一HTTP头字段， 并由IETF在HTTP头字段标准化草案中正式提出。 X-Forwarded-For: client1, proxy1, proxy2,
   * proxy3 其中的值通过一个 逗号+空格 把多个IP地址区分开, 最左边(client1)是最原始客户端的IP地址, 代理服务器每成功收到一个请求， 就把请求来源IP地址添加到右边。
   * 在上面这个例子中，这个请求成功通过了三台代理服务器：proxy1, proxy2 及 proxy3。请求由client1发出，
   * 到达了proxy3(proxy3可能是请求的终点)。请求刚从client1中发出时，XFF是空的，请求被发往proxy1；通过proxy1的时候，client1被添加到XFF中，
   * 之后请求被发往proxy2;通过proxy2的时候，proxy1被添加到XFF中，之后请求被发往proxy3；通过proxy3时，
   * proxy2被添加到XFF中，之后请求的的去向不明，如果proxy3不是请求终点，请求会被继续转发。
   */
  public static String X_FORWARDED_FOR = "x-forwarded-for";

  /** 一些代理服务器会将客户端的IP地址放在Proxy-Client-IP请求头中。例如：Proxy-Client-IP: 192.168.1.1。 */
  public static String PROXY_CLIENT_IP = "Proxy-Client-IP";

  /**
   * WebLogic Server使用的代理服务器将客户端的IP地址放在WL-Proxy-Client-IP请求头中。例如：WL-Proxy-Client-IP: 192.168.1.1。
   */
  public static String WL_PROXY_CLIENT_IP = "WL-Proxy-Client-IP";
}
