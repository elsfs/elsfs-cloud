/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.lang;

import java.io.File;

/**
 * 文件相关工具
 *
 * @author zeng
 */
public class FileUtils {
  public static final String FILE_SEPARATOR = System.getProperty("file.separator");

  /**
   * 获取文件路径，根据系统进行处理
   *
   * @param path {@link String}
   * @return {@link String}
   */
  public static String getRealFilePath(String path) {
    return path.replace("/", FILE_SEPARATOR).replace("\\", FILE_SEPARATOR);
  }

  /**
   * 获取全路径中的文件名
   *
   * @param file 文件
   * @return 文件名
   */
  public static String getFileName(File file) {
    if (file == null) {
      return null;
    }
    return getFileName(file.getPath());
  }

  /**
   * 获取全路径中的文件名
   *
   * @param filePath 文件路径
   * @return 文件名
   */
  public static String getFileName(String filePath) {
    if (FILE_SEPARATOR.equals(filePath)) {
      return filePath;
    }
    int lastSep = filePath.lastIndexOf(File.separator);
    return lastSep == -1 ? filePath : filePath.substring(lastSep + 1);
  }

  /**
   * 根据文件路径获取文件
   *
   * @param filePath 文件路径
   * @return 文件
   */
  public static File getFileByPath(String filePath) {
    return File.separator.equals(filePath) ? null : new File(filePath);
  }

  /**
   * 判断文件是否存在
   *
   * @param filePath 文件路径
   * @return {@code true}: 存在<br>
   *     {@code false}: 不存在
   */
  public static boolean isFileExists(String filePath) {
    return isFileExists(getFileByPath(filePath));
  }

  /**
   * 判断文件是否存在
   *
   * @param file 文件
   * @return {@code true}: 存在<br>
   *     {@code false}: 不存在
   */
  public static boolean isFileExists(File file) {
    return file != null && file.exists();
  }
}
