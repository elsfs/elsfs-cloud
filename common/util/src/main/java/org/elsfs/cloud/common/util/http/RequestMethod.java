/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.http;

/**
 * 请求方法
 *
 * @author zeng
 */
public enum RequestMethod {
  GET,
  HEAD,
  POST,
  PUT,
  PATCH,
  DELETE,
  OPTIONS,
  TRACE;

  /**
   * 解析请求方法
   *
   * @param method 请求方法
   * @return 请求方法
   */
  public static RequestMethod resolve(String method) {
    if (method == null) {
      throw new IllegalArgumentException("Method must not be null");
    }
    return switch (method) {
      case "GET" -> GET;
      case "HEAD" -> HEAD;
      case "POST" -> POST;
      case "PUT" -> PUT;
      case "PATCH" -> PATCH;
      case "DELETE" -> DELETE;
      case "OPTIONS" -> OPTIONS;
      case "TRACE" -> TRACE;
      default -> null;
    };
  }
}
