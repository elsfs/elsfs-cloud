/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.sql;

import java.io.Serializable;
import lombok.Getter;

/**
 * 数据库类型
 *
 * @author zeng
 */
@Getter
public enum DatabaseType implements Serializable {
  /** MYSQL */
  MYSQL("mysql", "MySql数据库"),

  /** MARIA DB */
  MARIADB("mariadb", "MariaDB数据库"),

  /** ORACLE */
  ORACLE("oracle", "Oracle数据库"),

  /** DB2 */
  DB2("db2", "DB2数据库"),

  /** H2 */
  H2("h2", "H2数据库"),

  /** HSQL */
  HSQL("hsql", "HSQL数据库"),

  /** SQLITE */
  SQLITE("sqlite", "SQLite数据库"),

  /** POSTGRE */
  POSTGRE_SQL("PostgreSql", "Postgre数据库"),

  /** SQL SERVER 2005 */
  SQL_SERVER2005("sqlServer2005", "SQLServer2005数据库"),

  /** SQLSERVER */
  SQL_SERVER("sqlserver", "SQLServer数据库"),

  /** DM */
  DM("dm", "达梦数据库"),

  /** HIGHGO */
  HIGHGO("highgo", "瀚高数据库"),

  /** xugu */
  XU_GU("xugu", "虚谷数据库"),

  /** Kingbase */
  KINGBASE_ES("kingbasees", "人大金仓数据库"),

  /** Phoenix */
  PHOENIX("phoenix", "Phoenix HBase数据库"),

  /** CacheDB */
  CACHEDB("cachedb", "Cache 数据库"),

  /** UNKONWN DB */
  OTHER("other", "其他数据库");

  /** 数据库名称 */
  private final String name;

  /** 描述 */
  private final String desc;

  /**
   * 构造
   *
   * @param name {@link String} 名称
   * @param desc {@link String} 描述
   */
  DatabaseType(String name, String desc) {
    this.name = name;
    this.desc = desc;
  }

  /**
   * 获取数据库类型
   *
   * @param dbType {@link String} 数据库类型字符串
   * @return {@link DatabaseType}
   */
  public static DatabaseType getType(String dbType) {
    DatabaseType[] dts = DatabaseType.values();
    for (DatabaseType dt : dts) {
      if (dt.getName().equalsIgnoreCase(dbType)) {
        return dt;
      }
    }
    return OTHER;
  }
}
