/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.pinyin;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 资源文件加载类
 *
 * @author zeng
 */
final class PinyinResource {

  private PinyinResource() {}

  private static Reader newClassPathReader(String classpath) {
    InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(classpath);
    return new InputStreamReader(is, StandardCharsets.UTF_8);
  }

  static Reader newFileReader(String path) throws FileNotFoundException {
    return new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8);
  }

  static Map<String, String> getResource(Reader reader) {
    Map<String, String> map = new ConcurrentHashMap<String, String>();
    try {
      BufferedReader br = new BufferedReader(reader);
      String line = null;
      while ((line = br.readLine()) != null) {
        String[] tokens = line.trim().split("=");
        map.put(tokens[0], tokens[1]);
      }
      br.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return map;
  }

  static Map<String, String> getPinyinResource() {
    return getResource(newClassPathReader("pinyin/pinyin.dict"));
  }

  static Map<String, String> getMutilPinyinResource() {
    return getResource(newClassPathReader("pinyin/mutil_pinyin.dict"));
  }

  static Map<String, String> getChineseResource() {
    return getResource(newClassPathReader("pinyin/chinese.dict"));
  }
}
