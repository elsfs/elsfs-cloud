/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.lang;

import java.util.Collection;
import java.util.Map;
import lombok.experimental.UtilityClass;

/**
 * Collection 工具类
 *
 * @author zeng
 */
@UtilityClass
public class CollectionUtils {

  /**
   * 校验集合是否为空
   *
   * @param coll {@link Collection} 入参
   * @return {@link Boolean} boolean
   */
  public boolean isEmpty(Collection<?> coll) {
    return (coll == null || coll.isEmpty());
  }

  /**
   * 判断Map是否为空
   *
   * @param map {@link Map} 入参
   * @return {@link Boolean} boolean
   */
  public boolean isEmpty(Map<?, ?> map) {
    return (map == null || map.isEmpty());
  }

  /**
   * 校验集合是否不为空
   *
   * @param coll {@link Collection} 入参
   * @return {@link Boolean} boolean
   */
  public boolean isNotEmpty(Collection<?> coll) {
    return !isEmpty(coll);
  }

  /**
   * 判断Map是否不为空
   *
   * @param map {@link Map} 入参
   * @return {@link Boolean} boolean
   */
  public boolean isNotEmpty(Map<?, ?> map) {
    return !isEmpty(map);
  }
}
