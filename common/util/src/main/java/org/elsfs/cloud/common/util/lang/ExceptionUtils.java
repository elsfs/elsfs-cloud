/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.lang;

import lombok.experimental.UtilityClass;
import org.elsfs.cloud.common.util.exception.ElsfsException;

/**
 * 异常工具类
 *
 * @author zeng
 */
@UtilityClass
public class ExceptionUtils {

  /**
   * 返回一个新的异常，统一构建，方便统一处理
   *
   * @param msg 消息
   * @param t 异常信息
   * @return 返回异常
   */
  public static ElsfsException mpe(String msg, Throwable t, Object... params) {
    return new ElsfsException(String.format(msg, params), t);
  }

  /**
   * 重载的方法
   *
   * @param msg 消息
   * @return 返回异常
   */
  public static ElsfsException mpe(String msg, Object... params) {
    return new ElsfsException(String.format(msg, params));
  }

  /**
   * 重载的方法
   *
   * @param t 异常
   * @return 返回异常
   */
  public static ElsfsException mpe(Throwable t) {
    return new ElsfsException(t);
  }

  /**
   * throwMpe
   *
   * @param condition condition
   * @param msg msg
   * @param params params
   */
  public static void throwMpe(boolean condition, String msg, Object... params) {
    if (condition) {
      throw mpe(msg, params);
    }
  }
}
