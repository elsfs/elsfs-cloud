/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.lang;

public class CharUtil implements CharPool {
  /**
   * 检查是否为数字字符，数字字符指0~9
   *
   * <pre>
   *   CharUtil.isNumber('a')  = false
   *   CharUtil.isNumber('A')  = false
   *   CharUtil.isNumber('3')  = true
   *   CharUtil.isNumber('-')  = false
   *   CharUtil.isNumber('\n') = false
   *   CharUtil.isNumber('&copy;') = false
   * </pre>
   *
   * @param ch 被检查的字符
   * @return true表示为数字字符，数字字符指0~9
   */
  public static boolean isNumber(char ch) {
    return ch >= '0' && ch <= '9';
  }
}
