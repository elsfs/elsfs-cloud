/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.lang;

import java.util.Collection;
import java.util.Map;

/**
 * 断言
 *
 * @author zeng
 */
public class Assert {

  /**
   * 断言 boolean 为 true
   *
   * <p>为 false 则抛出异常
   *
   * @param expression {@link Boolean} boolean 值
   * @param message {@link String}消息
   * @param params {@link String} 参数
   */
  public static void isTrue(boolean expression, String message, Object... params) {
    if (!expression) {
      throw ExceptionUtils.mpe(message, params);
    }
  }

  /**
   * 断言 boolean 为 false
   *
   * <p>为 true 则抛出异常
   *
   * @param expression {@link Boolean} boolean 值
   * @param message {@link String} 消息
   * @param params {@link Object}
   */
  public static void isFalse(boolean expression, String message, Object... params) {
    isTrue(!expression, message, params);
  }

  /**
   * 断言 object 为 null
   *
   * <p>不为 null 则抛异常
   *
   * @param object {@link String} 对象
   * @param message {@link String}消息
   * @param params {@link String} 参数
   */
  public static void isNull(Object object, String message, Object... params) {
    isTrue(object == null, message, params);
  }

  /**
   * 断言 object 不为 null
   *
   * <p>为 null 则抛异常
   *
   * @param object {@link String} 对象
   * @param message {@link String}消息
   * @param params {@link String} 参数
   */
  public static void notNull(Object object, String message, Object... params) {
    isTrue(object != null, message, params);
  }

  /**
   * 断言 value 不为 empty
   *
   * <p>为 empty 则抛异常
   *
   * @param value {@link String} 字符串
   * @param message {@link String}消息
   * @param params {@link String} 参数
   */
  public static void notEmpty(CharSequence value, String message, Object... params) {
    isTrue(StringUtils.isNotBlank(value), message, params);
  }

  /**
   * 断言 collection 不为 empty
   *
   * <p>为 empty 则抛异常
   *
   * @param collection {@link Collection} 集合
   * @param message {@link String}消息
   * @param params {@link String} 参数
   */
  public static void notEmpty(Collection<?> collection, String message, Object... params) {
    isTrue(CollectionUtils.isNotEmpty(collection), message, params);
  }

  /**
   * 断言 map 不为 empty
   *
   * <p>为 empty 则抛异常
   *
   * @param map {@link Map} 集合
   * @param message {@link String}消息
   * @param params {@link String} 参数
   */
  public static void notEmpty(Map<?, ?> map, String message, Object... params) {
    isTrue(CollectionUtils.isNotEmpty(map), message, params);
  }

  /**
   * 断言 数组 不为 empty
   *
   * <p>为 empty 则抛异常
   *
   * @param array {@link Object} 数组
   * @param message {@link String}消息
   * @param params {@link String} 参数
   */
  public static void notEmpty(Object[] array, String message, Object... params) {
    isTrue(ArrayUtils.isNotEmpty(array), message, params);
  }
}
