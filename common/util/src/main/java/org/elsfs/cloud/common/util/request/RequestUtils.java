/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.request;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 请求工具类
 *
 * @author zeng
 */
public class RequestUtils {
  /** 未知的 */
  public static String UNKNOWN = "unknown";

  /**
   * 获取请求IP
   *
   * @param request IP
   * @return IP Address
   */
  public static String getIpAddrByRequest(HttpServletRequest request) {
    String ip = request.getHeader(RequestHeaderName.X_FORWARDED_FOR);
    if (ip == null || ip.isEmpty() || UNKNOWN.equalsIgnoreCase(ip)) {
      ip = request.getHeader(RequestHeaderName.PROXY_CLIENT_IP);
    }
    if (ip == null || ip.isEmpty() || UNKNOWN.equalsIgnoreCase(ip)) {
      ip = request.getHeader(RequestHeaderName.WL_PROXY_CLIENT_IP);
    }
    if (ip == null || ip.isEmpty() || UNKNOWN.equalsIgnoreCase(ip)) {
      ip = request.getRemoteAddr();
    }
    return ip;
  }
}
