/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.util.lang;

import java.util.LinkedHashMap;

/** 枚举工具类 */
public class EnumUtils {
  /**
   * 判断某个值是存在枚举中
   *
   * @param <E> 枚举类型
   * @param enumClass 枚举类
   * @param val 需要查找的值
   * @return 是否存在
   */
  public static <E extends Enum<E>> boolean contains(final Class<E> enumClass, String val) {
    return EnumUtils.getEnumMap(enumClass).containsKey(val);
  }

  /**
   * 获取枚举字符串值和枚举对象的Map对应，使用LinkedHashMap保证有序<br>
   * 结果中键为枚举名，值为枚举对象
   *
   * @param <E> 枚举类型
   * @param enumClass 枚举类
   * @return 枚举字符串值和枚举对象的Map对应，使用LinkedHashMap保证有序
   */
  public static <E extends Enum<E>> LinkedHashMap<String, E> getEnumMap(final Class<E> enumClass) {
    final LinkedHashMap<String, E> map = new LinkedHashMap<>();
    for (final E e : enumClass.getEnumConstants()) {
      map.put(e.name(), e);
    }
    return map;
  }
}
