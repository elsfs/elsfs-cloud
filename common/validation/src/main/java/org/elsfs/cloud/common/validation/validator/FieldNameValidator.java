/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.validation.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import org.elsfs.cloud.common.validation.annotation.FieldName;

/** 字段名称 的验证器 */
public class FieldNameValidator implements ConstraintValidator<FieldName, String> {

  private Class<?> validationClass;

  @Override
  public void initialize(FieldName constraintAnnotation) {
    validationClass = constraintAnnotation.value();
  }

  @Override
  public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
    Field[] fields = validationClass.getDeclaredFields();
    for (Field field : fields) {
      if (field.getName().equals(s)) {
        return true;
      }
    }
    return false;
  }
}
