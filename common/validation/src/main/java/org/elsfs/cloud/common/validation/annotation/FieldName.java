/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.validation.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.elsfs.cloud.common.validation.validator.FieldNameValidator;

/**
 * 验证是否class的字段
 *
 * @author zeng
 */

// 注解的作用目标(可以设置作用在类，方法等等）
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME) // 注解的保留策略（注解的生命周期)
@Documented
@Constraint(validatedBy = FieldNameValidator.class) // 不同之处：与注解关联的验证器
public @interface FieldName {
  /**
   * 类名称
   *
   * @return class
   */
  Class<?> value();

  /**
   * 注解验证不通过时输出的信息
   *
   * @return message
   */
  String message() default "不是class的字段";

  /**
   * 约束注解在验证时所属的组别
   *
   * @return groups
   */
  Class<?>[] groups() default {};

  /**
   * 约束注解的有效负载
   *
   * @return payload
   */
  Class<? extends Payload>[] payload() default {};
}
