/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.validation.validator;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FieldNameValidatorTest {
  private String txt;

  @Test
  void isValid() {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    FiledNameTest filedNameTest = new FiledNameTest();
    filedNameTest.setName("txt");
    Set<ConstraintViolation<FiledNameTest>> set = validator.validate(filedNameTest);
    Assertions.assertTrue(set.isEmpty());
    filedNameTest.setName("test");
    Set<ConstraintViolation<FiledNameTest>> set1 = validator.validate(filedNameTest);
    Assertions.assertFalse(set1.isEmpty());
  }
}
