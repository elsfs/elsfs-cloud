/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.pay.service;

import com.ijpay.wxpay.WxPayApiConfig;
import org.elsfs.cloud.common.pay.entity.WxPay;

/**
 * 微信支付
 *
 * @author zeng
 */
public interface WxPayConfigService {
  /**
   * 保存或更新微信支付配置信息。
   *
   * @param wxPay 微信支付配置对象，包含需要保存或更新的配置信息。
   * @return 返回更新后的微信支付配置对象。
   */
  WxPay save(WxPay wxPay);

  /**
   * 更新微信支付配置信息。
   *
   * @param wxPay 微信支付配置对象，包含需要更新的配置信息。
   * @return 返回更新后的微信支付配置对象。
   */
  WxPay update(WxPay wxPay);

  /**
   * 根据appId删除微信支付配置信息。
   *
   * @param appId 应用ID，用于标识需要删除的微信支付配置。
   */
  void delete(String appId);

  /**
   * 根据appId获取本地配置的微信支付信息。
   *
   * @param appId 应用ID，用于查找对应的微信支付配置。
   * @return 返回找到的微信支付配置对象。
   */
  WxPay localConfig(String appId);

  /**
   * 获取默认的本地微信支付配置。
   *
   * @return 返回默认的微信支付配置对象。
   */
  WxPay defaultLocal();

  /**
   * 构建支付
   *
   * @param wxPay 支付配置
   * @return config
   */
  default WxPayApiConfig builder(WxPay wxPay) {
    return WxPayApiConfig.builder()
        .appId(wxPay.getAppId())
        .mchId(wxPay.getMchId())
        .slAppId(wxPay.getSlAppId())
        .slMchId(wxPay.getSlMchId())
        .apiKey(wxPay.getApiKey())
        .apiKey3(wxPay.getApiKey3())
        .domain(wxPay.getDomain())
        .certP12Path(wxPay.getCertP12Path())
        .certPath(wxPay.getCertPath())
        .certPath(wxPay.getCertPath())
        .platformCertPath(wxPay.getPlatformCertPath())
        .build();
  }
}
