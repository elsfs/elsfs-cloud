/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.pay.service;

import com.ijpay.unionpay.UnionPayApiConfig;
import org.elsfs.cloud.common.pay.entity.UnionPay;

/**
 * 银联支付
 *
 * @author zeng
 */
public interface UnionPayConfigService {
  /**
   * 保存银联支付信息。
   *
   * @param unionPay 包含银联支付详细信息的对象。
   * @return 返回保存后的银联支付对象。
   */
  UnionPay save(UnionPay unionPay);

  /**
   * 更新银联支付信息。
   *
   * @param unionPay 包含更新后的银联支付详细信息的对象。
   * @return 返回更新后的银联支付对象。
   */
  UnionPay update(UnionPay unionPay);

  /**
   * 删除指定的银联支付信息。
   *
   * @param appId 需要删除的银联支付的应用ID。
   */
  void delete(String appId);

  /**
   * 获取本地配置的银联支付信息。
   *
   * @param appId 请求银联支付配置的应用ID。
   * @return 返回对应应用ID的银联支付配置对象。
   */
  UnionPay localConfig(String appId);

  /**
   * 配置
   *
   * @param unionPay 配置
   * @return 配置
   */
  default UnionPayApiConfig builder(UnionPay unionPay) {
    UnionPayApiConfig config = new UnionPayApiConfig();
    config.setMchId(unionPay.getMchId());
    config.setGroupMchId(unionPay.getGroupMchId());
    config.setAgentMchId(unionPay.getAgentMchId());
    config.setApiKey(unionPay.getApiKey());
    config.setServerUrl(unionPay.getServerUrl());
    config.setDomain(unionPay.getDomain());
    return config;
  }
}
