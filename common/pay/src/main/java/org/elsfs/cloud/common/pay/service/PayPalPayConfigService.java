/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.pay.service;

import com.ijpay.paypal.PayPalApiConfig;
import org.elsfs.cloud.common.pay.entity.PayPalPay;
import org.elsfs.cloud.common.pay.entity.WxPay;

/**
 * payPal支付
 *
 * @author zeng
 */
public interface PayPalPayConfigService {
  /**
   * 保存PayPal支付信息
   *
   * @param payPalPay 包含支付信息的对象
   * @return 返回保存后的PayPal支付信息对象
   */
  PayPalPay save(PayPalPay payPalPay);

  /**
   * 更新PayPal支付信息
   *
   * @param payPalPay 包含更新后的支付信息的对象
   * @return 返回更新后的PayPal支付信息对象
   */
  PayPalPay update(WxPay payPalPay);

  /**
   * 删除指定应用ID的PayPal支付信息
   *
   * @param appId 要删除的支付信息的应用ID
   */
  void delete(String appId);

  /**
   * 获取指定应用ID的本地配置信息
   *
   * @param appId 需要获取配置信息的应用ID
   * @return 返回指定应用ID的本地配置信息
   */
  PayPalPay localConfig(String appId);

  /**
   * 配置
   *
   * @param payPalPay payPalPay
   * @return config
   */
  default PayPalApiConfig builder(PayPalPay payPalPay) {
    PayPalApiConfig config = new PayPalApiConfig();
    config.setClientId(payPalPay.getClientId());
    config.setSecret(payPalPay.getSecret());
    config.setDomain(payPalPay.getDomain());
    config.setSandBox(payPalPay.isSandBox());
    return config;
  }
}
