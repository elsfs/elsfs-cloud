/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.pay.entity;

import lombok.Data;

/**
 * 银联支付
 *
 * @author zeng
 */
@Data
public class UnionPay {
  /** 商户平台分配的账号 */
  private String mchId;

  /** 连锁商户号 */
  private String groupMchId;

  /** 授权交易机构代码 */
  private String agentMchId;

  /** 商户平台分配的密钥 */
  private String apiKey;

  /** 商户平台网关 */
  private String serverUrl;

  /** 应用域名，回调中会使用此参数 */
  private String domain;
}
