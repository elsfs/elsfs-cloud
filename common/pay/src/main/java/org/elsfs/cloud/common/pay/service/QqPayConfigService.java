/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.pay.service;

import com.ijpay.qqpay.QqPayApiConfig;
import org.elsfs.cloud.common.pay.entity.QqPay;

/**
 * qq支付
 *
 * @author zeng
 */
public interface QqPayConfigService {
  /**
   * 保存QQ支付配置信息。
   *
   * @param qqPay QQ支付对象，包含需要保存的配置信息。
   * @return 返回保存后的QQ支付对象。
   */
  QqPay save(QqPay qqPay);

  /**
   * 更新QQ支付配置信息。
   *
   * @param qqPay QQ支付对象，包含需要更新的配置信息。
   * @return 返回更新后的QQ支付对象。
   */
  QqPay update(QqPay qqPay);

  /**
   * 根据应用ID删除QQ支付配置信息。
   *
   * @param appId 应用的唯一标识符。
   */
  void delete(String appId);

  /**
   * 根据应用ID获取本地配置的QQ支付信息。
   *
   * @param appId 应用的唯一标识符。
   * @return 返回对应应用ID的QQ支付配置对象。
   */
  QqPay localConfig(String appId);

  /**
   * 配置
   *
   * @param qqPay 配置
   * @return 配置
   */
  default QqPayApiConfig builder(QqPay qqPay) {
    return QqPayApiConfig.builder()
        .appId(qqPay.getAppId())
        .mchId(qqPay.getMchId())
        .slAppId(qqPay.getSlAppId())
        .slMchId(qqPay.getSlMchId())
        .domain(qqPay.getDomain())
        .certPath(qqPay.getCertPath())
        .certPath(qqPay.getCertPath())
        .build();
  }
}
