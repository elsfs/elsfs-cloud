/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.pay.service;

import com.ijpay.jdpay.JdPayApiConfig;
import org.elsfs.cloud.common.pay.entity.JdPay;

/**
 * 京东支付
 *
 * @author zeng
 */
public interface JdPayConfigService {

  /**
   * 保存京东支付信息
   *
   * @param jdPay 一个包含京东支付详细信息的对象
   * @return 返回保存后的京东支付对象。该对象可能已经包含了系统自动添加的字段，如保存时间等。
   */
  JdPay save(JdPay jdPay);

  /**
   * 更新京东支付信息
   *
   * @param jdPay 一个包含京东支付信息的对象
   * @return 返回更新后的京东支付信息对象。如果更新成功，返回更新后的对象；如果更新失败，可能返回原对象或相应的错误对象。
   */
  JdPay update(JdPay jdPay);

  /**
   * 删除指定的应用。
   *
   * @param appId 应用的唯一标识符，用于指定要删除的应用。
   */
  void delete(String appId);

  /**
   * 本地配置接口
   *
   * @param appId 应用ID，用于标识调用此接口的应用
   * @return JdPay 返回一个JdPay对象，该对象包含了本地配置信息
   */
  JdPay localConfig(String appId);

  /**
   * 配置
   *
   * @param jdPay 配置
   * @return 配置
   */
  default JdPayApiConfig builder(JdPay jdPay) {
    return JdPayApiConfig.builder()
        .appId(jdPay.getAppId())
        .mchId(jdPay.getMchId())
        .domain(jdPay.getDomain())
        .certPath(jdPay.getCertPath())
        .certPath(jdPay.getCertPath())
        .rsaPublicKey(jdPay.getRsaPublicKey())
        .rsaPrivateKey(jdPay.getRsaPrivateKey())
        .build();
  }
}
