/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.pay.entity;

import lombok.Data;

/**
 * 微信支付参数
 *
 * @author zeng
 */
@Data
public class WxPay {
  /** 应用编号 */
  private String appId;

  /** 商户号 */
  private String mchId;

  /** 服务商应用编号 */
  private String slAppId;

  /** 服务商商户号 */
  private String slMchId;

  /** 商户平台「API安全」中的 API 密钥 */
  private String apiKey;

  /** 商户平台「API安全」中的 APIv3 密钥 */
  private String apiKey3;

  /** 应用域名，回调中会使用此参数 */
  private String domain;

  /** API 证书中的 p12 */
  private String certP12Path;

  /** API 证书中的 key.pem */
  private String keyPath;

  /** API 证书中的 cert.pem */
  private String certPath;

  /** 微信平台证书 */
  private String platformCertPath;
}
