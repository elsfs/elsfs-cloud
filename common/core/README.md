## 敏感词库组件
 SensitiveBye的敏感词组件是SensitiveWordProvider，默认自动注入，需要使用的时候初始化即可：
```java
@Bean
public SensitiveWordProvider sensitiveWordProvider(){
    return new SensitiveWordProvider();
}
```
 SensitiveWordProvider提供了一个有参构造器，用于以不同的方式获取词库，SensitiveBye内置了两种方式：
- SensitiveWordSourceFromResource (获取resource目录下的sensitive.txt文件, 可自定义文件名)
- SensitiveWordSourceFromUrl(传入一个url，从网络获取词库文件)
你可以通过实现ISensitiveWordSource接口的loadSource()自定义获取词库的方式。

 SensitiveWordProvider提供了三个方法：

```java
//handle方法用于将传入的字符串中的敏感词替换成输入的符号
String handle(String word, String symbol);
//contain方法用于检测传入的字符串中包含的敏感词组
List<String> contain(String word);
//reload方法用于重新载入词库
void reload();
```
```java
    @Resource
    private SensitiveWordProvider sensitiveWordProvider;
         sensitiveWordProvider.contain(msg);


```
## 字段脱敏组件
SensitiveBye的脱敏组件，默认自动注入
参考测试 org.elsfs.cloud.common.core.jackson.SensitiveByeSpringTest
```java
/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 测试
 *
 * @author zeng
 */
@NoArgsConstructor
@Slf4j
@SpringBootTest
public class SensitiveByeSpringTest {
  @Autowired ObjectMapper objectMapper;

  @Test
  public void jsonUser() throws JsonProcessingException {
    final User user =
        User.builder()
            .id("123")
            .phone("13212341234")
            .name("你好")
            .idCard("522322188012120123")
            .password("123456")
            .mobile("010-88880000")
            .email("abc@163.com")
            .address("北京市朝阳区十里堡123号")
            .bankCard("622312312341234123")
            .carNumber("京A-1234567")
            .custom("test自定义")
            .build();
    final String string = objectMapper.writeValueAsString(user);
    LOGGER.warn(string);
  }
}
```
实体
```java

/**
 * user
 *
 * @author zeng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
  private String id;

  @SensitiveBye(strategy = SensitiveType.PHONE)
  private String phone;

  @SensitiveBye(strategy = SensitiveType.CHINESE_NAME)
  private String name;

  @SensitiveBye(strategy = SensitiveType.ID_CARD)
  private String idCard;

  @SensitiveBye(strategy = SensitiveType.PASSWORD)
  private String password;

  @SensitiveBye(strategy = SensitiveType.MOBILE)
  private String mobile;

  @SensitiveBye(strategy = SensitiveType.EMAIL)
  private String email;

  @SensitiveBye(strategy = SensitiveType.ADDRESS)
  private String address;

  @SensitiveBye(strategy = SensitiveType.BANK_CARD)
  private String bankCard;

  @SensitiveBye(strategy = SensitiveType.CAR_NUMBER)
  private String carNumber;

  @SensitiveBye(strategy = SensitiveType.CUSTOM)
  private String custom;
}

```

