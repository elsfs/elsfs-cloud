/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.entity;

import cn.idev.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.elsfs.cloud.common.core.utils.TreeNode;

/**
 * tree entity
 *
 * @author zeng
 */
@SuppressWarnings("all")
public abstract class BaseTreeEntity<T extends TreeNode<T, ID>, ID> extends BaseSpellEntity
    implements TreeNode<T, ID> {
  @Schema(description = "子节点")
  @TableField(exist = false)
  @JsonInclude(JsonInclude.Include.NON_NULL)
  @ExcelIgnore
  private List<T> children = new ArrayList<>();

  /** 是否有子节点 */
  @Schema(description = "是否有子节点")
  @TableField(exist = false)
  @ExcelIgnore
  private boolean isChild = hasChild();

  public void setChildren(List<T> children) {
    if (children == null) {
      return;
    }
    this.children = children;
  }
  public List<T> getChildren() {
    return children;
  }
}
