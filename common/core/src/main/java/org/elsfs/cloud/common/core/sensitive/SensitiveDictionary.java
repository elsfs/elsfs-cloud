/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.sensitive;

import com.hankcs.algorithm.AhoCorasickDoubleArrayTrie;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Supplier;
import org.elsfs.cloud.common.util.lang.StringUtils;

/**
 * 敏感词典
 *
 * @author zeng
 */
public class SensitiveDictionary {

  // Aho-Corasick双数组自动机，用于高效匹配敏感词
  private static AhoCorasickDoubleArrayTrie<String> trie;

  /**
   * 对待处理数据进行敏感词处理。
   *
   * @param supplier 待处理的数据供应者，返回敏感词列表。
   * @param word 需要检查的文档或文本。
   * @return 匹配到的敏感词列表及其在文本中的位置信息。
   */
  protected static List<AhoCorasickDoubleArrayTrie.Hit<String>> handle(
      Supplier<List<String>> supplier, String word) {
    // 检查输入的文本是否为空
    if (StringUtils.hasText(word)) {
      throw new RuntimeException("The word is not allowed null or ''");
    }
    // 若敏感词自动机未初始化，则加载敏感词数据
    if (trie == null) {
      trie = load(supplier.get());
    }
    // 使用自动机进行文本扫描，返回所有匹配的敏感词
    return trie.parseText(word);
  }

  /**
   * 重新加载敏感词数据。
   *
   * @param supplier 敏感词数据供应者。
   */
  protected static void reload(Supplier<List<String>> supplier) {
    // 通过供应者获取敏感词列表并重新加载自动机
    trie = load(supplier.get());
  }

  /**
   * 加载敏感词数据到Aho-Corasick自动机。
   *
   * @param words 敏感词列表。
   * @return 构建好的Aho-Corasick双数组自动机。
   */
  private static AhoCorasickDoubleArrayTrie<String> load(List<String> words) {
    // 检查敏感词列表是否为空
    if (words == null || words.isEmpty()) {
      throw new RuntimeException("Failed to load sensitive words");
    }
    // 创建自动机并构建敏感词检索树
    AhoCorasickDoubleArrayTrie<String> arrayTrie = new AhoCorasickDoubleArrayTrie<>();
    Map<String, String> treeMap = new TreeMap<>();
    // 将敏感词添加到检索树中
    words.parallelStream().forEach(word -> treeMap.put(word, word));
    // 构建自动机
    arrayTrie.build(treeMap);
    return arrayTrie;
  }
}
