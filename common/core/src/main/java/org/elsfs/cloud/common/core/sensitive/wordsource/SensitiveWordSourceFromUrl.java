/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.sensitive.wordsource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.util.lang.StringUtils;

/**
 * 从url加载指定的词源文件
 *
 * @author zeng
 */
@Slf4j
public class SensitiveWordSourceFromUrl implements ISensitiveWordSource {

  private final String url;

  /**
   * 构造器
   *
   * @param url url 路径
   */
  public SensitiveWordSourceFromUrl(String url) {
    this.url = url;
  }

  @Override
  public List<String> loadSource() {
    try {
      URL url = new URL(this.url);
      BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
      String line;
      List<String> list = new ArrayList<>();
      while (null != (line = reader.readLine())) {
        if (StringUtils.hasText(line)) {
          list.add(line.trim());
        }
      }
      LOGGER.info("Successfully downloaded from {}", this.url);
      return list;
    } catch (Exception e) {
      throw new RuntimeException("the url resource download failed");
    }
  }
}
