/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringJoiner;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

/**
 * 启动信息
 *
 * @author zjy
 */
@Slf4j
public class PrintStartInfo {

  /**
   * 输出启动信息
   *
   * @param env 环境变量
   * @param stop 启动时间
   */
  @SneakyThrows(UnknownHostException.class)
  public static void outputLog(Environment env, long stop) {
    String ip = InetAddress.getLocalHost().getHostAddress();
    String port = env.getProperty("server.port");
    port = port == null ? "8080" : port;
    String pathEvn = env.getProperty("server.servlet.context-path");
    String path = pathEvn == null ? "" : pathEvn;
    LOGGER.info(
        new StringJoiner(
                "\n\t",
                "\n----------------------------------------------------------\n",
                "----------------------------------------------------------")
            .add("Application Elsfs-Admin is running! Access URLs:\n\t")
            .add("Local: \t\thttp://localhost:%s%s/".formatted(port, path))
            .add("External: \thttp://%s:%s%s/".formatted(ip, port, path))
            .add("Swagger文档: \thttp://%s:%s%s/doc.html".formatted(ip, port, path))
            .add("Start-up time：%ssecond\n".formatted((System.currentTimeMillis() - stop) / 1000))
            .toString());
  }
}
