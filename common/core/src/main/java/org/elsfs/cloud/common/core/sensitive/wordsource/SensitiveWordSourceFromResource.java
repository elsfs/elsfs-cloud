/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.sensitive.wordsource;

import org.elsfs.cloud.common.util.lang.StringUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 从resource目录下加载指定的词源文件
 *
 * @author zeng
 */
public class SensitiveWordSourceFromResource implements ISensitiveWordSource {

  private String resourceFile = "sensitive.txt";

  /** 构造器 */
  public SensitiveWordSourceFromResource() {}

  /**
   * 构造器
   *
   * @param filename 文件名称
   */
  public SensitiveWordSourceFromResource(String filename) {
    this.resourceFile = filename;
  }

  @Override
  public List<String> loadSource() {
    try (InputStream inputStream =
            this.getClass().getClassLoader().getResourceAsStream(resourceFile);
        InputStreamReader reader = new InputStreamReader(inputStream);
        BufferedReader br = new BufferedReader(reader)) {
      String line;
      List<String> list = new ArrayList<>();
      while (null != (line = br.readLine())) {
        if (StringUtils.isNotBlank(line)) {
          list.add(line.trim());
        }
      }
      return list;
    } catch (Exception e) {
      throw new RuntimeException("the resource file read failed");
    }
  }

  /**
   * 资源文件
   *
   * @param filename 资源文件
   */
  public void renameFile(String filename) {
    resourceFile = filename;
  }
}
