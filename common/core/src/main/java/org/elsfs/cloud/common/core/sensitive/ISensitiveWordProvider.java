/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.sensitive;

import java.util.List;

/**
 * 敏感词处理器
 *
 * @author zeng
 */
public interface ISensitiveWordProvider {
  /**
   * 处理词条
   *
   * @param word 处理的文字
   * @param symbol 替换的字符串
   * @return 处理后的文字
   */
  String handle(String word, String symbol);

  /**
   * 检查词条
   *
   * @param word 需要检查的字符串
   * @return 返回检测出包含的敏感词
   */
  List<String> contain(String word);
}
