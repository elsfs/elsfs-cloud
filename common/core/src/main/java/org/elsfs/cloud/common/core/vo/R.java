/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 返回对象
 *
 * @param <T> 返回类型
 * @author zeng
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@SuppressWarnings("UpperCamelCase")
public class R<T> implements Serializable {
  @Serial private static final long serialVersionUID = 1L;

  /** 响应状态码 */
  @Getter @Setter private int code;

  /** 响应消息 */
  @Getter @Setter private String message;

  /** 响应类型 */
  @Getter @Setter private Type type;

  @Getter private final String timestamp = String.valueOf(System.currentTimeMillis());
  @Getter @Setter private boolean success = true;

  /** 响应结果数据 */
  @JsonSerialize @Getter @Setter private T result;

  /**
   * 创建一个表示成功响应的对象。
   *
   * @param <T> 返回类型
   * @return R对象，表示成功响应。
   */
  public static <T> R<T> success() {
    return new R<>(0, "ok", Type.success, true, null);
  }

  /**
   * 创建一个包含结果数据的成功响应对象。
   *
   * @param <T> 返回类型
   * @param result 成功时的返回结果
   * @return R对象，表示成功响应并携带结果数据。
   */
  public static <T> R<T> success(T result) {
    return new R<>(0, "ok", Type.success, true, result);
  }

  /**
   * 创建一个带有自定义成功消息和结果数据的成功响应对象。
   *
   * @param <T> 返回类型
   * @param message 成功时的自定义消息
   * @param result 成功时的返回结果
   * @return R对象，表示成功响应并携带自定义消息和结果数据。
   */
  public static <T> R<T> success(T result, String message) {
    return new R<>(0, message, Type.success, true, result);
  }

  /**
   * 创建一个表示错误响应的对象。
   *
   * @param <T> 返回类型
   * @return R对象，表示错误响应。
   */
  public static <T> R<T> error() {
    return new R<>(-1, "error", Type.error, true, null);
  }

  /**
   * 创建一个携带错误消息的错误响应对象。
   *
   * @param message 错误时的错误消息
   * @param <T> 返回类型
   * @return R对象，表示错误响应并携带错误消息。
   */
  public static <T> R<T> error(String message) {
    return new R<>(-1, message, Type.error, true, null);
  }
  /**
   * 创建一个携带错误消息的错误响应对象。
   *
   * @param message 错误时的错误消息
   * @param <T> 返回类型
   * @return R对象，表示错误响应并携带错误消息。
   */
  public static <T> R<T> error(T data) {
    return new R<>(-1, null, Type.error, true, data);
  }
  /** 响应类型枚举，标识成功、错误或无效令牌等不同类型。 */
  public enum Type {
    /** 成功响应 */
    success,
    /** 错误响应 */
    error,
    /** 无效令牌响应 */
    invalid_token
  }
}
