/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.aop;

import java.lang.annotation.Annotation;
import java.util.concurrent.atomic.AtomicBoolean;
import org.springframework.aop.support.annotation.AnnotationClassFilter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

/**
 * 注解过滤器
 *
 * @author zeng
 */
class AnnotationClassOrMethodFilter extends AnnotationClassFilter {

  private final AnnotationMethodsResolver methodResolver;

  AnnotationClassOrMethodFilter(Class<? extends Annotation> annotationType) {
    super(annotationType, true);
    this.methodResolver = new AnnotationMethodsResolver(annotationType);
  }

  @Override
  public boolean matches(Class<?> clazz) {
    return super.matches(clazz) || this.methodResolver.hasAnnotatedMethods(clazz);
  }

  private record AnnotationMethodsResolver(Class<? extends Annotation> annotationType) {
    public boolean hasAnnotatedMethods(Class<?> clazz) {
      final AtomicBoolean found = new AtomicBoolean(false);
      ReflectionUtils.doWithMethods(
          clazz,
          method -> {
            if (found.get()) {
              return;
            }
            Annotation annotation =
                AnnotationUtils.findAnnotation(
                    method, AnnotationMethodsResolver.this.annotationType);
            if (annotation != null) {
              found.set(true);
            }
          });
      return found.get();
    }
  }
}
