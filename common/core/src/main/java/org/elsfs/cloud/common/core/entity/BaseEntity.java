/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.entity;

import cn.idev.excel.annotation.ExcelIgnore;
import cn.idev.excel.annotation.format.DateTimeFormat;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.OrderBy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.time.ZonedDateTime;
import lombok.Data;

/**
 * 基础entity
 *
 * @author zeng
 */
@Data
@JsonPropertyOrder(alphabetic = false)
public abstract class BaseEntity implements Serializable {
  /**
   * 记录创建时间。
   *
   * <p>此字段在实体对象插入数据库时自动填充，采用的格式为"yyyy-MM-dd HH:mm:ss"。 @JsonFormat
   * 注解用于序列化和反序列化时的时间格式转换。 @TableField 注解标识该字段为数据库表中的字段，并指定在插入操作时填充。
   */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
  @JsonProperty(index = 0x7ffffffd)
  @TableField(fill = FieldFill.INSERT)
  @OrderBy(sort = Short.MAX_VALUE)
  @Schema(description = "创建时间")
//  @ExcelProperty(value = "创建时间", converter = ZonedDateTimeStringConverter.class)
  @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
  private ZonedDateTime createAt;

  private String createBy;

  /**
   * 更新时间。
   *
   * <p>该字段以Instant类型存储，表示更新操作的时间点。
   *
   * <p>通过@JsonFormat注解指定序列化和反序列化时的时间格式为"yyyy-MM-dd HH:mm:ss"。
   *
   * <p>通过@TableField注解指定该字段在数据库中的填充策略为INSERT_UPDATE，即在插入和更新操作时都会填充该字段。
   */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
  @JsonProperty(index = 0x7ffffffe)
  @TableField(fill = FieldFill.INSERT_UPDATE)
//  @ExcelProperty(value = "更新时间", converter = ZonedDateTimeStringConverter.class)
  @Schema(description = "更新时间")
  private ZonedDateTime updateAt;

  private String updateBy;

  @TableLogic
  @TableField(fill = FieldFill.INSERT)
  @ExcelIgnore
  private String deleteFlag;
}
