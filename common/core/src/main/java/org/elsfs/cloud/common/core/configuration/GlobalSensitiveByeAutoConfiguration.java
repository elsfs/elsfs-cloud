/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.configuration;

import org.elsfs.cloud.common.core.sensitive.ISensitiveWordProvider;
import org.elsfs.cloud.common.core.sensitive.SensitiveWordProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 敏感词处理器配置
 *
 * @author zeng
 */
@Configuration
public class GlobalSensitiveByeAutoConfiguration {

  /**
   * 敏感词处理器bean
   *
   * @return 敏感词处理器
   */
  @Bean
  @ConditionalOnMissingBean
  public ISensitiveWordProvider sensitiveWordProvider() {
    return new SensitiveWordProvider();
  }
}
