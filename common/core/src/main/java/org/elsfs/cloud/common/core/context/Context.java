/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.context;

/**
 * 用于保存与特定上下文相关联的信息的设施。
 *
 * @author zeng
 * @since 0.0.2
 */
public interface Context {

  /**
   * 返回与键关联的属性的值。
   *
   * @param key the key for the attribute
   * @param     <V> the type of the value for the attribute
   * @return the value of the attribute associated to the key, or {@code null} if not available
   */
  <V> V get(Object key);

  /**
   * 返回与键关联的属性的值。
   *
   * @param key the key for the attribute
   * @param     <V> the type of the value for the attribute
   * @return the value of the attribute associated to the key, or {@code null} if not available or
   *     not of the specified type
   */
  default <V> V get(Class<V> key) {
    V value = get((Object) key);
    return key.isInstance(value) ? value : null;
  }

  /**
   * 如果存在与键关联的属性，则返回 {@code true}，否则返回{@code false}。
   *
   * @param key the key for the attribute
   * @return {@code true} if an attribute associated to the key exists, {@code false} otherwise
   */
  boolean hasKey(Object key);
}
