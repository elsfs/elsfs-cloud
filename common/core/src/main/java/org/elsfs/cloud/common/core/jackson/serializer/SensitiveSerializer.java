/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.annotations.SensitiveBye;
import org.elsfs.cloud.common.annotations.SensitiveType;

/**
 * 字段脱敏处理类，用于在序列化字符串时根据指定的脱敏策略对敏感信息进行处理。
 *
 * @author zeng
 */
@Getter
@Slf4j
public class SensitiveSerializer extends StdSerializer<String> {

  /** 脱敏策略注解实例，用于获取具体的脱敏策略。 */
  private final SensitiveBye sensitiveBye;

  /**
   * 构造函数
   *
   * @param sensitiveBye 脱敏策略注解实例
   */
  protected SensitiveSerializer(SensitiveBye sensitiveBye) {
    super(String.class);
    this.sensitiveBye = sensitiveBye;
  }

  /**
   * 序列化方法，将字符串根据脱敏策略进行处理后输出。
   *
   * @param s 需要序列化的字符串
   * @param jsonGenerator JSON生成器，用于输出处理后的字符串
   * @param serializerProvider 序列化提供者，未在本方法中使用
   * @throws IOException 如果序列化过程中发生IO错误
   */
  @Override
  public void serialize(
      String s, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
      throws IOException {
    // 获取脱敏策略并应用
    SensitiveType strategy = sensitiveBye.strategy();
    // todo 序列化脱敏
    jsonGenerator.writeString(s);
  }
}
