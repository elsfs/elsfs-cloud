/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.core.jackson.serializer.SensitiveByeNopAnnotationIntrospector;
import org.junit.jupiter.api.Test;

/**
 * 测试
 *
 * @author zeng
 */
@NoArgsConstructor
@Slf4j
public class SensitiveByeTest {
  @Test
  public void jsonUser() throws JsonProcessingException {

    //  添加的'test'自定义策略直接在注解中使用即可：@SensitiveBye("test")
    final ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setAnnotationIntrospector(new SensitiveByeNopAnnotationIntrospector());
    final DefaultSerializerProvider.Impl provider = new DefaultSerializerProvider.Impl();
    objectMapper.setSerializerProvider(provider);
    final User user =
        User.builder()
            .id("123")
            .phone("13212341234")
            .name("你好")
            .idCard("522322188012120123")
            .password("123456")
            .mobile("010-88880000")
            .email("abc@163.com")
            .address("北京市朝阳区十里堡123号")
            .bankCard("622312312341234123")
            .carNumber("京A-1234567")
            .custom("test自定义")
            .build();
    final String string = objectMapper.writeValueAsString(user);
    LOGGER.warn(string);
  }
}
