/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.jackson;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.core.sensitive.SensitiveWordProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 测试
 *
 * @author zeng
 */
@SpringBootApplication
@ComponentScan("org.elsfs.cloud")
@Slf4j
@EnableAspectJAutoProxy
public class CoreApplicationTest {
  /** main */
  public static void main(String[] args) {
    final ConfigurableApplicationContext context =
        SpringApplication.run(CoreApplicationTest.class, args);
    final SensitiveWordProvider provider = context.getBean(SensitiveWordProvider.class);
    final List<String> contain = provider.contain("张三包二奶");
    final String handled = provider.handle("张三包二奶", "*");
    LOGGER.warn("{}", contain);
    LOGGER.warn("{}", handled);
  }
}
