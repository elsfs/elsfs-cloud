/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.core.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.core.utils.TreeNode;
import org.elsfs.cloud.common.core.utils.TreeUtils;
import org.junit.jupiter.api.Test;

/**
 * 测试构建数
 *
 * @author zeng
 */
@Slf4j
class TreeNodeTest {
  @Test
  void buildTest() throws JsonProcessingException {
    final var node = new TreeNodeEntity();
    final var node1 = new TreeNodeEntity();
    final var node2 = new TreeNodeEntity();
    final var node3 = new TreeNodeEntity();
    node.setId("1");
    node1.setId("2");
    node2.setId("3");
    node3.setId("4");
    node.setParentId("1");
    node1.setParentId("1");
    node2.setParentId("2");
    node3.setParentId("3");
    node.setName("0");
    node1.setName("1");
    node2.setName("2");
    node3.setName("3");
    List<TreeNodeEntity> tree = TreeUtils.buildTree(List.of(node, node1, node2, node3), "1");

    ObjectMapper mapper = new ObjectMapper();

    LOGGER.info("{}", mapper.writeValueAsString(tree));
  }

  @Data
  public static class TreeNodeEntity implements TreeNode<TreeNodeEntity, String> {
    private String id;
    private String parentId;
    private String name;
    private List<TreeNodeEntity> children = new ArrayList<>();

    @Override
    public void addChild(TreeNodeEntity child) {
      children.add(child);
    }

    @Override
    public int compareTo(TreeNodeEntity o) {
      return 0;
    }
  }
}
