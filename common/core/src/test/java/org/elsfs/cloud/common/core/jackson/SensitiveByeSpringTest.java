/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 测试
 *
 * @author zeng
 */
@Slf4j
@SpringBootTest
public class SensitiveByeSpringTest {
  @Autowired ObjectMapper objectMapper;

  @Test
  public void jsonUser() throws JsonProcessingException {
    final User user =
        User.builder()
            .id("123")
            .phone("13212341234")
            .name("你好")
            .idCard("522322188012120123")
            .password("123456")
            .mobile("010-88880000")
            .email("abc@163.com")
            .address("北京市朝阳区十里堡123号")
            .bankCard("622312312341234123")
            .carNumber("京A-1234567")
            .custom("test自定义")
            .build();
    final String string = objectMapper.writeValueAsString(user);
    LOGGER.warn(string);
  }
}
