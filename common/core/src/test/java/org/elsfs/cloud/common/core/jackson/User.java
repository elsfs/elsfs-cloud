/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.core.jackson;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.elsfs.cloud.common.annotations.SensitiveBye;
import org.elsfs.cloud.common.annotations.SensitiveType;

/**
 * user
 *
 * @author zeng
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
  private String id;

  @SensitiveBye(strategy = SensitiveType.PHONE)
  private String phone;

  @SensitiveBye(strategy = SensitiveType.CHINESE_NAME)
  private String name;

  @SensitiveBye(strategy = SensitiveType.ID_CARD)
  private String idCard;

  @SensitiveBye(strategy = SensitiveType.PASSWORD)
  private String password;

  @SensitiveBye(strategy = SensitiveType.MOBILE)
  private String mobile;

  @SensitiveBye(strategy = SensitiveType.EMAIL)
  private String email;

  @SensitiveBye(strategy = SensitiveType.ADDRESS)
  private String address;

  @SensitiveBye(strategy = SensitiveType.BANK_CARD)
  private String bankCard;

  @SensitiveBye(strategy = SensitiveType.CAR_NUMBER)
  private String carNumber;

  @SensitiveBye(strategy = SensitiveType.CUSTOM)
  private String custom;
}
