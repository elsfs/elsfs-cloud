/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import lombok.Data;

/**
 * 重复校验
 *
 * @author zeng
 */
@Data
@Schema(title = "重复校验数据模型", description = "重复校验数据模型")
public class DuplicateCheckQry implements Serializable {
  /** 数据库名 */
  private String schemaName;

  /** 表名 */
  @Schema(title = "表名", name = "tableName", example = "sys_log")
  private String tableName;

  /** 字段名 */
  @Schema(title = "字段名", name = "fieldName", example = "id")
  private String fieldName;

  /** 字段值 */
  @Schema(title = "字段值", name = "fieldVal", example = "1000")
  private String fieldVal;

  /** 数据ID */
  @Schema(title = "数据ID", name = "dataId", example = "2000")
  private String dataId;

  /** 数据ID */
  @Schema(title = "主键名称", name = "keyName", example = "2000")
  private String keyName;
}
