/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.controller;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Controller配置
 *
 * @author zeng
 */
@Data
@Accessors(chain = true, fluent = true)
public class ControllerConfig {
  /** 是否支持分页 */
  private boolean page;

  /** 是否支持列表查询 */
  private boolean list;

  /** 是否支持查询单个 */
  private boolean getById;

  /** 是否支持修改状态 */
  private boolean editState;

  /** 是否支持添加 */
  private boolean add;

  /** 是否支持修改 */
  private boolean edit;

  /** 是否支持删除单个 */
  private boolean removeById;

  private boolean removeBatch;

  /** 是否支持树形结构 */
  private boolean tree;

  /** 是否支持导出 */
  private boolean exportExcel;

  /** 是否支持导入 */
  private boolean importExcel;

  /** 是否支持查询分页软删除数据 */
  private boolean logicPage;

  /** 是否支持恢复软删除数据 */
  private boolean logicRestore;

  /** 是否支持批量恢复软删除数据 */
  private boolean logicRestoreBatch;

  /** 是否支持删除软删除数据 */
  private boolean logicDelete;

  /** 是否支持批量删除软删除数据 */
  private boolean logicDeleteBatch;
}
