package org.elsfs.cloud.common.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Schema(title = "重复校验数据模型", description = "重复校验数据模型")
@Accessors(chain = true)
public class DuplicateCheckVO
{
  // 是否通过
  private Boolean pass;
  // 错误信息
  private String message;
}
