/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.controller;

import io.swagger.v3.oas.annotations.Operation;
import java.util.Set;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;
import org.elsfs.cloud.common.mybatis.utils.DateQuery;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 逻辑删除控制器
 *
 * @param <E> entity
 * @param <Q> entity
 * @param <Add> entity
 * @param <Edit> entity
 * @param <ER> IElsfsRepository
 * @param <ID> id type
 */
public abstract class LogicRepositoryCrudController<
        E, Q extends E, Add extends E, Edit extends E, ER extends IElsfsRepository<E, ID>, ID>
    extends RepositoryCrudController<E, Q, Add, Edit, ER, ID> {

  /**
   * 分页查询软删除数据
   *
   * @param entity entity
   * @param qry qry
   * @return page
   */
  @GetMapping("/logic/page")
  @Operation(summary = "分页查询软删除数据", description = "分页查询软删除数据")
  public R<?> logicPage(@ParameterObject E entity, @ParameterObject DateQuery<E> qry) {
    if (!getConfig().logicPage()) {
      return R.error("不支持查询软删除数据");
    }
    return R.success(repository.ignoreLogicDeletPage(qry.toPage(), qry.query(entity)));
  }

  /**
   * 恢复软删除数据
   *
   * @param id id
   * @return r
   */
  @PutMapping("/logic/add/{id}")
  @Operation(summary = "恢复软删除数据", description = "恢复软删除数据")
  public R<Boolean> restoreById(@PathVariable("id") String id) {
    if (getConfig().logicRestore()) {
      return R.success(repository.restoreById(id), "恢复成功");
    }
    return R.error("不支持恢复");
  }

  /**
   * 批量恢复软删除数据
   *
   * @param ids ids
   * @return r
   */
  @PutMapping("/logic/add")
  @Operation(summary = "批量恢复软删除数据", description = "批量恢复软删除数据")
  public R<Boolean> restoreBatchById(@RequestBody Set<String> ids) {
    if (getConfig().logicRestoreBatch()) {
      return R.success(repository.restoreBatchById(ids), "批量恢复成功");
    }
    return R.error("不支持批量恢复");
  }

  /**
   * 根据id删除软删除数据
   *
   * @param id id
   * @return r
   */
  @DeleteMapping("/logic/del/{id}")
  @Operation(summary = "根据id删除软删除数据", description = "根据id删除软删除数据")
  public R<Boolean> logicDel(@PathVariable("id") String id) {
    if (getConfig().logicDelete()) {
      return R.success(repository.deleteByIdIgnoreLogicDelete(id), "回收站删除成功");
    }
    return R.error("不支持删除");
  }

  /**
   * 根据ids批量删除软删除数据
   *
   * @param idList idList
   * @return r
   */
  @DeleteMapping("/logic/del")
  @Operation(summary = "根据ids批量删除软删除数据", description = "根据ids批量删除软删除数据")
  public R<Boolean> deleteBatch(@RequestBody Set<String> idList) {
    if (getConfig().logicDeleteBatch()) {
      return R.success(repository.deleteBatchByIdIgnoreLogicDelete(idList), "回收站删除成功");
    }
    return R.error("不支持批量删除");
  }
}
