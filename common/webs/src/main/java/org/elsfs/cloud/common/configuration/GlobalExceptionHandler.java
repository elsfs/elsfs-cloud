/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.configuration;

import java.util.List;
import java.util.Locale;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.util.exception.ElsfsException;
import org.elsfs.cloud.common.util.exception.NotSupportedException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.Assert;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常的处理器@RestControllerAdvice
 *
 * @see RestControllerAdvice 该注解是一个复合注解包括下面两个注解
 * @see ControllerAdvice 将控制所有的Controller层并拦截所有的异常
 * @see ResponseBody 将本类中所有方法的返回值转换为JSON
 */
@Slf4j
@Order(10000)
@RestControllerAdvice
@ConditionalOnExpression("!'${security.oauth2.client.clientId}'.isEmpty()")
public class GlobalExceptionHandler {

  /**
   * 获取一个消息源访问器，用于访问应用程序的消息定义。 这个方法创建并配置了一个可重载的资源包消息源，用于从指定的资源包中加载消息。
   *
   * @return MessageSourceAccessor 返回一个配置好的消息源访问器实例，可以用来获取本地化消息。
   */
  protected final MessageSourceAccessor getMessageSource() {
    // 创建一个可重载的资源包消息源实例
    ReloadableResourceBundleMessageSource messageSource =
        new ReloadableResourceBundleMessageSource();
    // 添加消息资源包的基名，首先从Spring Security提供的消息资源开始
    messageSource.addBasenames("org.springframework.security.messages");
    // 然后添加应用类路径下的消息资源
    messageSource.addBasenames("classpath:messages");
    // 设置默认的语言环境为中文（中国）
    messageSource.setDefaultLocale(Locale.CHINA);
    // 返回一个消息源访问器，方便在应用中获取本地化消息
    return new MessageSourceAccessor(messageSource);
  }

  /**
   * 全局异常.
   *
   * @param e the e
   * @return R
   */
  @ExceptionHandler(ElsfsException.class)
  @ResponseStatus(HttpStatus.PAYMENT_REQUIRED)
  public R handleElsfsException(ElsfsException e) {
    LOGGER.error("全局异常信息 ex={}", e.getMessage(), e);
    // 业务异常交由 sentinel 记录
    // Tracer.trace(e);
    return R.error(e.getLocalizedMessage());
  }

  /**
   * 全局异常.
   *
   * @param e the e
   * @return R
   */
  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  public R handleGlobalException(Exception e) {
    LOGGER.error("全局异常信息 ex={}", e.getMessage(), e);
    // 业务异常交由 sentinel 记录
    // Tracer.trace(e);
    return R.error(e.getLocalizedMessage());
  }

  /**
   * 处理业务校验过程中碰到的非法参数异常 该异常基本由{@link org.springframework.util.Assert}抛出
   *
   * @param exception 参数校验异常
   * @return API返回结果对象包装后的错误输出结果
   * @see Assert#hasLength(String, String)
   * @see Assert#hasText(String, String)
   * @see Assert#isTrue(boolean, String)
   * @see Assert#isNull(Object, String)
   * @see Assert#notNull(Object, String)
   */
  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(HttpStatus.OK)
  public R<Void> handleIllegalArgumentException(IllegalArgumentException exception) {
    LOGGER.error("非法参数,ex = {}", exception.getMessage(), exception);
    return R.error(exception.getMessage());
  }

  /**
   * AccessDeniedException
   *
   * @param e the e
   * @return R
   */
  @ExceptionHandler(AccessDeniedException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  public R<Void> handleAccessDeniedException(AccessDeniedException e) {
    String msg =
        getMessageSource().getMessage("AbstractAccessDecisionManager.accessDenied", e.getMessage());
    LOGGER.warn("拒绝授权异常信息 ex={}", msg);
    return R.error(msg);
  }

  /**
   * 处理校验对象时候的异常MethodArgumentNotValidException
   *
   * @param exception e
   * @return R
   */
  @ExceptionHandler({MethodArgumentNotValidException.class})
  @ResponseStatus(HttpStatus.OK)
  public R<Void> handleBodyValidException(MethodArgumentNotValidException exception) {
    List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
    getWarn(fieldErrors);
    return R.error(
        String.format(
            "%s %s", fieldErrors.get(0).getField(), fieldErrors.get(0).getDefaultMessage()));
  }

  /**
   * validation Exception (以form-data形式传参)
   *
   * @param exception e
   * @return R
   */
  @ExceptionHandler({BindException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public R bindExceptionHandler(BindException exception) {
    List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
    getWarn(fieldErrors);
    return R.error(fieldErrors.get(0).getDefaultMessage());
  }

  @ExceptionHandler({NotSupportedException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public R notSupportedBindExceptionHandler(NotSupportedException exception) {
    LOGGER.error("非法参数,ex = {}", exception.getMessage(), exception);
    return R.error(exception.getMessage());
  }

  private static void getWarn(List<FieldError> fieldErrors) {
    LOGGER.warn("参数绑定异常,ex = {}", fieldErrors.get(0).getDefaultMessage());
  }
}
