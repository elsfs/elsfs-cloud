/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.controller;

import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import org.elsfs.cloud.common.core.utils.TreeNode;
import org.elsfs.cloud.common.core.utils.TreeUtils;
import org.elsfs.cloud.common.core.vo.R;
import org.elsfs.cloud.common.mybatis.repository.IElsfsRepository;
import org.elsfs.cloud.common.mybatis.utils.DateQuery;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * This is an abstract tree {@link TreeRepositoryCrudController controller} for crud and tree query
 *
 * @param <E> entity
 * @param <Q> query entity
 * @param <Add> add entity
 * @param <Edit> edit entity
 * @param <ER> repository type
 * @param <ID> id type
 * @author zeng
 */
public abstract class TreeRepositoryCrudController<
        E extends TreeNode<E, String>,
        Q extends E,
        Add extends E,
        Edit extends E,
        ER extends IElsfsRepository<E, ID>,
        ID>
    extends RepositoryCrudController<E, Q, Add, Edit, ER, ID> {

  @Override
  protected ControllerConfig getConfig() {
    return super.getConfig().tree(true);
  }

  /**
   * query tree
   *
   * @param qry query datetime range and order by
   * @param sysDept where entity
   * @return tree data
   */
  @GetMapping("/tree")
  @Operation(summary = "获取树型列表", description = "获取树型列表")
  public R<List<E>> tree(DateQuery<E> qry, E sysDept) {
    return R.success(TreeUtils.buildTree(repository.list(qry.query(sysDept)), "0"));
  }
}
