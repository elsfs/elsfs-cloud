/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字典注解
 *
 * @author zeng
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dict {

  /**
   * 用于区分不同业务
   *
   * @return 用于区分不同业务
   */
  String dictCode();

  /**
   * 目标显示属性（待绑定属性，注意非数据库字段请排除）
   *
   * @return 目标显示属性（待绑定属性，注意非数据库字段请排除）
   */
  String target();

  /**
   * 数据字典表
   *
   * @return 数据字典表
   */
  String dictTable() default "";

  /**
   * 注解生成字段名 如果不是# 号开头 则不生成 其中#号是字段名 例如： sex 则生成 sex_dictText字段
   *
   * @return 注解生成字段名
   */
  String fieldName() default "#_dictText";
}
