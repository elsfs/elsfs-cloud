/*
 * Copyright (c) 2023-2024 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.annotations;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 内置脱敏策略
 *
 * @author zeng
 */
@Getter
@RequiredArgsConstructor
public enum SensitiveType {

  /** 中文名字的敏感信息处理方式 */
  CHINESE_NAME,

  /** 身份证号的敏感信息处理方式 */
  ID_CARD,

  /** 密码的敏感信息处理方式 */
  PASSWORD,

  /** 手机号的敏感信息处理方式 */
  MOBILE,

  /** 电话号码的敏感信息处理方式 */
  PHONE,

  /** 电子邮件地址的敏感信息处理方式 */
  EMAIL,

  /** 地址的敏感信息处理方式 */
  ADDRESS,

  /** 车牌号的敏感信息处理方式 */
  CAR_NUMBER,

  /** 银行卡号的敏感信息处理方式 */
  BANK_CARD,

  /** 自定义敏感信息的处理方式 */
  CUSTOM
}
