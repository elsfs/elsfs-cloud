/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据库编码开关
 *
 * @author zeng
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EnableCipher {

  /**
   * 定义参数的加密或解密处理方式。 该注解用于指定参数是否需要进行加密或解密处理，默认为不处理。
   *
   * <p>CipherType 加密或解密类型，默认为不处理（CipherType.NONE）。
   *
   * @return 返回CipherType枚举类型，指定参数的处理方式。
   */
  CipherType parameter() default CipherType.NONE;

  /**
   * 定义结果处理的形式，可以选择加密、解密或者不处理。 默认情况下不进行处理。
   *
   * @return CipherType 结果处理类型，枚举类型，包括加密、解密和不处理（默认）三种选项。
   */
  CipherType result() default CipherType.NONE;

  /** 参数处理形式 */
  enum CipherType {

    /** 不处理 */
    NONE,

    /** 加密 */
    ENCRYPT,

    /** 解密 */
    DECRYPT,
  }
}
