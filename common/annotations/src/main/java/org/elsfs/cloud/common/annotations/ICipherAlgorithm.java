/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.common.annotations;

/**
 * 加密算法接口
 *
 * @author zeng
 */
public interface ICipherAlgorithm {

  /**
   * 加密方法
   *
   * @param value 明文
   * @return 返回加密后的密文
   */
  String encrypt(String value);

  /**
   * 解密方法
   *
   * @param value 密文
   * @return 返回解密后的明文, 如果算法不可逆，直接返回密文
   */
  String decrypt(String value);
}
