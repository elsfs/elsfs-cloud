///*
// * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package org.elsfs.cloud.common.excel;
//
//
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//
//import cn.idev.excel.EasyExcel;
//import cn.idev.excel.ExcelWriter;
//import cn.idev.excel.FastExcel;
//import cn.idev.excel.annotation.ExcelProperty;
//import cn.idev.excel.annotation.format.DateTimeFormat;
//import cn.idev.excel.annotation.format.NumberFormat;
//import cn.idev.excel.annotation.write.style.ContentFontStyle;
//import cn.idev.excel.annotation.write.style.ContentStyle;
//import cn.idev.excel.annotation.write.style.HeadFontStyle;
//import cn.idev.excel.annotation.write.style.HeadStyle;
//import cn.idev.excel.converters.Converter;
//import cn.idev.excel.converters.WriteConverterContext;
//import cn.idev.excel.enums.poi.FillPatternTypeEnum;
//import cn.idev.excel.event.SyncReadListener;
//import cn.idev.excel.metadata.GlobalConfiguration;
//import cn.idev.excel.metadata.data.ReadCellData;
//import cn.idev.excel.metadata.data.WriteCellData;
//import cn.idev.excel.metadata.property.ExcelContentProperty;
//import cn.idev.excel.write.metadata.WriteSheet;
//import com.alibaba.excel.annotation.ExcelProperty;
//import com.alibaba.excel.annotation.format.DateTimeFormat;
//import com.alibaba.excel.annotation.format.NumberFormat;
//import com.alibaba.excel.annotation.write.style.ContentFontStyle;
//import com.alibaba.excel.converters.Converter;
//import com.alibaba.excel.converters.WriteConverterContext;
//import com.alibaba.excel.metadata.GlobalConfiguration;
//import com.alibaba.excel.metadata.data.ReadCellData;
//import com.alibaba.excel.metadata.data.WriteCellData;
//import com.alibaba.excel.metadata.property.ExcelContentProperty;
//import lombok.EqualsAndHashCode;
//import lombok.Getter;
//import lombok.Setter;
//import lombok.ToString;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.Test;
//import org.springframework.core.io.ClassPathResource;
//
///**
// * 测试导入导出
// *
// * @author zeng
// */
//@Slf4j
//public class DemoTest {
//
//  private static final File excelFile;
//
//  static {
//    try {
//      excelFile = new ClassPathResource("demo.xlsx").getFile();
//    } catch (IOException e) {
//      throw new RuntimeException(e);
//    }
//  }
//
//  @Test
//  void writeWorkbookExcel() throws IOException {
//    // 分页查询数据
//    DemoData demoData = new DemoData();
//    demoData.setData("测试数据");
//    demoData.setCreateAt("2023年11月04日 22时55分25秒");
//    demoData.setNumberFormat(1.1d);
//    demoData.setSex(2);
//    FastExcel.write(excelFile, DemoData.class)
//        // 在 write 方法之后， 在 sheet方法之前都是设置WriteWorkbook的参数
//        .sheet("工作表")
//        .doWrite(List.of(demoData,demoData));
//  }
//
//  /**
//   * 最简单的读
//   *
//   * <p>1. 创建excel对应的实体对象 参照{@link DemoData}
//   *
//   * <p>2. 由于默认一行行的读取excel，所以需要创建excel一行一行的回调监听器，参照{@link AnalysisEventListener}
//   *
//   * <p>3. 直接读即可
//   */
//  @Test
//  public void simpleRead() {
//    // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
//    List<DemoData> doneReadSync =
//      FastExcel
//            // 读取文件 处理的类型 箭头
//            .read(excelFile, DemoData.class, new SyncReadListener())
//            .sheet("工作表")
//            .head(DemoData.class)
//            .doReadSync();
//    doneReadSync.forEach(s -> LOGGER.info("{}", s));
//  }
//
//  /**
//   * excel 导入导出实体
//   *
//   * @author zeng
//   */
//  @Getter
//  @Setter
//  @EqualsAndHashCode
//  @ToString
//  // 头背景设置成红色 IndexedColors.RED.getIndex()
//  @HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 10)
//  // 头字体设置成15
//  @HeadFontStyle(fontHeightInPoints = 15)
//  // 内容的背景设置成绿色 IndexedColors.GREEN.getIndex()
//  @ContentStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 17)
//  // 内容字体设置成15
//  @ContentFontStyle(fontHeightInPoints = 15)
//  public static class DemoData {
//    @ExcelProperty("数据")
//    // 字符串的头背景设置成粉红 IndexedColors.PINK.getIndex()
//    @HeadStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 14)
//    // 字符串的头字体设置成20
//    @HeadFontStyle(fontHeightInPoints = 30)
//    // 字符串的内容的背景设置成天蓝 IndexedColors.SKY_BLUE.getIndex()
//    @ContentStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 40)
//    // 字符串的内容字体设置成20
//    @ContentFontStyle(fontHeightInPoints = 30)
//    private String data;
//
//    // 格式化时间读取
//    @DateTimeFormat("yyyy年MM月dd日 HH时mm分ss秒")
//    @ExcelProperty("创建时间")
//    private String createAt;
//
//    @NumberFormat("#.##%")
//    @ExcelProperty("数字转换")
//    public Double numberFormat;
//
//    @ExcelProperty(value = "性别", converter = DemoConverter.class)
//    private Integer sex;
//  }
//
//  /**
//   * 自定义转换器
//   *
//   * @author zeng
//   */
//  public static class DemoConverter implements Converter<Integer> {
//    /**
//     * Excel转换为java对象
//     *
//     * @param cellData cellData
//     * @param contentProperty contentProperty
//     * @param globalConfiguration globalConfiguration
//     * @return i
//     * @throws Exception e
//     */
//    @Override
//    public Integer convertToJavaData(
//        ReadCellData<?> cellData,
//        ExcelContentProperty contentProperty,
//        GlobalConfiguration globalConfiguration)
//        throws Exception {
//      String stringValue = cellData.getStringValue();
//      return switch (stringValue) {
//        case "男" -> 1;
//        case "女" -> 2;
//        default -> 0;
//      };
//    }
//
//    /**
//     * java类型转换为Excel
//     *
//     * @param context 数据
//     * @return WriteCellData
//     * @throws Exception e
//     */
//    @Override
//    public WriteCellData<?> convertToExcelData(WriteConverterContext<Integer> context)
//        throws Exception {
//      return switch (context.getValue()) {
//        case 1 -> new WriteCellData<>("男");
//        case 2 -> new WriteCellData<>("女");
//        default -> new WriteCellData<>("未知");
//      };
//    }
//  }
//}
