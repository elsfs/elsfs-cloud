//package org.elsfs.cloud.common.excel.configuration;
//
//import org.elsfs.cloud.starter.excel.handler.DictDataProvider;
//import org.elsfs.cloud.starter.excel.vo.DictEnum;
//import org.springframework.boot.autoconfigure.AutoConfiguration;
//
//@AutoConfiguration
//public class ElsfsDictDataProvider implements DictDataProvider {
//  /**
//   * 获取 dict
//   *
//   * @param type 类型
//   * @return {@link DictEnum[] }
//   */
//  @Override
//  public DictEnum[] getDict(String type) {
//    return new DictEnum[0];
//  }
//}
