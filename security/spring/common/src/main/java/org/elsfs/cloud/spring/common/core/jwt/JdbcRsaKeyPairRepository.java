/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.core.jwt;

import com.nimbusds.jose.jwk.JWK;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.elsfs.cloud.api.security.key.OAuth2JWKRepository;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlParameterValue;

/**
 * 默认实现 基于jdbc RsaKeyPairRepository
 *
 * @author zeng
 */
@RequiredArgsConstructor
public class JdbcRsaKeyPairRepository implements OAuth2JWKRepository {
  private static final String COLUMN_NAMES = "id,key_type,json_object, issue_time";
  private static final String TABLE_NAME = "oauth2_jwk";
  private static final String LOAD_All_SQL = "SELECT " + COLUMN_NAMES + " FROM " + TABLE_NAME;
  private static final String LOAD_BY_ID_SQL = LOAD_All_SQL + " WHERE id = ?";
  private static final String DELETE_BY_ID_SQL = "DELETE FROM " + TABLE_NAME + " WHERE " + "id = ?";
  private static final String SAVE_SQL =
      "INSERT INTO " + TABLE_NAME + " (" + COLUMN_NAMES + ") VALUES (?, ?, ?,?)";
  private final JdbcOperations jdbcOperations;
  @Setter private JWKRowMapper jwkRowMapper = new JWKRowMapper();

  @Override
  public List<JWK> findJWKs() {
    return jdbcOperations.query(LOAD_All_SQL, jwkRowMapper);
  }

  @Override
  public void delete(String id) {
    jdbcOperations.update(DELETE_BY_ID_SQL, new SqlParameterValue(Types.VARCHAR, id));
  }

  @Override
  public void save(JWK jwk) {
    jwk = verificationJWK(jwk);
    delete(jwk.getKeyID());
    PreparedStatementSetter pss =
        new ArgumentPreparedStatementSetter(
            Arrays.asList(
                    new SqlParameterValue(new SqlParameter("id", Types.VARCHAR), jwk.getKeyID()),
                    new SqlParameterValue(
                        new SqlParameter("key_type", Types.VARCHAR), jwk.getKeyType()),
                    new SqlParameterValue(
                        new SqlParameter("json_object", Types.VARCHAR), jwk.toJSONString()),
                    new SqlParameterValue(
                        new SqlParameter("issue_time", Types.DATE), jwk.getIssueTime()))
                .toArray());
    jdbcOperations.update(SAVE_SQL, pss);
  }

  @Override
  public JWK findById(String keyId) {
    return jdbcOperations.queryForObject(LOAD_BY_ID_SQL, jwkRowMapper);
  }

  /** JWK映射 */
  public static class JWKRowMapper implements RowMapper<JWK> {

    @SneakyThrows
    @Override
    public JWK mapRow(ResultSet rs, int rowNum) throws SQLException {
      String jsonObject = rs.getString("json_object");
      return JWK.parse(jsonObject);
    }
  }
}
