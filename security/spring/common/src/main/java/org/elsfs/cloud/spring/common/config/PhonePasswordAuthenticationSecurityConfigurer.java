/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.config;

import org.elsfs.cloud.spring.common.filter.phone.password.PhonePasswordAuthenticationFilter;
import org.elsfs.cloud.spring.common.filter.phone.password.PhonePasswordAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * 配置手机号密码登录
 *
 * @author zeng
 */
public final class PhonePasswordAuthenticationSecurityConfigurer
    extends AbstractSecurityFilterConfigurer<
        PhonePasswordAuthenticationSecurityConfigurer, PhonePasswordAuthenticationFilter> {

  public PhonePasswordAuthenticationSecurityConfigurer() {
    super(new PhonePasswordAuthenticationFilter());
  }

  @Override
  public void init(HttpSecurity http) throws Exception {
    PhonePasswordAuthenticationProvider provider =
        new PhonePasswordAuthenticationProvider(
            getPasswordEncoder(http), getElsfsUserDetailsService(http));
    provider.setMessageSource(getMessageSource());
    http.authenticationProvider(provider);
  }
}
