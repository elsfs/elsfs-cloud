/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.core.jackson;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import org.elsfs.cloud.spring.common.core.userdetails.SecurityUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/** jackson 反序列化为 securityUser */
public class SecurityUserDeserializer extends JsonDeserializer<SecurityUser> {
  private static final TypeReference<Set<SimpleGrantedAuthority>> SIMPLE_GRANTED_AUTHORITY_SET =
      new TypeReference<Set<SimpleGrantedAuthority>>() {};

  @Override
  public SecurityUser deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException, JacksonException {
    ObjectMapper mapper = (ObjectMapper) jp.getCodec();
    JsonNode jsonNode = mapper.readTree(jp);
    SecurityUser user = new SecurityUser();
    user.setUserId(readJsonNode(jsonNode, "userId").asText());
    user.setUsername(readJsonNode(jsonNode, "username").asText());
    JsonNode passwordNode = readJsonNode(jsonNode, "password");
    String password = passwordNode.asText("");
    user.setPassword(password);
    user.setNickname(readJsonNode(jsonNode, "nickname").asText(""));
    user.setAvatar(readJsonNode(jsonNode, "avatar").asText(""));
    user.setTenantId(readJsonNode(jsonNode, "tenantId").asText());
    user.setPhone(readJsonNode(jsonNode, "phone").asText());
    user.setEmail(readJsonNode(jsonNode, "email").asText());
    Map<String, Object> attributes =
        mapper.convertValue(jsonNode.get("attributes"), new MapTypeReference());
    Set<? extends GrantedAuthority> authorities =
        mapper.convertValue(jsonNode.get("authorities"), new GrantedAuthoritySetTypeReference());
    user.setAuthorities(authorities);
    return user;
  }

  private JsonNode readJsonNode(JsonNode jsonNode, String field) {
    return jsonNode.has(field) ? jsonNode.get(field) : MissingNode.getInstance();
  }

  private static class MapTypeReference extends TypeReference<Map<String, Object>> {}

  private static class GrantedAuthoritySetTypeReference
      extends TypeReference<Set<SimpleGrantedAuthority>> {}
}
