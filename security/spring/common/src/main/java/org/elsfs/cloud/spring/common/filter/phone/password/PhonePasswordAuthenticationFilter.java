/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.phone.password;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import org.elsfs.cloud.spring.common.filter.AbstractFormFilter;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.context.DelegatingSecurityContextRepository;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.RequestAttributeSecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

/**
 * 自定义form表单 账号密码登录
 *
 * @author zeng
 */
public class PhonePasswordAuthenticationFilter extends AbstractFormFilter {
  public static final String DEFAULT_FILTER_PROCESSES_URI = "/login/phone";

  public static final String SPRING_SECURITY_FORM_PHONE_KEY = "phone";
  public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";
  private static final AntPathRequestMatcher DEFAULT_ANT_PATH_REQUEST_MATCHER =
      new AntPathRequestMatcher(DEFAULT_FILTER_PROCESSES_URI, "POST");
  private String phoneParameter = SPRING_SECURITY_FORM_PHONE_KEY;
  private String passwordParameter = SPRING_SECURITY_FORM_PASSWORD_KEY;
  private boolean postOnly = true;

  /** 构造器 */
  public PhonePasswordAuthenticationFilter() {
    super(DEFAULT_ANT_PATH_REQUEST_MATCHER);
    // https://github.com/spring-projects/spring-security/issues/13225
    setSecurityContextRepository(
        new DelegatingSecurityContextRepository(
            new RequestAttributeSecurityContextRepository(),
            new HttpSessionSecurityContextRepository()));
  }

  @Override
  public Authentication attemptAuthentication(
      HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    if (this.postOnly && !request.getMethod().equals("POST")) {
      throw new AuthenticationServiceException(
          "Authentication method not supported: " + request.getMethod());
    }
    String phone;
    String password;
    if (isJson(request)) {
      try {
        var read = objectMapper.reader().readValue(request.getInputStream(), Map.class);
        phone = read.get(getPhoneParameter()) instanceof String ph ? ph : "";
        password = read.get(getPasswordParameter()) instanceof String p ? p : "";
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } else {
      phone = obtainPhone(request);
      password = obtainPassword(request);
    }
    phone = (phone != null) ? phone.trim() : "";
    password = (password != null) ? password : "";
    PhonePasswordAuthenticationToken authRequest =
        PhonePasswordAuthenticationToken.unauthenticated(phone, password);

    // Allow subclasses to set the "details" property
    setDetails(request, authRequest);
    return getAuthenticationManager().authenticate(authRequest);
  }

  @Nullable protected String obtainPassword(HttpServletRequest request) {
    return request.getParameter(this.passwordParameter);
  }

  @Nullable protected String obtainPhone(HttpServletRequest request) {
    return request.getParameter(this.phoneParameter);
  }

  protected void setDetails(
      HttpServletRequest request, PhonePasswordAuthenticationToken authRequest) {
    authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
  }

  public void setPhoneParameter(String phoneParameter) {
    Assert.hasText(phoneParameter, "Username parameter must not be empty or null");
    this.phoneParameter = phoneParameter;
  }

  public void setPasswordParameter(String passwordParameter) {
    Assert.hasText(passwordParameter, "Password parameter must not be empty or null");
    this.passwordParameter = passwordParameter;
  }

  public void setPostOnly(boolean postOnly) {
    this.postOnly = postOnly;
  }

  public final String getPhoneParameter() {
    return this.phoneParameter;
  }

  public final String getPasswordParameter() {
    return this.passwordParameter;
  }
}
