/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.core.userdetails;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import org.elsfs.cloud.api.security.entity.SecurityUserEntity;
import org.springframework.security.core.GrantedAuthority;

/**
 * security 用户
 *
 * @author zeng
 */
@Data
@JsonTypeInfo(
  use = JsonTypeInfo.Id.NAME,
  include = JsonTypeInfo.As.PROPERTY,
  property = "type"
)
@JsonTypeName("SecurityUser")
public class SecurityUser implements SecurityUserEntity {
  private String userId;
  private String username;
  private String password;
  private String nickname;
  private String sex;
  private String avatar;
  private String tenantId;
  private String phone;
  private String email;
  private String validFlag;
  private boolean enabled;

  private Set<? extends GrantedAuthority> authorities = new HashSet<>();

  @Override
  @JsonIgnore
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  @JsonIgnore
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  @JsonIgnore
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  @JsonIgnore
  public boolean isEnabled() {
    return "0".equals(validFlag);
  }

  @Override
  public void eraseCredentials() {
    this.password = null;
  }
}
