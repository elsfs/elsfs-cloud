/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.applet;

/**
 * 小程序认证常用字段
 *
 * @author zeng
 */
public final class AppletParameterNames {
  public static final String SECRET = "secret";
  public static final String APPID = "appid";
  public static final String JS_CODE = "js_code";
  public static final String AUTH_CODE = "authCode";
  public static final String PHONE_CODE = "phoneCode";

  public static final String TYPE = "type";
}
