/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.config;

import org.elsfs.cloud.spring.common.federation.FederatedIdentityService;
import org.elsfs.cloud.spring.common.filter.applet.AppletAuthorizationCodeAuthenticationProvider;
import org.elsfs.cloud.spring.common.filter.applet.AppletLoginAuthenticationFilter;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 小程序认证配置
 *
 * @author zeng
 */
public final class AppletLoginAuthenticationConfigurer
    extends AbstractSecurityFilterConfigurer<
        AppletLoginAuthenticationConfigurer, AppletLoginAuthenticationFilter> {

  public AppletLoginAuthenticationConfigurer() {
    super(new AppletLoginAuthenticationFilter());
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    filter.setClientRegistrationRepository(getClientRegistrationRepository(http));
    filter.setAuthenticationManager(http.getSharedObject(AuthenticationManager.class));
    filter.setAuthenticationSuccessHandler(successHandler);
    filter.setAuthenticationFailureHandler(failureHandler);
    http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    super.configure(http);
  }

  @Override
  public void init(HttpSecurity http) throws Exception {
    AppletAuthorizationCodeAuthenticationProvider provider =
        new AppletAuthorizationCodeAuthenticationProvider(
            getFederatedIdentityServiceBean(http), getElsfsUserDetailsService(http));
    http.authenticationProvider(provider);
  }

  static <B extends HttpSecurityBuilder<B>>
      ClientRegistrationRepository getClientRegistrationRepository(B builder) {
    ClientRegistrationRepository clientRegistrationRepository =
        builder.getSharedObject(ClientRegistrationRepository.class);
    if (clientRegistrationRepository == null) {
      clientRegistrationRepository = getClientRegistrationRepositoryBean(builder);
      builder.setSharedObject(ClientRegistrationRepository.class, clientRegistrationRepository);
    }
    return clientRegistrationRepository;
  }

  private static <B extends HttpSecurityBuilder<B>>
      ClientRegistrationRepository getClientRegistrationRepositoryBean(B builder) {
    return builder
        .getSharedObject(ApplicationContext.class)
        .getBean(ClientRegistrationRepository.class);
  }

  private static <B extends HttpSecurityBuilder<B>>
      FederatedIdentityService getFederatedIdentityServiceBean(B builder) {
    return builder
        .getSharedObject(ApplicationContext.class)
        .getBean(FederatedIdentityService.class);
  }
}
