/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.federation;

import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.elsfs.cloud.api.security.entity.FederatedIdentityInterface;
import org.elsfs.cloud.common.core.entity.BaseEntity;

/**
 * 联合登录信息表 第三方登录
 *
 * @author zeng
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FederatedIdentity extends BaseEntity implements FederatedIdentityInterface {

  private String id;
  private String userId;

  /** 第三方应用的唯一标识 */
  private String identifier;

  /** 第三方应用名称 (微信, 微博,qq.gitee,github等) */
  private String identityType;

  /** 第三方登录的属性集合 */
  private Map<String, Object> attributes = new LinkedHashMap<>();

  /** 授权客户端注册Id */
  private String authorizedClientRegistrationId;
}
