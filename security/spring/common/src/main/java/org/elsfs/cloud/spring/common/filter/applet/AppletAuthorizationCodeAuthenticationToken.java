/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.applet;

import java.io.Serial;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.util.Assert;

/**
 * 小程序认证 token
 *
 * @author zeng
 */
@Getter
public class AppletAuthorizationCodeAuthenticationToken extends AbstractAuthenticationToken {
  @Serial private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

  private final Map<String, Object> additionalParameters = new HashMap<>();

  private final ClientRegistration clientRegistration;

  private final AppletAuthorizationRequest appletAuthorizationRequest;

  /**
   * 构造
   *
   * @param clientRegistration c
   * @param appletAuthorizationRequest r
   */
  public AppletAuthorizationCodeAuthenticationToken(
      ClientRegistration clientRegistration,
      AppletAuthorizationRequest appletAuthorizationRequest) {
    super(Collections.emptyList());
    Assert.notNull(clientRegistration, "clientRegistration cannot be null");
    Assert.notNull(appletAuthorizationRequest, "authorizationExchange cannot be null");
    this.clientRegistration = clientRegistration;
    this.appletAuthorizationRequest = appletAuthorizationRequest;
  }

  @Override
  public Object getPrincipal() {
    return this.clientRegistration.getClientId();
  }

  @Override
  public Object getCredentials() {
    return this.appletAuthorizationRequest.getOpenidCode();
  }
}
