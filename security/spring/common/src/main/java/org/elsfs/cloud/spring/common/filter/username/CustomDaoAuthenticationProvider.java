/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.username;

import org.elsfs.cloud.api.security.service.SecurityUserService;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 自定义 {@link DaoAuthenticationProvider}
 *
 * @author zeng
 */
public class CustomDaoAuthenticationProvider extends DaoAuthenticationProvider {
  /**
   * 构造器
   *
   * @param passwordEncoder passwordEncoder
   * @param securityUserService 自定义方ElsfsUserDetailsService bean
   */
  public CustomDaoAuthenticationProvider(
      PasswordEncoder passwordEncoder,
      SecurityUserService securityUserService,
      MessageSource messageSource) {
    super.setPasswordEncoder(passwordEncoder);
    super.setUserDetailsService(securityUserService);
    super.setMessageSource(messageSource);
  }
}
