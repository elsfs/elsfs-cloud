/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.phone.sms.send;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import org.elsfs.cloud.common.core.vo.R;
import org.springframework.core.log.LogMessage;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

/**
 * 短信发送
 *
 * @author zeng
 */
public class SmsSendFilter extends GenericFilterBean {
  public static final String DEFAULT_FILTER_PROCESSES_URI = "/login/phone/code";

  protected static ObjectMapper objectMapper = new ObjectMapper();
  public static final String SPRING_SECURITY_FORM_PHONE_KEY = "phone";
  public static final String SPRING_SECURITY_FORM_SECRET_KEY = "secretKey";
  private String phoneParameter = SPRING_SECURITY_FORM_PHONE_KEY;
  private String secretKeyParameter = SPRING_SECURITY_FORM_SECRET_KEY;
  private RequestMatcher requiresAuthenticationRequestMatcher;
  private GenerateSmsCode generateSmsCode = this::generateCode;
  private SmsCodeService smsCodeService = new MemorySmsCodeService();

  /** 构造器 */
  public SmsSendFilter() {
    this(DEFAULT_FILTER_PROCESSES_URI);
  }

  /** 构造器 */
  public SmsSendFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
    Assert.notNull(
        requiresAuthenticationRequestMatcher,
        "requiresAuthenticationRequestMatcher cannot be null");
    this.requiresAuthenticationRequestMatcher = requiresAuthenticationRequestMatcher;
  }

  protected SmsSendFilter(String defaultFilterProcessesUrl) {
    setFilterProcessesUrl(defaultFilterProcessesUrl);
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    doFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
  }

  protected void doFilter(
      HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws ServletException, IOException {
    if (!requiresAuthentication(request, response)) {
      chain.doFilter(request, response);
      return;
    }
    String phone;
    String secretKey;
    if (isJson(request)) {
      try {
        var read = objectMapper.reader().readValue(request.getInputStream(), Map.class);
        phone = read.get(this.phoneParameter) instanceof String ph ? ph : "";
        secretKey = read.get(this.secretKeyParameter) instanceof String p ? p : "";
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } else {
      phone = obtainPhone(request);
      secretKey = obtainSecretKey(request);
    }
    phone = (phone != null) ? phone.trim() : "";
    secretKey = (secretKey != null) ? secretKey : "";
    String string = smsCodeService.getCodeByPhone(phone);
    if (string != null) {
      R<Object> r = R.error();
      r.setMessage("短信已发送，1分钟后再试！");
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      ServletOutputStream stream = response.getOutputStream();
      byte[] value = objectMapper.writeValueAsBytes(r);
      stream.write(value);
      stream.flush();
    }
    String code = generateSmsCode.generate(phone, secretKey);
    smsCodeService.saveCodeAndSend(phone, secretKey, code);
    writeResponse(response);
  }

  protected void writeResponse(HttpServletResponse response) throws IOException {
    R<Object> r = R.success();
    r.setMessage("发送成功");
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    ServletOutputStream stream = response.getOutputStream();
    byte[] value = objectMapper.writeValueAsBytes(r);
    stream.write(value);
    stream.flush();
  }

  @Nullable protected String obtainPhone(HttpServletRequest request) {
    return request.getParameter(this.phoneParameter);
  }

  @Nullable protected String obtainSecretKey(HttpServletRequest request) {
    return request.getParameter(this.secretKeyParameter);
  }

  protected String generateCode(String phone, String secretKey) {
    final Random random = new Random(); // 定义一个随机生成数技术，用来生成随机数
    String yzm1 = "1234567890"; // 定义一个String变量存放需要的数据，一共10位
    String code = ""; // 定义一个空的Atring变量用来接收生成的验证码
    for (int i = 0; i < 5; i++) {
      int a = random.nextInt(10); // 随机生成0-10之间的数，提供索引位置
      code += yzm1.charAt(a); // 用get 和提供的索引找到相应位置的数据给变量
    }
    return code;
  }

  protected boolean isJson(HttpServletRequest request) {
    var contentType = request.getContentType();
    if (contentType == null) {
      return false;
    }
    return MediaType.APPLICATION_JSON.includes(MediaType.parseMediaType(contentType));
  }

  protected boolean requiresAuthentication(
      HttpServletRequest request, HttpServletResponse response) {
    if (this.requiresAuthenticationRequestMatcher.matches(request)) {
      return true;
    }
    if (this.logger.isTraceEnabled()) {
      this.logger.trace(
          LogMessage.format(
              "Did not match request to %s", this.requiresAuthenticationRequestMatcher));
    }
    return false;
  }

  public void setFilterProcessesUrl(String filterProcessesUrl) {
    setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(filterProcessesUrl));
  }

  public final void setRequiresAuthenticationRequestMatcher(RequestMatcher requestMatcher) {
    Assert.notNull(requestMatcher, "requestMatcher cannot be null");
    this.requiresAuthenticationRequestMatcher = requestMatcher;
  }

  public void setSecretKeyParameter(String secretKeyParameter) {
    this.secretKeyParameter = secretKeyParameter;
  }

  public void setPhoneParameter(String phoneParameter) {
    this.phoneParameter = phoneParameter;
  }

  public void setGenerateSmsCode(GenerateSmsCode generateSmsCode) {
    this.generateSmsCode = generateSmsCode;
  }

  public void setSmsCodeService(SmsCodeService smsCodeService) {
    this.smsCodeService = smsCodeService;
  }
}
