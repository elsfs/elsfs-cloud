/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.applet;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 小程序类型
 *
 * @author zeng
 */
@RequiredArgsConstructor
@Getter
public enum AppletType {
  WECHAT_MINI("wechatMini");
  private final String name;

  /**
   * 根据名称获取类型
   *
   * @param name nam
   * @return AppletType
   */
  public static AppletType getAppletType(String name) {
    for (AppletType value : values()) {
      if (value.name.equals(name)) {
        return value;
      }
    }
    return null;
  }
}
