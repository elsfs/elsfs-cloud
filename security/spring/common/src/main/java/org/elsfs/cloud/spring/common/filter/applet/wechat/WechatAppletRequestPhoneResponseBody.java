/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.applet.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 请求微信手机号body
 *
 * @author zeng
 */
@Data
public class WechatAppletRequestPhoneResponseBody {
  public static final String SUCCESS_ERR_MSG = "ok";

  @JsonProperty("errcode")
  private String errCode;

  @JsonProperty("errmsg")
  private String errMsg;

  @JsonProperty("phone_info")
  private PhoneInfo phoneInfo;

  /** 手机信息 */
  @Data
  public static class PhoneInfo {
    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
    private Watermark watermark;

    /** 小程序信息 */
    @Data
    public static class Watermark {
      private Long timestamp;
      private String appid;
    }
  }
}
