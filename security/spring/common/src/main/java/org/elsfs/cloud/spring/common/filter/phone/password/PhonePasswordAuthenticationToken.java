/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.phone.password;

import java.io.Serial;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.util.Assert;

/**
 * 手机号密码登录token
 *
 * @author zeng
 */
public class PhonePasswordAuthenticationToken extends AbstractAuthenticationToken {
  @Serial private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

  private final Object principal;

  private Object credentials;

  /**
   * This constructor can be safely used by any code that wishes to create a <code>
   * UsernamePasswordAuthenticationToken</code>, as the {@link #isAuthenticated()} will return
   * <code>false</code>.
   */
  public PhonePasswordAuthenticationToken(Object principal, Object credentials) {
    super(null);
    this.principal = principal;
    this.credentials = credentials;
    setAuthenticated(false);
  }

  /**
   * This constructor should only be used by <code>AuthenticationManager</code> or <code>
   * AuthenticationProvider</code> implementations that are satisfied with producing a trusted (i.e.
   * {@link #isAuthenticated()} = <code>true</code>) authentication token.
   *
   * @param principal 账号
   * @param credentials 密码
   * @param authorities 权限
   */
  public PhonePasswordAuthenticationToken(
      Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
    super(authorities);
    this.principal = principal;
    this.credentials = credentials;
    super.setAuthenticated(true); // must use super, as we override
  }

  /**
   * This factory method can be safely used by any code that wishes to create a unauthenticated
   * <code>UsernamePasswordAuthenticationToken</code>.
   *
   * @param principal 账号
   * @param credentials 密码
   * @return UsernamePasswordAuthenticationToken with false isAuthenticated() result
   * @since 5.7
   */
  public static PhonePasswordAuthenticationToken unauthenticated(
      Object principal, Object credentials) {
    return new PhonePasswordAuthenticationToken(principal, credentials);
  }

  /**
   * This factory method can be safely used by any code that wishes to create a authenticated <code>
   * UsernamePasswordAuthenticationToken</code>.
   *
   * @param principal 账号
   * @param credentials 密码
   * @return UsernamePasswordAuthenticationToken with true isAuthenticated() result
   * @since 5.7
   */
  public static PhonePasswordAuthenticationToken authenticated(
      Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
    return new PhonePasswordAuthenticationToken(principal, credentials, authorities);
  }

  @Override
  public Object getCredentials() {
    return this.credentials;
  }

  @Override
  public Object getPrincipal() {
    return this.principal;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    Assert.isTrue(
        !isAuthenticated,
        "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list"
            + " instead");
    super.setAuthenticated(false);
  }

  @Override
  public void eraseCredentials() {
    super.eraseCredentials();
    this.credentials = null;
  }
}
