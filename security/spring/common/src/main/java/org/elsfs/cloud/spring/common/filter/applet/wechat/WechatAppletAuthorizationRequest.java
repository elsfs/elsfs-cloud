/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.applet.wechat;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Map;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.elsfs.cloud.spring.common.filter.applet.AppletAuthorizationRequest;
import org.elsfs.cloud.spring.common.filter.applet.AppletParameterNames;
import org.elsfs.cloud.spring.common.filter.applet.ResponseBody;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * 微信小程序请求
 *
 * @author zeng
 */
@SuperBuilder
@Slf4j
public class WechatAppletAuthorizationRequest extends AppletAuthorizationRequest {

  private String secret;

  @Override
  public MultiValueMap<String, String> toMap() {
    MultiValueMap<String, String> stringHashMap = new LinkedMultiValueMap<>();
    if (appid != null) {
      stringHashMap.add(AppletParameterNames.APPID, appid);
    }
    if (secret != null) {
      stringHashMap.add(AppletParameterNames.SECRET, secret);
    }
    if (openidCode != null) {
      stringHashMap.add(AppletParameterNames.JS_CODE, openidCode);
    }
    stringHashMap.add(
        OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.AUTHORIZATION_CODE.getValue());
    return stringHashMap;
  }

  @Override
  public ResponseBody requestOpenid() {
    String redirectUri =
        UriComponentsBuilder.fromHttpUrl("https://api.weixin.qq.com/sns/jscode2session")
            .queryParams(this.toMap())
            .build()
            .toUriString();
    ResponseEntity<String> response = REST_OPERATIONS.getForEntity(redirectUri, String.class);
    try {
      return OBJECT_MAPPER.readValue(response.getBody(), ResponseBody.class);
    } catch (JsonProcessingException e) {
      throw new AuthenticationServiceException(e.getMessage());
    }
  }

  @Override
  public String requestPhone() {
    if (phoneCode == null) {
      return null;
    }
    var accessToken = requestToken();
    if (accessToken == null) {
      return null;
    }
    var phoneUrl =
        UriComponentsBuilder.fromHttpUrl(
                "https://api.weixin.qq.com/wxa/business/getuserphonenumber")
            .queryParam(OAuth2ParameterNames.ACCESS_TOKEN, accessToken)
            .build()
            .toUriString();
    // 构造请求体参数
    String requestBody =
        "{\"".concat(OAuth2ParameterNames.CODE).concat("\":\"").concat(phoneCode).concat("\"}");
    // 设置请求头信息
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    // 将请求体转换成HttpEntity对象
    HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);
    ResponseEntity<String> response =
        REST_OPERATIONS.exchange(phoneUrl, HttpMethod.POST, entity, String.class);
    try {
      WechatAppletRequestPhoneResponseBody responseBody =
          OBJECT_MAPPER.readValue(response.getBody(), WechatAppletRequestPhoneResponseBody.class);
      if (WechatAppletRequestPhoneResponseBody.SUCCESS_ERR_MSG.equals(responseBody.getErrMsg())) {
        WechatAppletRequestPhoneResponseBody.PhoneInfo phoneInfo = responseBody.getPhoneInfo();
        return phoneInfo.getPhoneNumber();
      }
    } catch (JsonProcessingException e) {
      LOGGER.warn(e.getMessage());
    }
    return null;
  }

  /**
   * 请求 token 参数的 map
   *
   * @return map
   */
  private String requestToken() {
    MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
    valueMap.add(AppletParameterNames.APPID, appid);
    valueMap.add(OAuth2ParameterNames.GRANT_TYPE, "client_credential");
    valueMap.add(AppletParameterNames.SECRET, secret);
    String requestUrl =
        UriComponentsBuilder.fromHttpUrl("https://api.weixin.qq.com/cgi-bin/token")
            .queryParams(valueMap)
            .build()
            .toUriString();
    var response = REST_OPERATIONS.getForEntity(requestUrl, Map.class);
    var body = response.getBody();
    if (body != null) {
      return body.get(OAuth2ParameterNames.ACCESS_TOKEN).toString();
    }
    return null;
  }
}
