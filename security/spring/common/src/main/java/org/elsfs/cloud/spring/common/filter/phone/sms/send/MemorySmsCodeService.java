/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.phone.sms.send;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.elsfs.cloud.common.sms.service.DefaultSmsService;
import org.elsfs.cloud.common.sms.service.ISmsService;

/**
 * 内存短信储存
 *
 * @author zeng
 */
public class MemorySmsCodeService implements SmsCodeService {

  private final ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>(256);

  @Override
  public ISmsService getSmsService() {
    return new DefaultSmsService();
  }

  @Override
  public String getCodeByPhone(String phone) {
    return concurrentHashMap.get(phone);
  }

  @Override
  public void saveCodeAndSend(String phone, String secretKey, String code) {
    concurrentHashMap.put(phone, code);
    getSmsService().sendSms(phone, "", Map.of("code", code));
  }

  @Override
  public void removeCode(String phone) {
    concurrentHashMap.remove(phone);
  }
}
