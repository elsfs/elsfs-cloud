/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter.applet;

import java.util.Collections;
import org.elsfs.cloud.spring.common.federation.FederatedIdentity;
import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * 小程序 token
 *
 * @author zeng
 */
public class AppletAuthorizationToken extends AbstractAuthenticationToken {

  private final FederatedIdentity federatedIdentity;

  public AppletAuthorizationToken(FederatedIdentity federatedIdentity) {
    super(Collections.emptySet());
    this.federatedIdentity = federatedIdentity;
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getPrincipal() {
    return federatedIdentity;
  }
}
