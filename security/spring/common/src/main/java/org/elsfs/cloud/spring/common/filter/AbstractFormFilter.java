/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.common.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * form 表单登录 Filter - 账号密码登录 - 手机号验证码登录 - 手机号密码登录 - 邮箱验证码登录 - 邮箱密码登录
 *
 * @author zeng
 */
public abstract class AbstractFormFilter extends AbstractAuthenticationProcessingFilter {
  protected static ObjectMapper objectMapper = new ObjectMapper();

  protected AbstractFormFilter(String defaultFilterProcessesUrl) {
    super(defaultFilterProcessesUrl);
  }

  protected AbstractFormFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
    super(requiresAuthenticationRequestMatcher);
  }

  protected AbstractFormFilter(
      String defaultFilterProcessesUrl, AuthenticationManager authenticationManager) {
    super(defaultFilterProcessesUrl, authenticationManager);
  }

  protected AbstractFormFilter(
      RequestMatcher requiresAuthenticationRequestMatcher,
      AuthenticationManager authenticationManager) {
    super(requiresAuthenticationRequestMatcher, authenticationManager);
  }

  protected boolean isJson(HttpServletRequest request) {
    return MediaType.APPLICATION_JSON.includes(
        MediaType.parseMediaType(request.getHeader(HttpHeaders.CONTENT_TYPE)));
  }
}
