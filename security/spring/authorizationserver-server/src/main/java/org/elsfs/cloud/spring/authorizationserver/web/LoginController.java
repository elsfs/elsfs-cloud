/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.authorizationserver.web;

import java.util.HashSet;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 登录端点
 *
 * @author zeng
 */
@Controller
@RequiredArgsConstructor
public class LoginController {
  @Autowired(required = false)
  private OAuth2ClientProperties oauth2ClientProperties;

  /** 登录页面 */
  @GetMapping("/login")
  public String login(Model model) {
    Set<String> registration = new HashSet<>();
    if (oauth2ClientProperties != null) {
      registration = oauth2ClientProperties.getRegistration().keySet();
      if (registration == null) {
        registration = new HashSet<>();
      }
    }

    model.addAttribute("registration", registration);
    return "login";
  }
}
