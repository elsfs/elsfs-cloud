/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.authorizationserver.configuration;

import lombok.RequiredArgsConstructor;
import org.elsfs.cloud.common.properties.ElsfsSecurityProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.server.servlet.OAuth2AuthorizationServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;

/**
 * AuthorizationServerSettings 配置
 *
 * @author zeng
 */
@Configuration(proxyBeanMethods = false)
@RequiredArgsConstructor
@EnableConfigurationProperties(ElsfsSecurityProperties.class)
public class AuthorizationServerSettingsConfig {
  private final OAuth2AuthorizationServerProperties auth2AuthorizationServerProperties;
  private final OAuth2ResourceServerProperties auth2ResourceServerProperties;

  @Bean
  AuthorizationServerSettings authorizationServerSettings() {
    String issuer = auth2AuthorizationServerProperties.getIssuer();
    if (issuer == null) {
      issuer = auth2ResourceServerProperties.getJwt().getIssuerUri();
    }
    if (issuer == null) {
      throw new RuntimeException("issuer 未配置");
    }
    return AuthorizationServerSettings.builder().issuer(issuer).build();
  }
}
