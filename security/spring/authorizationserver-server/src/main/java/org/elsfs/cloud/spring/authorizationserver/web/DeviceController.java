/*
 * Copyright (c) 2023-2023 elsfs Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.elsfs.cloud.spring.authorizationserver.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 设备认证自定义端点
 *
 * @author zeng
 */
@Controller
public class DeviceController {

  /**
   * 设备授权页面 activate
   *
   * @param userCode userCode
   * @return activate html
   */
  @GetMapping("/activate")
  public String activate(@RequestParam(value = "user_code", required = false) String userCode) {
    if (userCode != null) {
      return "redirect:/oauth2/device_verification?user_code=" + userCode;
    }
    return "device-activate";
  }

  @GetMapping("/activated")
  public String activated() {
    return "device-activated";
  }
}
